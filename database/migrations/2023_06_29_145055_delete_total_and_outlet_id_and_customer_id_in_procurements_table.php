<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('procurements', 'total')) {
            Schema::table('procurements', function (Blueprint $table) {
                $table->dropColumn(['total']);
            });
        }

        if (Schema::hasColumn('procurements', 'outlet_id')) {
            Schema::table('procurements', function (Blueprint $table) {
                $table->dropColumn(['outlet_id']);
            });
        }

        if (Schema::hasColumn('procurements', 'customer_id')) {
            Schema::table('procurements', function (Blueprint $table) {
                $table->dropColumn(['customer_id']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('procurements', 'total')) {
            Schema::table('procurements', function (Blueprint $table) {
                $table->string('total')->nullable();
            });
        }

        if (!Schema::hasColumn('procurements', 'outlet_id')) {
            Schema::table('procurements', function (Blueprint $table) {
                $table->string('outlet_id')->nullable();
            });
        }

        if (!Schema::hasColumn('procurements', 'customer_id')) {
            Schema::table('procurements', function (Blueprint $table) {
                $table->string('customer_id')->nullable();
            });
        }
    }
};
