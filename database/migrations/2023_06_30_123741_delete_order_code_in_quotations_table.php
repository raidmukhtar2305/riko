<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('quotations', 'order_code')) {
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn(['order_code']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('quotations', 'order_code')) {
            Schema::table('quotations', function (Blueprint $table) {
                $table->string('order_code')->nullable();
            });
        }
    }
};
