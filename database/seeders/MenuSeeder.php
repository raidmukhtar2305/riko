<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Menu;
use App\Models\Permission;
use Illuminate\Support\Str;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            // $dashboard = Menu::create([
            //     'id' => Str::uuid()->toString(),
            //     'name' => 'Dashboard',
            //     'route' => 'admin.dashboard',
            //     'parent_id' => NULL,
            // ]);

            $user_manage = Menu::create([
                'id' => Str::uuid()->toString(),
                'name' => 'User Management',
                'route' => NULL,
                'parent_id' => NULL,
            ]);

            $master = Menu::create([
                'id' => Str::uuid()->toString(),
                'name' => 'Data Master',
                'route' => NULL,
                'parent_id' => NULL,
            ]);

            $transaction = Menu::create([
                'id' => Str::uuid()->toString(),
                'name' => 'Transaction',
                'route' => NULL,
                'parent_id' => NULL,
            ]);

            $permission = Permission::whereName('user.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'User List',
                'route' => 'admin.users.index',
                'parent_id' => $user_manage->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('role.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Role',
                'route' => 'admin.roles.index',
                'parent_id' => $user_manage->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('permission.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Permission',
                'route' => 'admin.permissions.index',
                'parent_id' => $user_manage->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('menu.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Menu',
                'route' => 'admin.menus.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('categories.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Category',
                'route' => 'admin.categories.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('product.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Product',
                'route' => 'admin.products.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('customer.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Customer',
                'route' => 'admin.customer.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('company.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Company Management',
                'route' => 'admin.company.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('package.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Package',
                'route' => 'admin.package.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('order.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Order',
                'route' => 'admin.orders.index',
                'parent_id' => $transaction->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('outlet.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Outlet',
                'route' => 'admin.outlet.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('warehouse.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Warehouse',
                'route' => 'admin.warehouse.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('quotation.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Quotation',
                'route' => 'admin.quotation.index',
                'parent_id' => $transaction->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('procurement.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Procurement',
                'route' => 'admin.procurement.index',
                'parent_id' => $transaction->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('procurement.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Invoice',
                'route' => 'admin.invoice.all',
                'parent_id' => $transaction->id,
                'permission_id' => $permission->id
            ]);
            
            $permission = Permission::whereName('bank.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Bank',
                'route' => 'admin.banks.all',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
        }
    }
}
