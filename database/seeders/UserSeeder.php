<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $permissions = [
                'role.list',
                'role.create',
                'role.update',
                'role.delete',

                'permission.list',
                'permission.create',
                'permission.update',
                'permission.delete',

                'user.list',
                'user.create',
                'user.update',
                'user.delete',
            ];
            foreach ($permissions as $permission) {
                Permission::create(['name' => $permission]);
            }
            $roles = [
                'superadmin',
                'admin',
                'cashier'
            ];
            foreach ($roles as $role) {
                Role::create(['name' => $role]);
            }
            $role_superadmin = Role::whereName('superadmin')->first();
            $role_superadmin->givePermissionTo($permissions);
            $user = User::create([
                'name' => 'Super Admin',
                'username' => 'superadmin@robust.web.id',
                'email' => 'superadmin@robust.web.id',
                'phone' => '+6287777284665',
                'address' => 'bogor',
                'type' => 'superadmin',
                'password' => Hash::make('superadmin123'),
            ]);
            $user->assignRole('superadmin');
//            User::create([
//                'name' => 'Admin',
//                'username' => 'admin@robust.web.id',
//                'email' => 'admin@robust.web.id',
//                'phone' => '+6287777284664',
//                'address' => 'bogor',
//                'type' => 'admin',
//                'password' => Hash::make('admin123'),
//            ]);
//            $user->assignRole('admin');
//
//            User::create([
//                'name' => 'cashier',
//                'username' => 'cashier@robust.web.id',
//                'email' => 'cashier@robust.web.id',
//                'phone' => '+6287777284664',
//                'address' => 'bogor',
//                'type' => 'cashier',
//                'password' => Hash::make('cashier123'),
//            ]);
//            $user->assignRole('cashier');

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
        }


    }
}
