<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permission;
use Illuminate\Support\Str;


class StockLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createPermissions = [
            ['name'=>'stock-log.list','guard_name'=>'web'],
            ['name'=>'stock-log.create','guard_name'=>'web'],
            ['name'=>'stock-log.update','guard_name'=>'web'],
            ['name'=>'stock-log.delete','guard_name'=>'web'],
        ];

        foreach($createPermissions as $permission){
            $data = Permission::where('name', $permission['name'])
                ->where('guard_name', $permission['guard_name'])
                ->first();

            if (is_null($data)){
                Permission::create([
                    'id' => Str::uuid()->toString(),
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ]);
            }

        }

    }
}
