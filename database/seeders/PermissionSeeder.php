<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permission;

use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permissions
        $createPermissions = [
            ['name'=>'customer.list','guard_name'=>'web'],
            ['name'=>'customer.create','guard_name'=>'web'],
            ['name'=>'customer.update','guard_name'=>'web'],
            ['name'=>'customer.delete','guard_name'=>'web'],

            ['name'=>'product.list','guard_name'=>'web'],
            ['name'=>'product.create','guard_name'=>'web'],
            ['name'=>'product.update','guard_name'=>'web'],
            ['name'=>'product.delete','guard_name'=>'web'],

            ['name'=>'categories.list','guard_name'=>'web'],
            ['name'=>'categories.create','guard_name'=>'web'],
            ['name'=>'categories.update','guard_name'=>'web'],
            ['name'=>'categories.delete','guard_name'=>'web'],

            ['name'=>'order.list','guard_name'=>'web'],
            ['name'=>'order.create','guard_name'=>'web'],
            ['name'=>'order.update','guard_name'=>'web'],
            ['name'=>'order.delete','guard_name'=>'web'],
            ['name'=>'order.updateStatus','guard_name'=>'web'],

            ['name'=>'menu.list','guard_name'=>'web'],
            ['name'=>'menu.create','guard_name'=>'web'],
            ['name'=>'menu.update','guard_name'=>'web'],
            ['name'=>'menu.delete','guard_name'=>'web'],

            ['name'=>'company.list','guard_name'=>'web'],
            ['name'=>'company.create','guard_name'=>'web'],
            ['name'=>'company.update','guard_name'=>'web'],
            ['name'=>'company.delete','guard_name'=>'web'],

            ['name'=>'package.list','guard_name'=>'web'],
            ['name'=>'package.create','guard_name'=>'web'],
            ['name'=>'package.update','guard_name'=>'web'],
            ['name'=>'package.delete','guard_name'=>'web'],

            ['name'=>'delivery-order.list','guard_name'=>'web'],
            ['name'=>'delivery-order.create','guard_name'=>'web'],
            ['name'=>'delivery-order.update','guard_name'=>'web'],
            ['name'=>'delivery-order.delete','guard_name'=>'web'],

            ['name'=>'outlet.list','guard_name'=>'web'],
            ['name'=>'outlet.create','guard_name'=>'web'],
            ['name'=>'outlet.update','guard_name'=>'web'],
            ['name'=>'outlet.delete','guard_name'=>'web'],

            ['name'=>'warehouse.list','guard_name'=>'web'],
            ['name'=>'warehouse.create','guard_name'=>'web'],
            ['name'=>'warehouse.update','guard_name'=>'web'],
            ['name'=>'warehouse.delete','guard_name'=>'web'],

            ['name'=>'invoice.list','guard_name'=>'web'],
            ['name'=>'invoice.create','guard_name'=>'web'],
            ['name'=>'invoice.update','guard_name'=>'web'],
            ['name'=>'invoice.delete','guard_name'=>'web'],

            ['name'=>'procurement.list','guard_name'=>'web'],
            ['name'=>'procurement.create','guard_name'=>'web'],
            ['name'=>'procurement.update','guard_name'=>'web'],
            ['name'=>'procurement.delete','guard_name'=>'web'],

            ['name'=>'quotation.list','guard_name'=>'web'],
            ['name'=>'quotation.create','guard_name'=>'web'],
            ['name'=>'quotation.update','guard_name'=>'web'],
            ['name'=>'quotation.delete','guard_name'=>'web'],

            ['name'=>'quotation.list','guard_name'=>'web'],
            ['name'=>'quotation.create','guard_name'=>'web'],
            ['name'=>'quotation.update','guard_name'=>'web'],
            ['name'=>'quotation.delete','guard_name'=>'web'],
            
            ['name'=>'bank.list','guard_name'=>'web'],
            ['name'=>'bank.create','guard_name'=>'web'],
            ['name'=>'bank.update','guard_name'=>'web'],
            ['name'=>'bank.delete','guard_name'=>'web'],

        ];

        foreach($createPermissions as $permission){
            $data = Permission::where('name', $permission['name'])
                ->where('guard_name', $permission['guard_name'])
                ->first();

            if (is_null($data)){
                Permission::create([
                    'id' => Str::uuid()->toString(),
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ]);
            }

        }



    }
}
