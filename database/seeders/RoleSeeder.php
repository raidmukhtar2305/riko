<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::beginTransaction();
        try {
            Role::create([
                'id' => Str::uuid()->toString(),
                'name' => 'cashier',
                'guard_name' => 'web'
            ]);
            Role::create([
                'id' => Str::uuid()->toString(),
                'name' => 'admin',
                'guard_name' => 'web'
            ]);
            Role::create([
                'id' => Str::uuid()->toString(),
                'name' => 'superadmin',
                'guard_name' => 'web'
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
        }
    }
}
