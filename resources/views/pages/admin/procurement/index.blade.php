@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid">
            <!--begin::Card-->
            <div class="card">
                <div class="card-header border-0 p-6 d-flex justify-content-end">
                    <a href="{{ route('admin.procurement.create') }}" class="btn btn-primary btn-hover-scale"> <i
                            class="la la-plus"></i> Create </a>
                    <!-- <button type="button" class="btn btn-primary btn btn-primary btn-hover-scale" data-bs-toggle="modal" data-bs-target="#product-modal">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <i class="la la-plus"></i>  gi Create -->
                    {{-- </button> --}}
                </div>
                <!--begin::Card body-->
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script>
        $(document).ready(function() {
            let method = 'POST'

            //DELETE
            $(document).on('click', '.delete-btn', function() {
                // console.log($(this).data('id'))
                Swal.fire({
                    title: 'Are you sure',
                    icon: 'info',
                    showDenyButton: true,
                    confirmButtonText: 'Yes',
                    denyButtonText: `No`,
                }).then((result) => {
                    if (result.isConfirmed) {
                        const url = '{{ url('admin/procurement/destroy') }}/' + $(this).data('id')
                        $.ajax({
                            url: url,
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            }
                        }).done(response => {
                            console.log(response)
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                message: response.message
                            })
                            $('.dataTable').DataTable().ajax.reload()
                        })
                    }
                })
            })
        })





        // $(function() {

        //     var table = $('.data-table').DataTable({
        //         // paging: false,
        //         columnDefs: [{
        //             targets: 'no-sort',
        //             orderable: false
        //         }],
        //         dom: '<"row"<"col-sm-6"Bl><"col-sm-6"f>>' +
        //             '<"row"<"col-sm-12"<"table-responsive"tr>>>' +
        //             '<"row"<"col-sm-5"i><"col-sm-7"p>>',
        //         fixedHeader: {
        //             header: true
        //         },
        //         dom: 'Bfrtip',
        //         buttons: [{
        //                 extend: 'excelHtml5',
        //                 className: 'export-button',
        //                 title: 'Excel Procurement',
        //                 footer: true,
        //                 exportOptions: {
        //                     columns: [0, 1, 2, 3, 4]
        //                 }
        //             }, {
        //                 extend: 'csvHtml5',
        //                 className: 'export-button',
        //                 title: 'CSV Procurement',
        //                 footer: true,
        //                 exportOptions: {
        //                     columns: [0, 1, 2, 3, 4]
        //                 }
        //             },
        //             {
        //                 extend: 'pdfHtml5',
        //                 className: 'export-button',
        //                 title: 'PDF Procurement',
        //                 footer: true,
        //                 exportOptions: {
        //                     columns: [0, 1, 2, 3, 4]
        //                 }
        //             },

        //         ],
        //         processing: true,
        //         serverSide: true,
        //         // searching: false,
        //         ajax: {
        //             url: "{{ route('admin.procurement.index') }}",
        //             data: function(d) {
        //                 d.procurement_code = $('.searchData').val(),
        //                     d.date = $('.searchData').val(),
        //                     d.status = $('.status').val(),
        //                     d.company_name = $('.searchData').val(),
        //                     d.search = $('input[type="search"]').val()
        //             }
        //         },

        //         columns: [{
        //                 data: 'id',
        //                 render: function(data, type, full, meta) {
        //                     return meta.settings._iDisplayStart + meta.row + 1;
        //                 }
        //             },
        //             {
        //                 data: 'company_name',
        //                 name: 'company_name',
        //             },
        //             {
        //                 data: 'procurement_code',
        //                 name: 'procurement_code'
        //             },
        //             {
        //                 data: 'date',
        //                 name: 'date'
        //             },
        //             {
        //                 data: 'action',
        //                 name: 'action',
        //                 orderable: false,
        //                 searchable: false
        //             },
        //         ]
        //     });

        //     // $(".searchData").keyup(function() {
        //     //     table.draw();
        //     // });

        //     // $('.status').change(function() {
        //     //         table.draw();
        //     // });


        // });
    </script>
@endpush
