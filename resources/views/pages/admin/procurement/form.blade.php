<div class="w-100 flex-lg-row-auto w-lg-300px mb-7 me-7 me-lg-10">
    <!--begin::Procurentment details-->
    <div class="card card-flush py-4">
        <!--begin::Card header-->
        <div class="card-header">
            <div class="card-title">
                <h2>Procurement Details</h2>
            </div>
        </div>
        <!--end::Card header-->

        <!--begin::Card body-->
        <div class="card-body pt-0">
            <div class="d-flex flex-column gap-10">
                <div class="fv-row">
                    <label class="form-label">Procurement Code</label>

                    @if (data_get($procurement, 'procurement_code'))
                        <div class="fw-bold fs-3">#{{ data_get($procurement, 'procurement_code') }}</div>
                    @else
                        <div class="text-muted fs-7">The code will be assigned after the procurement process.</div>
                    @endif

                    <input type="hidden" name="procurement_code" value="{{ procurement_code() }}" />
                </div>

                <div class="fv-row">
                    <label class="form-label">Procurement Date</label>
                    <input type="date" name="date" placeholder="Select a date" class="form-control mb-2"
                        value="{{ data_get($procurement, 'date') }}" required />
                    <div class="text-muted fs-7">Set the date of the procurement to process.</div>
                </div>

                @if (Auth::user()->hasRole('superadmin'))
                    <div class="fv-row">
                        <label class="form-label">Company</label>
                        <select name="company_id" id="company_id" class="form-select mb-4" required>
                            @isset($procurement)
                                <option value="{{ $procurement->company_id }}" selected>
                                    {{ $procurement->company->name }}
                                </option>
                            @endisset
                        </select>
                        <div class="text-muted fs-7">Set the customer of the procurement to process.</div>
                    </div>
                @endif

                <div class="fv-row">
                    <label class="form-label">Warehouse</label>
                    <select name="warehouse_id" id="warehouse_id" class="form-select mb-4" required>
                        @isset($procurement)
                            <option value="{{ $procurement->warehouse_id }}" selected>
                                {{ data_get($procurement, 'warehouse.name') }}
                            </option>
                        @endisset
                    </select>
                    <div class="text-muted fs-7">Set the warehouse of the procurement to process.</div>
                </div>

                <div class="fv-row d-none" id="form-invoice-description">
                    <label class="form-label">Description</label>
                    <textarea name="invoice_description" class="form-control"></textarea>
                    <div class="text-muted fs-7">Description for your invoice</div>
                </div>

                <div class="fv-row d-none" id="form-invoice-note">
                    <label class="form-label">Note</label>
                    <textarea name="invoice_note" class="form-control"></textarea>
                    <div class="text-muted fs-7">Note for your invoice</div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column flex-lg-row-fluid gap-7 gap-lg-10">

    <div class="card card-flush py-4">

        <div class="card-header">
            <div class="card-title">
                <h2>Select Products</h2>
            </div>
        </div>

        <div class="card-body pt-0">
            <div class="d-flex flex-column gap-10">
                <div>
                    <label class="form-label">Add products to this procurement</label>
                    <div class="row row-cols-1 row-cols-xl-3 row-cols-md-2 border border-dashed rounded pt-3 pb-1 px-2 mb-5 mh-300px overflow-scroll"
                        id="kt_ecommerce_edit_order_selected_products">
                        <span class="w-100 text-muted">Select one or more products from the list</span>
                    </div>

                    <div class="fv-row">
                        <table class="table table-responsive table-rounded border gy-5 gs-5">
                            <thead>
                                <tr class="fw-semibold border-bottom">
                                    <th class="col-md-9">Name Product</th>
                                    <th class="col-md-2">Qty</th>
                                    <th class="col-md-1">Action</th>
                                </tr>
                            </thead>

                            <tbody class="product-container">

                                @foreach (data_get($procurement, 'procurement_details', []) as $procurement_detail)
                                    <tr class="product-list" data-id="{{ $procurement_detail->id }}">
                                        <td>
                                            <select name="product_id[]" class="form-select" required>
                                                <option value="{{ $procurement_detail->product_id }}" selected>
                                                    @if (Auth::user()->hasRole('superadmin'))
                                                        {{ $procurement_detail->product->company->name }} -
                                                    @endif

                                                    {{ $procurement_detail->product_name . ' - ' . number_format($procurement_detail->price, 2) }}
                                                </option>
                                            </select>
                                        </td>

                                        <td>
                                            <input type="number" name="product_qty[]"
                                                value="{{ $procurement_detail->qty }}" min="1"
                                                class="form-control" placeholder="Qty" required>
                                        </td>

                                        <td class="btn-group">
                                            <a class="btn btn-sm btn-success add-product-list">
                                                <span class="fa-solid fa-plus"></span>
                                            </a>

                                            <a class="btn btn-sm btn-danger remove-product-list hide">
                                                <span class="fa-solid fa-minus"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr class="product-list">
                                    <td>
                                        <select name="product_id[]" class="form-select" required></select>
                                    </td>

                                    <td>
                                        <input type="number" name="product_qty[]" min="1" class="form-control"
                                            placeholder="Qty" required>
                                    </td>

                                    <td class="btn-group">
                                        <a class="btn btn-sm btn-success add-product-list">
                                            <span class="fa-solid fa-plus"></span>
                                        </a>

                                        <a class="btn btn-sm btn-danger remove-product-list hide">
                                            <span class="fa-solid fa-minus"></span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="separator"></div>
            </div>
        </div>
    </div>

    <div class="card card-flush py-4">
        <div class="card-header">
            <div class="card-title">
                <h2>Delivery Details</h2>
            </div>
        </div>

        <div class="card-body pt-0">
            <div class="fv-row">
                <label class="form-label mt-2">Description</label>
                <textarea class="form-control mb-2" name="description" rows="3">{{ data_get($procurement, 'description') }}</textarea>
            </div>
        </div>
    </div>

    <input type="hidden" name="procurement_detail_deleted">

    <div class="d-flex justify-content-end">
        <a href="{{ route('admin.procurement.index') }}" class="btn btn-light me-5">Cancel</a>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function() {

            generateSelectOptionCompanies();
            generateSelectOptionWarehouses();
            generateProductValidation();

            function generateProductValidation() {
                $.each($(".product-list"), function(indexInArray, valueOfElement) {
                    $(this).find('.product-count').text(indexInArray + 1);

                    $(this).find('[name*="product_id"]').attr('name', 'product_id[' + indexInArray + ']');
                    $(this).find('[name*="product_qty"]').attr('name', 'product_qty[' + indexInArray + ']');
                });

                generateSelectOptionProducts();

                if (parseInt($(".product-list").length) > 1) {
                    $(".product-list .remove-product-list").removeClass('disabled');
                    $(".product-list .add-product-list").addClass('disabled');
                    $(".product-list:last .add-product-list").removeClass('disabled');
                } else {
                    $(".product-list .remove-product-list").addClass('disabled');
                    $(".product-list .add-product-list").removeClass('disabled');
                }
            }

            function generateSelectOptionProducts() {
                $('[name*="product_id"]').select2({
                    placeholder: "Choose product...",
                    ajax: {
                        url: "{{ route('admin.products.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });
            }

            function generateSelectOptionCompanies() {
                $('[name*="company_id"]').select2({
                    placeholder: "Choose company...",
                    ajax: {
                        url: "{{ route('admin.company.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });
            }

            function generateSelectOptionWarehouses() {
                $('[name*="warehouse_id"]').select2({
                    placeholder: "Choose warehouse...",
                    ajax: {
                        url: "{{ route('admin.warehouse.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            console.log(params.term)
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });
            }

            $(document).on('click', '.add-product-list', function(e) {
                e.preventDefault();

                $('[name*="product_id"]').select2('destroy');
                var c = $(this).parents('.product-list').clone();
                c.find('input').val("").attr('class', 'form-control').removeAttr("aria-describedby");
                c.find('.invalid-feedback').remove();
                $('.product-container').append(c);
                generateProductValidation();
            });

            $(document).on('click', '.remove-product-list', function(e) {
                e.preventDefault();
                if ($('.product-container .product-list').length > 1) {
                    var c = $(this).parents('.product-list');
                    let procurement_detail_deleted_id = c.data('id');
                    if (procurement_detail_deleted_id) {
                        let procurement_detail_deleted = $('[name*="procurement_detail_deleted"]').val();
                        $('[name*="procurement_detail_deleted"]').val(
                            procurement_detail_deleted_id + ',' + procurement_detail_deleted
                        );
                    }
                    c.remove();

                    generateProductValidation();
                }
            });

            $(document).on('submit', '#procurement-form', function(e) {
                e.preventDefault()

                const payload = $(this).serialize()
                const url = $(this).attr('action')
                const validator = document.getElementById('procurement-form').checkValidity()

                if (validator) {
                    $.ajax({
                            url: url,
                            method: 'POST',
                            data: payload
                        })
                        .done(response => {
                            if (response.success) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: response.message
                                })
                                window.location.href = "{{ route('admin.procurement.index') }}";
                            }
                        })
                        .fail(response => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.message
                            })
                        })
                }
            })
        });
    </script>
@endpush
