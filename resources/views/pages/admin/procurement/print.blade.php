<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Procurement | {{$procurement->procurement_code}}</title>
    <link rel="shortcut icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHsAAAB7CAMAAABjGQ9NAAAAnFBMVEXj5ufqVR7i5ef////i5eawt73L0dj29/fqRADr7e7m6er8/PzrUhXI2uPnw7zU2d7jb1Lj7O7rUA24v8XpSwDpWSnrWC71wLT36+ntbFL98vDwjXToOwD50cnzqpvsZTzsZ0T75uLrb03wknvrXzLDyMzn3dzqgGnprKHnz8zufmDvnIzxpJLzt6vtd1r52tTrXELnLQDekoben5SvOI07AAAKYklEQVRogb1bC3vithIVYyXGNjcphJQtIdm8tmm3m7S99///t2s95ikZsllYfRHIBnw0M2eOHnZc0zjXNN4NjYOm6UMbxrYP7a53HyqXbeeHpvdtE9678V22qbpm6Puh8f0w9D61Hbad/xh0f37m3HhtEHhOYuK7q6GGdijwUeyzdi9qaitsab0n5PEtOACdUGvrGrCvencYOyFBQAVhPcF+pIzYZ1dDRALRA7A+H7nluQaO+ZFz4BEZAMJLPKA2TJ1PxwH77MpFjg3NJO9cQAREze9j7T362nnh/Air2lC2+4gdLM+ofZ3tKt5ADB8+6O3Y14wdYr4/3kPJ8BhvYEJFl45HPlfVBt0ODkDswHZEr1bDcCe5Vho/5Q4+753A3s/2Mr8T230KNkS7ZVrZHpi2iPeZYHur2Z6qg8w1w/bMMnFxZBMi1Nr5e4wt2a651o/YnFeO2B5UzQtbgI0KSaTSKwacWrEIuxPb20qO7dFz4U+YDvNEkdh72O76iCRRI/O9hfPUEeqSbkMt3nvZzlxziu3kQcAgis54ir/khM/f985gJ8t9UV1lBMv5DT7bQ/E+MKiiD6zdyHZZQes5CD13RCySdeVippZgeI4LWLsz2yt6nplOs5ecY2wwjhU0VNReueVLu2tsDz6Xo6fHthu8MEu+7/E3UPBL7CrbpZ6rtrgsIMNAu7jahpLnzHYZ94Hy2wm2j5WwKyk+De0hEbKKfdVbpjtXzFiSnkP2X5ItAOIb5AE95QC3uSN1u0u2F3MHID3PbAMwdpdKTp+m/k5go+VDzLDeRz0HwfDxGEJbelP8QWmlHEbyy2Ud++yqpVHM6nlieJq98IULdS2DT4dZe2AC27A963nBdj1cg4JUdssxjIj+ZQpcsX388jA4yfCwGoLBe7bGKrnUc6DOkObCHsMV25XFdgz14uo4VABQB4C6oMadUKYirtnO7NZ6LtQaOABgX7PqF4MMXE2CR22HpOfR50nPaQXakEOZ5PkUjW6iO6CasSP95ZcvV9XyZVDzcxDjd2w3gt1a2MU5zUBUG/LBOA6fV0un4j0IhqfqmE5OQJCis7XC3VJhUquKDZHpUc9dZrhLDEc9ZwbzpXlxWtLLpiBMgkNbma+B1HPNq0g4IhbQoAY6GVTIoxpXfX5Izy2ZKY2RaIrbAjz32qdfVSxv8zyCfT4khqPPgR3Oyc2RVkxIExrVMS4lOGp6ZT02ths1d1Bxlfy3Sq9/gSEowKGi5wO2O9JzMBcUbfSJPO+11meHWfCu60eW963D3a04krK2IFm81SzmOYhIAK3fbPJVwNuanpv5WgogAvBExoOeuSBG1ry4Wkf1zx2dyO9Sz33ScwgXAJ1rqSXWiUpwvQoByIBJ7EvU84k10VS8kccy4uR5iro5Ebqr4m33W9KMpUntBqR91m6EAJDQep1mJVeA63hT7fBdpxZQ1OXlypGbuEHmcucZHCLLY7ztXDG244W8+LmMngIQB7xZ4eyoJhWOtGVqvob6ANKTKYm8mKvg7MWx3EuLaRIVPxd6PtT0nNeCRtEYPR6x4NNsDX2D8gq8lnRyVBN6XtHUoReDQqmcgk5mSC06rE97ralFdun1GF9Amg21Hqnv0wJSDwS+1HPJ8B71HFS20Diq87q2BeB0KigXea3n1bW/3E3z/MZX9nLri1OMRtFicYGcLPQcFNvHXqVLx/FfTlNinch1dK9xQe4RDgEj2w/qOfVZbNLitZC3colaZnv+gt3BGE8Weq53+Lzjy1pVB7ou90hTqkIJcaat7a8BrsfC/NyLK6oZKmEYKlZsLzsS/Cbi3XUhzqP/l7HdLMP7su/7sS6X8W98WcbjPh/Ek+kvnk0nw3s6yF8M7aKrrhd6HhneNL9f3N5eUD1a8UvecMzEVfkNo6Uvu/lpyh9LHQ+fdh+ynof6sp6dqMwvNE087vPlnO7/XJwKerZ4Wzpd+H5JWBvAxcnMHstS2S3i3bXtqN+389NBr2TAIeS31vPTYwuzzXztlD7P2IAjTT8i92K/5dTYcmyj/Za8/v4JPqeZHOl5iHcPp8YGrS0Y77br2mH5E+JNVJd6PhHvVS4/rDop3kx1kd9hflb6fDW7yeXlerfCk7t1LvPdXPRpTefHsrNmRJ/zzMm3XWI66rnFXjwODZWHp/Tp6uuWzm03L4S++quR5duqwOa51ri8snpufb5+VZe7TWcf1MmHt/yj60Gdb+4XBbaaefB8Le7XW+z5Rl8u+uX6kz7ZPKVf/arPbp9Lu2U5pOcR+2ETyt/xgjerjP0QyzaZerMm7IHKU8XnYjTxpOeQdo6r2N/idGIXQ//3LmP/s4vlPjpmuEbs7Wf6aYVrDrnmcPzm+VqR3xH7Lp1cfw0wiP1bCuZi/hLObuaEPZmLaDeCi/yu6rnEnu0eEoEk9viVSMf1O7Epy8YZ2wE919gh5I8WezYLGReCm7FTmbabNdWJ8Xsi3og9/1Szeza/HQ9f1xn77T6V50Ih0W5cDpKet0HP226fz+dPlXiPZX0Xkm9tcmy4MTRHXRN6Hpje7o/3t38Do/+9aSTPGXv1VMNuBut2oS1A+T1M63nE3qZUjlf8uijt/vYd2Lw44/ye0HOra6+kaybeG8T+lMvDWw1brNeMnpfzNYOdIm99HjzyF/L8cXYdy8KGW+t5eN5Jr8fq8U4Sud1u3pJXTH4HqjXPi3drC3Kd9DyyvKvr+ev941g+z3GwVNjr35CC79UWHMBTZnX743231moRse/z3CEmXhOD+y67MeL+4HxNaYvCfr0LZbNlCr7b53n/47v0XGHLsuHx+312p7CTntfzO85bLPZMYw/4uR5Dp+0GXBNhfkN1PbZ43I5pY0xZ3Qng7es9/ma1IRdMYvMe2UE9n62ebz4XmTp/w8nrzeNcTFTnL/u2DtjniW99xtyzHqsNhwuatC8OftdgOzGQZT3vftL6m58Q0XoOP239zeN3Pz1fOwl2ZT1W0/OTYJPh+/X8+NisLYf0/JhlsXRiY93m91Dm9xGhPy+F0TG/Bzlf878/n2xzb/1fE2+h5zG/h4v1enGSsr4/Z5anrVx7v2T559Mvj1Tefjla+d/5ORlc1/Oxtm3TUP3P0cp5wubNfG/03P6/gSsekPiBcilv34DV86IOR4VWLg8+F/lduR96ebQCNI44nKaSnsf8zmzntjtmETfWzHwN75fQs+/pWclKSb+ufeDrHyC0eoDf6nlRP/hvTFWTJdVCy+o5xxvoWWhP/81Bv1Vdsv8CoW9WenzuA/gp8nQvfyjzW9Vj2S16Q7cnK/lt74cCeEgPGMTYRTmEbIYPT7/EL+S/9EYfhgNAg9FujDrtn3eW4al9VLv1Dc1Szw3bQ7Sk4co8OlCOcGSrpx+w0/fMz221t15/qHAXYqs7EG9PESLvM+f5FcSHFc4DWwv4vHhYiO7V8/a4AZce8N1kfrP1jmMMIswF822M8/djy/HdeXII/69Dnq91ec+L2kN/FMvtHfg0ig0Yb2Q4gNDzqHDeocmC0N/Fdmegvfpfpql4h/rjViM4Dt2dvP7/AQRx6EPhAVvDAAAAAElFTkSuQmCC">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        * {
            font-size: 12px;
        }
        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .mt-3 {
            margin-top: 15px;
        }

        .font-weight-normal {
            margin-left: 20px;
        }

        table thead {
            background-color: #ececec;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        #logo {
            float: left;
        }

        #logo img {
            height: 40px;
        }

        #text-header p {
            color: #304a68;
            margin-bottom: 1px;
            /* margin-left: 50px; */
            margin-top: 20px;
            text-align: left;
            font-size: 42px;
            line-height: 90%;
            font-weight: bold;
        }

        .text-footer p {
            color: #304a68;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            margin-top: 20px;
            text-align: center;
            font-size: 22px;
            line-height: 90%;
            font-weight: bold;
        }

        #text-company p {
            color: #616568;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            /* margin-top: 30px; */
            text-align: left;
            font-size: 30px;
            line-height: 90%;
        }

        .text-date {
            color: #1c1f21;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            margin-top: 10px;
            text-align: right;
            font-size: 15px;
            line-height: 90%;
            font-weight: bold;
        }

        .text-customer {
            color: black;
            font-size: 15px;
            line-height: 50%;
        }

        .row {
            margin-top: 20px;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        td{
            vertical-align: text-top !important;
        }
        header {
                display: block;
                top: -10px;
            }
        footer {
            position: fixed;
            bottom: 7px;
            left: 0px;
            right: 0px;
            text-align: center;
        }
        .container{
            margin-top: 20px !important;
        }
        .d-block{
            display: block;
        }
    </style>
</head>

<body>
    <div class="container">
        @if (request()->route()->getName() == 'admin.procurement.print-preview')
            <a class="dropdown-item mb-2" href="{{ route('admin.procurement.print', $procurement->id) }}">Download Procurement</a>
        @endif

        <header>
            <div id="text-company">
                <p> {{ $procurement->company->name }} </p>
            </div>
        </header>


        <hr style="border:1px solid">
        <div class="">

            <table  style="width: 100%">
                <tr>
                    <td>
                        <div class="text-left align-top">
                            <p class="mr-2">Detail Procurement :</p>
                            {{-- <p class="text-customer"><b> Note : {{$procurement->description}}</b></p> --}}
                            <p class="text-customer"><b> Date : {{ date('l, j F Y', strtotime($procurement->date)) }}</b></p>
                            <p class="text-customer"><b> #{{$procurement->procurement_code}}</b></p>
                        </div>
                    </td>
                </tr>
            </table>


            <div class="">
                <div class="cart">
                    <div class="mt-3">
                        <p> List Product</p>
                        <table border="1" cellspacing="0" cellpadding="8" class="table table-bordered" style="max-height: 100%" >
                            <thead>
                                <tr>
                                    <th>Name Product</th>
                                    <th>QTY</th>
                                    {{-- <th>Price</th>
                                    <th>Description</th>
                                    <th>Subtotal</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($procurement->procurement_details as $key => $value)
                                <tr>
                                    <td><b>{{ $value->product_name }}</b></td>
                                    <td class="text-end">{{ $value->qty }}</td>
                                    {{-- <td class="text-end">Rp. {{ number_format($value->price) }}</td>
                                    <td class="text-end">{{ $value->product_description }}</td>
                                    <td class="text-end">Rp. {{ number_format($value->price * $value->qty) }}</td> --}}
                                </tr>
                            @endforeach
                            @if (request()->route()->getName() == 'admin.procurement.print-preview')
                                @if (count($procurement->procurement_details) < 5)
                                    <tr style="height: 200px; overflow-y: auto;">
                                        <td></td>
                                        <td></td>
                                        {{-- <td></td>
                                        <td></td>
                                        <td></td> --}}
                                    </tr>
                                @endif
                            @endif
                            {{-- <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="1" class="total"> <b>Total</b> </td>
                                <td class="total"><b>Rp. {{ number_format($procurement->total)}}</b> </td>
                            </tr > --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="cart">
                    <table  style="width: 100%">
                        <tr>
                            <td>
                                <div class="text-left align-top">
                                    <p class="text-customer"><b> Description : {{$procurement->description}}</b></p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
</body>

</html>
