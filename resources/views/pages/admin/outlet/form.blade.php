<!--begin::Modal-->
<div class="modal fade" tabindex="-1" id="outlet-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        <form method="post" action="{{ route('admin.outlet.store') }}" class="form needs-validation" id="outlet-form"
            autocomplete="off" novalidate>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form {{ $view }}</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Name</label>
                        <input type="text" class="form-control form-control-solid" name="name" placeholder="Name" required/>
                    </div>

                    @if (Auth::user()->hasRole('superadmin'))
                        <div class="mb-10 fv-row">
                            <label for="company_id" class="required form-label">Company</label>
                            <select name="company_id" id="company_id"
                                class="form-control form-control-solid form-select mb-2">
                                <option value="" disabled selected>--Choose option--</option>
                                @foreach ($companies as $company)
                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="mb-10 fv-row">
                        <label for="warehouse_id" class="required form-label">Warehouse</label>
                        <select name="warehouse_id" id="warehouse_id" required
                            class="form-control form-control-solid form-select mb-2">
                            <option value="" disabled selected>--Choose option--</option>
                            @foreach ($warehouses as $warehouse)
                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Address</label>
                        <input type="text" class="form-control form-control-solid" name="address"
                            placeholder="Address" required />
                    </div>


                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Phone</label>
                        <input type="number" class="form-control form-control-solid" name="phone" placeholder="Phone"
                            required />
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
