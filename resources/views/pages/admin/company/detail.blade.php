@extends('layouts.admin.app')
@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <!--begin::Post-->
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Order details page-->
        <div class="d-flex flex-column gap-7 gap-lg-10">
            <!--begin::Order summary-->
            <div class="d-flex flex-column flex-xl-row gap-7 gap-lg-10">
                <!--begin::Order details-->
                <div class="card card-flush py-4 flex-row-fluid">

                    <div class="card-body pt-0">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-2">Company Detail</h3>
                                </div>

                                <div class="col-md-6">
                                    <div class="d-flex justify-content-end">
                                        <!-- <button class="btn btn-md-primary" onclick="history.back()">Back</button> -->
                                        <!-- <a href="#"  class="btn btn-white">White</a> -->
                                        <a href="#" onclick="history.back()" class="btn btn-primary">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                            <!--begin::Table body-->
                            <tbody class="fw-semibold text-gray-600">
                                <!--begin::Customer name-->
                                <tr>
                                    <td class="text-muted">
                                        <div class="d-flex align-items-center">Company Name
                                        </div>
                                    </td>
                                    <td class="fw-bold text-end">
                                        <div class="d-flex align-items-center justify-content-end">
                                            <!--begin::Name-->
                                            <a href="#"
                                                class="text-gray-600 text-hover-primary">{{ $detail->name }}</a>
                                            <!--end::Name-->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-muted">
                                        <div class="d-flex align-items-center">Email
                                        </div>
                                    </td>
                                    <td class="fw-bold text-end">
                                        <div class="d-flex align-items-center justify-content-end">
                                            <!--begin::Name-->
                                            <a href="#"
                                                class="text-gray-600 text-hover-primary">{{ $detail->email }}</a>
                                            <!--end::Name-->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-muted">
                                        <div class="d-flex align-items-center">Address
                                        </div>
                                    </td>
                                    <td class="fw-bold text-end">
                                        <div class="d-flex align-items-center justify-content-end">
                                            <!--begin::Name-->
                                            <a href="#"
                                                class="text-gray-600 text-hover-primary">{{ $detail->address }}</a>
                                            <!--end::Name-->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-muted">
                                        <div class="d-flex align-items-center">Company Description
                                        </div>
                                    </td>
                                    <td class="fw-bold text-end">
                                        <div class="d-flex align-items-center justify-content-end">
                                            <!--begin::Name-->
                                            <a href="#"
                                                class="text-gray-600 text-hover-primary">{{ $detail->description }}</a>
                                            <!--end::Name-->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <!--end::Table body-->
                        </table>
                    </div>
                    <!--end::Card body-->
                </div>

                <!--end::Order details-->
                <!--end::Customer details-->
            </div>
            <!--end::Order summary-->
            <!--begin::Tab content-->
            <!--end::Tab content-->
        </div>

        <ul class="nav nav-tabs mt-7" id="myTab" role="tablist">
            <li class="nav-item border-0" role="presentation">
                <button class="nav-link active border-0 px-10 py-5" id="packages-tab" data-bs-toggle="tab"
                    data-bs-target="#packages-tab-pane" type="button" role="tab" aria-controls="packages-tab-pane"
                    aria-selected="true">Packages</button>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active bg-white p-4 card rounded-top-0" id="packages-tab-pane"
                role="tabpanel" aria-labelledby="packages-tab" tabindex="0">
                <div class="card-header border-0 pt-3 d-flex justify-content-end">
                    <button type="button" class="btn btn-primary btn-primary btn-hover-scale" data-bs-toggle="modal"
                        data-bs-target="#company-package-modal">
                        <i class="la la-plus"></i> Add Package
                    </button>
                </div>
                <div class="card-body">
                    <div class="border rounded">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="ps-5">Package</th>
                                    <th>Expire Date</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($detail->company_packages->sortByDesc('created_at') as $companyPackage)
                                    <tr>
                                        <td class="ps-5">{{ @$companyPackage->package->name }} ({{ @$companyPackage->package->summary_days }} days)</td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($companyPackage->expire_date)->format('d F Y') }}
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($companyPackage->created_at)->format('d F Y') }}</td>
                                        <td>
                                            <button class="btn btn-sm btn-primary delete-btn"
                                                data-id="{{ $companyPackage->id }}" type="button">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="100" class="ps-5">You have not assigned any packages to this company yet.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Order details page-->
    </div>

    <!--begin::Modal-->
    <div class="modal fade" tabindex="-1" id="company-package-modal" data-bs-backdrop="static" data-bs-keyboard="false">
        <div class="modal-dialog modal-dialog-scrollable">
            <form method="post" action="{{ route('admin.company.packages.store', $detail->id) }}"
                class="form needs-validation" id="company-form" autocomplete="off" novalidate>
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Form {{ $view }}</h5>

                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                            aria-label="Close">
                            <span class="svg-icon svg-icon-2x"></span>
                        </div>
                        <!--end::Close-->
                    </div>

                    <div class="modal-body">

                        <div class="mb-10 fv-row">
                            <label class="form-label">Package</label>
                            <select name="package_id" id="select-package" class="form-select"
                                data-control="select2" data-placeholder="Select Packages">
                                <option disabled selected>Select Package</option>
                                @foreach($packages as $package)
                                    <option value="{{ $package->id }}">{{ $package->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--end::Modal-->
    <!--end::Post-->
</div>
<!--end::Container-->
@endsection

@push('scripts')
    <script>
        $(document).on('click', '.delete-btn', function () {
            Swal.fire({
                title: 'Are you sure',
                icon: 'info',
                showDenyButton: true,
                confirmButtonText: 'Yes',
                denyButtonText: `No`,
            }).then((result) => {
                if (result.isConfirmed) {
                    const url = `{{ url("admin/company") }}/{{ $detail->id }}/packages/${$(this).data('id')}`
                    $.ajax({
                        url: url,
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    }).done(response => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            message: response.message
                        })
                        setTimeout(() => {
                            window.location.reload()
                        }, 500);
                    })
                }
            })
        })

    </script>
@endpush
