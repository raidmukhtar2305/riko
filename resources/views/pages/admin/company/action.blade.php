<div class="dropdown">
    <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown"
        aria-expanded="false">
        Action
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">
        @if (Auth::user()->hasRole('superadmin'))
        <li><a class="dropdown-item" data-id="{{ $id }}" href="{{ route('admin.company.show-page', $id) }}">Show</a></li>
        @endif
        <li><a class="dropdown-item edit-btn" href="#" data-id="{{ $id }}">Edit</a></li>
        @if (Auth::user()->hasRole('superadmin'))
        <li>
            <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item delete-btn" data-id="{{ $id }}" href="#">Delete</a></li>
        @endif
    </ul>
</div>

