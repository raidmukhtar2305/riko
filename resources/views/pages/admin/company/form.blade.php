<!--begin::Modal-->
<div class="modal fade" tabindex="-1" id="company-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        <form method="post" action="{{ route('admin.company.store') }}" class="form needs-validation" id="company-form"
            autocomplete="off" novalidate>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form {{ $view }}</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">

                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="d-block fw-semibold fs-6 mb-5">Logo Input</label>
                        <!--end::Label-->

                        <!--begin::Image input-->
                        <div class="image-input image-input-outline image-input-empty" data-kt-image-input="true"
                            style="background-image: url('{{ asset('assets/media/avatars/blank.png') }}')">
                            <!--begin::Preview existing avatar-->
                            <div class="image-input-wrapper w-125px h-125px"></div>
                            <!--end::Preview existing avatar-->

                            <!--begin::Label-->
                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                <i class="bi bi-pencil-fill fs-7"></i>

                                <!--begin::Inputs-->
                                <input type="file" name="logo" id="image" accept=".png, .jpg, .jpeg" />
                                <input type="hidden" name="avatar_remove" />
                                <!--end::Inputs-->
                            </label>
                            <!--end::Label-->

                            <!--begin::Cancel-->
                            <span
                                class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow btn-cancel-img"
                                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <!--end::Cancel-->

                            <!--begin::Remove-->
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <!--end::Remove-->
                        </div>
                        <!--end::Image input-->

                        <!--begin::Hint-->
                        <div class="form-text">Allowed file types: png, jpg, jpeg.</div>
                        <!--end::Hint-->
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Company Name</label>
                        <input type="text" id="name" class="form-control form-control-solid" name="name"
                            placeholder="Name" required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Email</label>
                        <input type="email" class="form-control form-control-solid" name="email" placeholder="Email"
                            required />
                    </div>



                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Address</label>
                        <input type="text" class="form-control form-control-solid" name="address"
                            placeholder="Address" required />
                    </div>


                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Phone</label>
                        <input type="number" pattern="[0-9]" class="form-control form-control-solid" name="phone"
                            placeholder="Phone" required onkeypress="return hanyaAngka(event)" />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="bank_name" class="required form-label">Bank Name</label>
                        <input type="text" class="form-control form-control-solid" name="bank_name"
                            placeholder="Bank Name" required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="account_name" class="required form-label">Account Name</label>
                        <input type="text" class="form-control form-control-solid" name="account_name"
                            placeholder="Account Name" required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="account_number" class="required form-label">Account Number</label>
                        <input type="text" class="form-control form-control-solid" name="account_number"
                            placeholder="Account Number" required onkeypress="return hanyaAngka(event)" />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Company Description</label>
                        <input type="text" class="form-control form-control-solid" name="description"
                            placeholder="Description" />
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(function() {

        $('#txtNumeric').keydown(function(e) {

            if (e.shiftKey || e.ctrlKey || e.altKey) {

                e.preventDefault();

            } else {

                var key = e.keyCode;

                if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >=
                        65 && key <= 90))) {

                    e.preventDefault();

                }

            }

        });

    });

    function hanyaAngka(event) {
        var angka = (event.which) ? event.which : event.keyCode
        if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
            return false;
        return true;
    }
</script>
<!--end::Modal-->
