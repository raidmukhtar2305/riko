<div class="modal fade" tabindex="-1" id="invoice-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        <form method="post" action="{{ route('admin.invoice.store') }}" class="form needs-validation" id="invoice-form"
            autocomplete="off" novalidate>
            @csrf

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form {{ $view }}</h5>

                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                </div>

                <div class="modal-body">
                    <div class="mb-10 fv-row">
                        <label for="date" class="required form-label">Date</label>
                        <input type="date"
                            min="{{ \Carbon\Carbon::parse($order->date)->format('Y-m-d') }}"
                         class="form-control form-control-solid" name="date" placeholder="Date"
                            required />
                    </div>

                    <div class="mb-10 fv-row">
                        <div class="d-flex justify-content-between">
                            <label for="nominal" class="required form-label">Nominal</label>
                            <label for="nominal" class="required form-label" id="remaining-label"><i class="text-danger">Remaining: {{ $remaining }}</i></label>
                        </div>
                        <input type="hidden" value="{{ $remaining }}" id="remaining">
                        <input type="number" class="form-control form-control-solid" name="nominal" id="nominal"
                            placeholder="Nominal" required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="description" class="form-label">Description</label>
                        <textarea class="form-control form-control-solid" name="description" placeholder="Description" id="description"></textarea>
                    </div>

                    {{-- <div class="mb-10 fv-row">
                        <label for="note" class="form-label">Note</label>
                        <textarea class="form-control form-control-solid" name="note" placeholder="note" id="note"></textarea>
                    </div> --}}
                </div>

                <input type="hidden" name="order_id" value="{{ request()->get('orderId') }}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
