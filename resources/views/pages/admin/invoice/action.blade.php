<div class="dropdown">
    <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown"
        aria-expanded="false">
        Action
    </button>

    <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">
        {{-- @dd($invoice) --}}
        <li><a class="dropdown-item" href="{{ route('admin.invoice.show', $invoice->id) }}"> <i class="bi bi-eye"></i> Show</a></li>
        <li><a class="dropdown-item edit-btn" href="#" data-id="{{ $invoice->id }}">Edit</a></li>
        @if ($invoice->status == 0)
            <li><a class="dropdown-item status-btn" href="#" data-status="1" data-id="{{ $invoice->id }}" ><i class="bi bi-check-circle"></i> Lunas</a></li>
        @else
            <li><a class="dropdown-item status-btn" href="#" data-status="0" data-id="{{ $invoice->id }}" ><i class="bi bi-x-circle"></i> Belum Lunas</a></li>
        @endif
        <li>
            <a class="dropdown-item" href="{{ route('admin.invoice.print-preview', $invoice->id) }}" target="_blank">  <i class="fa fa-download" aria-hidden="true"></i> Preview Print Invoice</a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('admin.invoice.print', $invoice->id) }}"> <i class="fa fa-download" aria-hidden="true"></i> Download Invoice</a>
        </li>
        <li>
            <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item delete-btn" data-id="{{ $invoice->id }}" href="#">Delete</a></li>
    </ul>
</div>

