@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid">
            <!--begin::Card-->
            <div class="card">
                <div class="card-header border-0 pt-6 d-flex justify-content-end">
                    <button type="button" class="btn btn-primary btn btn-primary btn-hover-scale" data-bs-toggle="modal"
                        data-bs-target="#invoice-modal">
                        <i class="la la-plus"></i> Create
                    </button>
                </div>
                <!--begin::Card body-->
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->

    @include('pages.' . $path . '.' . $view . '.form')
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script>
        $(document).ready(function() {
            let method = 'POST'

            //STORE
                //STORE
                $('#invoice-form').submit(function(e) {
                e.preventDefault()
                const payload = new FormData(this)
                const url = $(this).attr('action')
                const validator = document.getElementById('invoice-form').checkValidity()

                if (method != 'POST')
                    payload.append('_method', 'PUT');

                if (validator) {
                    $.ajax({
                            url: url,
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            method: method,
                            method: 'POST',
                            data: payload,
                            async: false,
                            cache: false,
                            contentType: false,
                            enctype: 'multipart/form-data',
                            processData: false,
                        })
                        .done(response => {
                            if (response.success) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: response.message
                                })

                                $('.dataTable').DataTable().ajax.reload()
                                $('#invoice-modal').modal('hide')
                            }
                        })
                        .fail(response => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.message
                            })
                        })
                }
            })

            //DELETE
            $(document).on('click', '.delete-btn', function() {
                Swal.fire({
                    title: 'Are you sure',
                    icon: 'info',
                    showDenyButton: true,
                    confirmButtonText: 'Yes',
                    denyButtonText: `No`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        const url = '{{ url('admin/invoice') }}/' + $(this).data('id')
                        $.ajax({
                            url: url,
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            }
                        }).done(response => {
                            console.log(response)
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                message: response.message
                            })
                            $('.dataTable').DataTable().ajax.reload()
                        })
                    }
                })
            })

            //EDIT
            $(document).on('click', '.edit-btn', function() {
                const url = '{{ url('admin/invoice/get-detail') }}/' + $(this).data('id')
                $('#invoice-form').attr('action', '{{ url('admin/invoice') }}/' + $(this).data('id'))
                method = 'PUT'

                let data = {}
                $.get(url).done(response => {
                    if (response.success) data = response.data
                    setForm(data)
                    $('#invoice-modal').modal('show')
                })
            })

            function setForm(data) {
                $('[name=order_id]').val(data.order_id)
                $('[name=date]').val(data.date)
                $('[name=nominal]').val(data.nominal)
                $('[name=note]').val(data.note)
                $('[name=description]').val(data.description)
            }

            $('.modal').on('hidden.bs.modal', function(event) {
                $('#invoice-form').attr('action', '{{ route('admin.invoice.store') }}')
                method = 'POST'
            })

            // update status
            $(document).on('click', '.status-btn', function() {
                Swal.fire({
                    title: 'Are you sure',
                    icon: 'info',
                    showDenyButton: true,
                    confirmButtonText: 'Yes',
                    denyButtonText: `No`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        const url = '{{ url('admin/invoice/update-status') }}/' + $(this).data('id')
                        console.log(url);
                        let payload = {}
                        payload.status = $(this).data('status')
                        $.ajax({
                            url: url,
                            method: 'PUT',
                            data: payload,
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            }
                        }).done(response => {
                            console.log(response)
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                message: response.message
                            })
                            $('.dataTable').DataTable().ajax.reload()
                        })
                    }
                })
            })
        })

        $('#nominal').keyup(function() {
            var remaining = $('#remaining').val();
            var nominal = $('#nominal').val();

            console.log('remaining: ' + parseInt(remaining));
            console.log('nominal: ' + nominal);

            if (!isNaN(remaining) && !isNaN(nominal)) {
                var result = remaining - nominal;
                if (result < 0) {
                    result = 0;
                    $('#remaining-label').text('Nominal melebihi remaining');
                } else {
                    $('#remaining-label').text('Remaining: ' + result);
                }
            } else {
                $('#remaining-label').text('Invalid input. Please enter valid numbers.');
            }
        });
    </script>
@endpush
