@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Order details page-->
            <div class="d-flex flex-column gap-7 gap-lg-10">
                <!--begin::Order summary-->
                <div class="d-flex flex-column flex-xl-row gap-7 gap-lg-10">
                    <!--begin::Order details-->
                    <div class="card card-flush py-4 flex-row-fluid">
                        <div class="card-body pt-0">
                            <div class="card-title">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="mt-2">Invoice Detail</h3>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="d-flex justify-content-end">
                                            <!-- <button class="btn btn-md-primary" onclick="history.back()">Back</button> -->
                                            <!-- <a href="#"  class="btn btn-white">White</a> -->
                                            <a href="#" onclick="history.back()" class="btn btn-primary">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">
                                        <tr>
                                            <td class="text-muted d-flex align-items-center">Company</td>
                                            <td class="fw-bold text-end">{{ $invoice->company->name }}</td>
                                        </tr>

                                        <tr>
                                            <td class="text-muted d-flex align-items-center">Order</td>
                                            <td class="fw-bold text-end">{{ $invoice->order->order_code }}</td>
                                        </tr>

                                        <tr>
                                            <td class="text-muted d-flex align-items-center">Reference</td>
                                            <td class="fw-bold text-end">{{ $invoice->reference }}</td>
                                        </tr>

                                        <tr>
                                            <td class="text-muted d-flex align-items-center">Date</td>
                                            <td class="fw-bold text-end">{{ $invoice->date }}</td>
                                        </tr>

                                        <tr>
                                            <td class="text-muted d-flex align-items-center">Nominal</td>
                                            <td class="fw-bold text-end"> Rp. {{ number_format($invoice->nominal) }}</td>
                                        </tr>

                                        <tr>
                                            <td class="text-muted d-flex align-items-center">Description</td>
                                            <td class="fw-bold text-end">{{ $invoice->description }}</td>
                                        </tr>
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Order details-->
                    <!--end::Customer details-->
                </div>
                <!--end::Order summary-->
            </div>
            <!--end::Order details page-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection
