@extends('layouts.admin.app')

@section('content')
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap gap-2">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column align-items-start me-3 py-2 py-lg-0 gap-2">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bold m-0 fs-3">Dashboard
                <!--begin::Separator-->
                <span class="h-20px border-gray-400 border-start mx-3"></span>
                <!--end::Separator-->
                <!--begin::Description-->
                <small class="text-gray-500 fs-7 fw-semibold my-1">{{ @Auth::user()->company->name }}</small>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Actions-->
        <!--end::Actions-->
    </div>


    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid col-md-10" id="kt_content">
            <div class="row gy-5 g-xl-10">
                {{-- total renevue --}}
                <div class="col-sm-6 mb-5 mb-xl-10">
                    <!--begin::List widget 1-->
                    <div class="card card-flush h-lg-100">
                        <!--begin::Header-->
                        <div class="card-header pt-5">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bold text-dark">Profit</span>
                            </h3>
                            <!--end::Title-->

                            <!--begin::Toolbar-->
                            <div class="card-toolbar">
                                <!--begin::Menu-->
                                <span class="svg-icon svg-icon-2hx svg-icon-gray-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-wallet2" viewBox="0 0 16 16">
                                        <path d="M12.136.326A1.5 1.5 0 0 1 14 1.78V3h.5A1.5 1.5 0 0 1 16 4.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 13.5v-9a1.5 1.5 0 0 1 1.432-1.499L12.136.326zM5.562 3H13V1.78a.5.5 0 0 0-.621-.484L5.562 3zM1.5 4a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z"/>
                                      </svg>
                                </span>
                            </div>
                            <!--end::Toolbar-->
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div class="card-body pt-5">
                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Today</div>
                            <!--end::Section-->

                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <i class="ki-duotone ki-arrow-up-right fs-2 text-success me-2"><span class="path1"></span><span class="path2"></span></i>

                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">Rp. {{number_format($totalProfitByToday)}}</span>
                            </div>
                            <!--end::Statistics-->
                            </div>
                            <!--end::Item-->
                            <!--begin::Separator-->
                            <div class="separator separator-dashed my-3"></div>
                            <!--end::Separator-->

                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">This Month</div>
                            <!--end::Section-->

                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <i class="ki-duotone ki-arrow-down-right fs-2 text-danger me-2"><span class="path1"></span><span class="path2"></span></i>

                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">Rp. {{number_format($totalProfitByThisMonth)}}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                            </div>
                            <!--end::Item-->
                            <!--begin::Separator-->
                            <div class="separator separator-dashed my-3"></div>
                            <!--end::Separator-->
                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">This year</div>
                            <!--end::Section-->

                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <i class="ki-duotone ki-arrow-up-right fs-2 text-success me-2"><span class="path1"></span><span class="path2"></span></i>
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">Rp. {{number_format($totalProfitByThisYear)}}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                            </div>
                        <!--end::Item-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::LIst widget 1-->
                </div>

                {{-- total order --}}
                <div class="col-sm-6 mb-5 mb-xl-10">
                    <!--begin::List widget 1-->
                    <div class="card card-flush h-lg-100">
                        <!--begin::Header-->
                        <div class="card-header pt-5">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bold text-dark">Total Order</span>
                            </h3>
                            <!--end::Title-->

                            <!--begin::Toolbar-->
                            <div class="card-toolbar">
                                <!--begin::Menu-->
                                <span class="svg-icon svg-icon-2hx svg-icon-gray-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
                                        <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                </span>
                            </div>
                            <!--end::Toolbar-->
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div class="card-body pt-5">
                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">Today</div>
                            <!--end::Section-->

                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <i class="ki-duotone ki-arrow-up-right fs-2 text-success me-2"><span class="path1"></span><span class="path2"></span></i>

                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{$totalOrdersToday}}</span>
                            </div>
                            <!--end::Statistics-->
                            </div>
                            <!--end::Item-->
                            <!--begin::Separator-->
                            <div class="separator separator-dashed my-3"></div>
                            <!--end::Separator-->

                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">This Month</div>
                            <!--end::Section-->

                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <i class="ki-duotone ki-arrow-down-right fs-2 text-danger me-2"><span class="path1"></span><span class="path2"></span></i>

                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{$totalOrdersThisMonth}}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                            </div>
                            <!--end::Item-->
                            <!--begin::Separator-->
                            <div class="separator separator-dashed my-3"></div>
                            <!--end::Separator-->
                            <!--begin::Item-->
                            <div class="d-flex flex-stack">
                            <!--begin::Section-->
                            <div class="text-gray-700 fw-semibold fs-6 me-2">This year</div>
                            <!--end::Section-->

                            <!--begin::Statistics-->
                            <div class="d-flex align-items-senter">
                                <i class="ki-duotone ki-arrow-up-right fs-2 text-success me-2"><span class="path1"></span><span class="path2"></span></i>
                                <!--begin::Number-->
                                <span class="text-gray-900 fw-bolder fs-6">{{$totalOrdersThisYear}}</span>
                                <!--end::Number-->
                            </div>
                            <!--end::Statistics-->
                            </div>
                        <!--end::Item-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::LIst widget 1-->
                </div>
            </div>
            <div class="row g-5 g-lg-12">
                <!--begin::Col-->
                <div class="col-md-6">
                    <div class="card card-custom gutter-b mb-10">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Chart Order</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Chart-->
                            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                                <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#orderByDate">Date</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#orderByMonth">Month</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#orderByYear">Year</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="tab-order-chart">
                                <div class="tab-pane fade show active" id="orderByDate" role="tabpanel">
                                    <div id="orderByDateChart"></div>
                                </div>
                                <div class="tab-pane fade" id="orderByMonth" role="tabpanel">
                                    <div id="orderByMonthChart"></div>
                                </div>
                                <div class="tab-pane fade" id="orderByYear" role="tabpanel">
                                    <div id="orderByYearChart"></div>
                                </div>
                            </div>
                            <!--end::Chart-->
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Chart Profit</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Chart-->
                            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                                <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#profitByDate">Date</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#grafik_keuntungan_month">Month</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#profitByYear">Year</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="tab-profit-chart">
                                <div class="tab-pane fade show active" id="profitByDate" role="tabpanel">
                                    <div id="profitByDateChart"></div>
                                </div>
                                <div class="tab-pane fade" id="grafik_keuntungan_month" role="tabpanel">
                                    <div id="grafik_keuntungan"></div>
                                </div>
                                <div class="tab-pane fade" id="profitByYear" role="tabpanel">
                                    <div id="profitByYearChart"></div>
                                </div>
                            </div>
                            <!--end::Chart-->
                        </div>
                    </div>
                    <!--end::Mixed Widget 2-->
                </div>


                <!--end::Col-->
            </div>

        </div>
    </div>
@endsection

@push('scripts')
{{ $dataTable->scripts() }}

<script>
    var options = {
        series: [{
            name: "Total",
            data: {!! $orderByMonth !!}
    }],
    chart: {
        height: 350,
            type: 'line',
            zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Order by Month',
            align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
        },
    },
    xaxis: {
        categories: {!! $months !!},
    }
    };

    var optionYears = {
        series: [{
            name: "Total",
            data: {!! $orderByYear !!}
    }],
    chart: {
        height: 350,
            type: 'line',
            zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Order by Year',
            align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
        },
    },
    xaxis: {
        categories: {!! $years !!},
    }
    };

    var profitOptions = {
        series: [{
            name: "Total",
            data: {!! $profitByMonth !!}
    }],
    chart: {
        height: 350,
            type: 'line',
            zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Profit by Month',
            align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
        },
    },
    xaxis: {
        categories: {!! $months !!},
    },
    yaxis: {
        labels: {
            formatter: function(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                });
                return formatter.format(value);
            }
        }
    },
    tooltip: {
        y: {
            formatter: function(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                });
                return formatter.format(value);
            }
        }
    }
    };

    var optionsByDate = {
        series: [{
            name: "Total",
            data: {!! $orderByDate !!}
    }],
    chart: {
        height: 350,
            type: 'line',
            zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Order by Date',
            align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
        },
    },
    xaxis: {
        categories: {!! $dates !!},
    }
    };

    var profitOptionsByDate = {
        series: [{
            name: "Total",
            data: {!! $profitByDate !!}
    }],
    chart: {
        height: 350,
            type: 'line',
            zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Profit by Date',
            align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
        },
    },
    xaxis: {
        categories: {!! $dates !!},
    },
    yaxis: {
        labels: {
            formatter: function(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                });
                return formatter.format(value);
            }
        }
    },
    tooltip: {
        y: {
            formatter: function(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                });
                return formatter.format(value);
            }
        }
    }
    };

    var profitOptionsByYear = {
        series: [{
            name: "Total",
            data: {!! $profitByYear !!}
    }],
    chart: {
        height: 350,
            type: 'line',
            zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Profit by Year',
            align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
        },
    },
    xaxis: {
        categories: {!! $years !!},
    },
    yaxis: {
        labels: {
            formatter: function(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                });
                return formatter.format(value);
            }
        }
    },
    tooltip: {
        y: {
            formatter: function(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                });
                return formatter.format(value);
            }
        }
    }
    };

    var chart = new ApexCharts(document.querySelector("#orderByMonthChart"), options);
    chart.render();

    var chartOrderByYear = new ApexCharts(document.querySelector("#orderByYearChart"), optionYears);
    chartOrderByYear.render();

    var chartProfit = new ApexCharts(document.querySelector("#grafik_keuntungan"), profitOptions);
    chartProfit.render();

    var chartOrderByDate = new ApexCharts(document.querySelector("#orderByDateChart"), optionsByDate);
    chartOrderByDate.render();

    var chartProfitByDate = new ApexCharts(document.querySelector("#profitByDateChart"), profitOptionsByDate);
    chartProfitByDate.render();

    var chartProfitByYear = new ApexCharts(document.querySelector("#profitByYearChart"), profitOptionsByYear);
    chartProfitByYear.render();
</script>
@endpush
