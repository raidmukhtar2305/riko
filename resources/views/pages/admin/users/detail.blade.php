@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Order details page-->
            <div class="d-flex flex-column gap-7 gap-lg-10">
                <!--begin::Order summary-->
                <div class="d-flex flex-column flex-xl-row gap-7 gap-lg-10">
                    <!--begin::Order details-->
                    <div class="card card-flush py-4 flex-row-fluid">

                        <div class="card-body pt-0">
                            <div class="card-title">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="mt-2">User Detail</h3>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="d-flex justify-content-end">
                                            <!-- <button class="btn btn-md-primary" onclick="history.back()">Back</button> -->
                                            <!-- <a href="#"  class="btn btn-white">White</a> -->
                                            <a href="#" onclick="history.back()" class="btn btn-primary">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">
                                        <tr>
                                            <td class="text-muted">
                                                <div class="d-flex align-items-center mb-2">
                                                    Image Product
                                                </div>
                                                @foreach ($product['productImage'] ?? [] as $productImage)
                                                    @if (!empty($productImage->image))
                                                        <img style="width:150px; margin-right: 5px;"
                                                            src="{{ asset('products/' . $productImage->image) }}"
                                                            alt="">
                                                    @else
                                                        <img style="width:150px; margin-right: 5px; border: 1px solid black;"
                                                            src="{{ asset('assets2/images/no-image.jpg') }}" alt="no-image">
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div> --}}

                            <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                                <!--begin::Table body-->
                                <tbody class="fw-semibold text-gray-600">
                                    <!--begin::Customer name-->
                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Name
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->name }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Username
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->username }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Email
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->email }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Role
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->type }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Phone
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->phone }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Address
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->address }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Company Name
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->company->name }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Outlet Name
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $user->outlet->name }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Order details-->
                    <!--end::Customer details-->
                </div>
                <!--end::Order summary-->
                <!--begin::Tab content-->
                <!--end::Tab content-->
            </div>
            <!--end::Order details page-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection
