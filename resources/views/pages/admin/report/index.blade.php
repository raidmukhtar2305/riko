@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid">
            <!--begin::Card-->
            <div class="card">
                <div class="card-header border-0 pt-6 d-flex justify-content-justify">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="input-group">
                                        <div class="input-group date">
                                            <span class="input-group-text bg-light d-block " id="basic-addon1">
                                                <i class="fa fa-calendar mt-2 mb-2"></i>
                                            </span>
                                            <input type="text" class="form-control cursor-pointer" id="start_date"
                                                placeholder="Start Date" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="input-group">
                                        <div class="input-group date">
                                            <span class="input-group-text bg-light d-block " id="basic-addon1">
                                                <i class="fa fa-calendar mt-2 mb-2"></i>
                                            </span>
                                            <input type="text" class="form-control cursor-pointer" id="end_date"
                                                placeholder="End Date" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <button id="filter" class="btn btn-primary">Submit</button>
                                    <button id="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--begin::Card body-->
                <br>
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table table-hover table-rounded table-striped border gy-7 gs-7" id="records">
                            <thead>
                                <tr>
                                    <th class="fw-normal fs-6 text-gray-800">Company Name</th>
                                    <th class="fw-normal fs-6 text-gray-800">Customer Name</th>
                                    <th class="fw-normal fs-6 text-gray-800">Date</th>
                                    <th class="fw-normal fs-6 text-gray-800">Total</th>
                                    <th class="fw-normal fs-6 text-gray-800">Status</th>
                                    {{-- <th class="fw-normal fs-6 text-gray-800">Action</th> --}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
    {{-- @include("pages.".$path.".".$view.'.form') --}}
@endsection
@push('scripts')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"> --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {
            $("#start_date").datepicker({
                "dateFormat": "yy-mm-dd"
            });
            $("#end_date").datepicker({
                "dateFormat": "yy-mm-dd"
            });
        });

        function fetch(start_date, end_date) {
            $.ajax({
                url: "{{ route('admin.report.records') }}",
                type: "GEt",
                data: {
                    start_date: start_date,
                    end_date: end_date
                },
                dataType: "json",
                success: function(data) {
                    var i = 1;
                    $('#records').DataTable({
                        dom: 'Bfrtip',
                        "data": data.orders,
                        "responsive": true,
                        "columns": [{
                                "data": "company.name"
                            },
                            {
                                "data": "customer.name",
                            },
                            {
                                "data": "date",
                                "render": function(data, type, row, meta) {
                                    return moment(row.date).format('DD-MM-YYYY');
                                }
                            },
                            {
                                "data": "total",
                            },
                            {
                                "data": "status",
                                "render": function(data, type, row) {
                                    if (data == 0) {
                                        return "Pending";
                                    } else if (data == 1) {
                                        return "Approve";
                                    } else {
                                        return "Unknown";
                                    }
                                }

                            },
                            // {
                            //     "data": "Actions",
                            //     render: function(data, type, row, meta) {
                            //         return ;
                            //     }
                            // }
                        ]
                    });
                }
            });
        }
        fetch();

        $(document).on("click", "#filter", function(e) {
            e.preventDefault();
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();
            if (start_date == "" || end_date == "") {
                alert("isi tanggal");
            } else {
                $('#records').DataTable().destroy();
                fetch(start_date, end_date);
            }
        });

        $(document).on("click", "#reset", function(e) {
            e.preventDefault();
            $("#start_date").val('');
            $("#end_date").val('');
            $('#records').DataTable().destroy();
            fetch();
        });
    </script>
@endpush
<style>
    .ui-datepicker {
        width: 300px;
        height: 220px;
        margin: 5px auto 0;
        font: 14pt Arial, sans-serif;
    }

    .ui-datepicker table {
        width: 100%;
    }

    .ui-datepicker-header {
        background: #3399ff;
        color: #3399ff;
        font-family: 'Arial';
        border-width: 1px 0 0 0;
        border-style: solid;
        border-color: #111;
    }

    .ui-datepicker-prev {
        float: left;
        cursor: pointer;
        background-position: center -30px;
    }

    .ui-datepicker-next {
        float: right;
        cursor: pointer;
        background-position: center 0px;
    }
</style>
