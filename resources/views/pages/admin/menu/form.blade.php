<!--begin::Modal-->
<div class="modal fade" tabindex="-1" id="menu-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        <form method="post" action="{{ route('admin.menus.store') }}" class="form needs-validation" id="menu-form" autocomplete="off" novalidate>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form {{ $title }}</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="mb-10 fv-row">
                        <label for="parent_id" class="required form-label">Parent</label>
                        <select name="parent_id" class="form-control form-control-solid form-select mb-2" >
                            <option value="">Tanpa Parent</option>
                            @foreach ($parents as $parent)
                            <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Name</label>
                        <input type="text" class="form-control form-control-solid" name="name" placeholder="Name" required/>
                    </div>


                    <div class="mb-10 fv-row">
                        <label for="name" class="form-label">Route</label>
                        <input type="text" class="form-control form-control-solid" name="route" placeholder="Route"/>
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="permission" class="form-label">Permission</label>
                        <select name="permission_id" class="form-control form-control-solid form-select mb-2" >
                            @foreach ($permissions as $permission)
                            <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button  class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!--end::Modal-->
