<!--begin::Modal-->
<div class="modal fade" tabindex="-1" id="customer-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        <form method="post" action="{{ route('admin.customer.store') }}" class="form needs-validation" id="customer-form" autocomplete="off" novalidate>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form {{ $view }}</h5>
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    @if (Auth::user()->hasRole('superadmin'))
                    <div class="mb-10 fv-row">
                        <label for="company_id" class="required form-label">Company</label>
                        <select name="company_id" id="company_id"
                            class="form-control form-control-solid form-select mb-2 search" required>
                            <option value="" disabled selected>--Choose option--</option>
                            @foreach ($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Name</label>
                        <input type="text" class="form-control form-control-solid" name="name" placeholder="Name" required/>
                    </div>
                    <div class="mb-10 fv-row">
                        <label for="email" class="required form-label">Email</label>
                        <input type="email" class="form-control form-control-solid" name="email" placeholder="email"
                            required />
                    </div>
                    <div class="mb-10 fv-row">
                        <label for="phone_number" class="required form-label">Phone Number</label>
                        <input type="number" class="form-control form-control-solid" name="phone_number" placeholder="Phone Number"
                            required />
                    </div>
                    <div class="mb-10 fv-row">
                        <label for="address" class="required form-label">Address</label>
                        <textarea class="form-control form-control-solid" name="address" placeholder="Address" id="address" required></textarea>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button  class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>
    $(document).ready(function() {
        $('.search').select2({
            dropdownParent: $('#customer-modal'),
            containerCssClass: 'select2-container-modal'
        });
    });
    </script>
<!--end::Modal-->
