@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Order details page-->
            <div class="d-flex flex-column gap-7 gap-lg-10">
                <!--begin::Order summary-->
                <div class="d-flex flex-column flex-xl-row gap-7 gap-lg-10">
                    <!--begin::Order details-->
                    <div class="card card-flush py-4 flex-row-fluid">

                        <div class="card-body pt-0">
                            <div class="card-title">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="mt-2">Customer Detail</h3>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="d-flex justify-content-end">
                                            <!-- <button class="btn btn-md-primary" onclick="history.back()">Back</button> -->
                                            <!-- <a href="#"  class="btn btn-white">White</a> -->
                                            <a href="#" onclick="history.back()" class="btn btn-primary">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">
                                        <tr>
                                            <td class="text-muted">
                                                <div class="d-flex align-items-center mb-2">
                                                    Image Product
                                                </div>
                                                @foreach ($product['productImage'] ?? [] as $productImage)
                                                    @if (!empty($productImage->image))
                                                        <img style="width:150px; margin-right: 5px;"
                                                            src="{{ asset('products/' . $productImage->image) }}"
                                                            alt="">
                                                    @else
                                                        <img style="width:150px; margin-right: 5px; border: 1px solid black;"
                                                            src="{{ asset('assets2/images/no-image.jpg') }}" alt="no-image">
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div> --}}

                            <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                                <!--begin::Table body-->
                                <tbody class="fw-semibold text-gray-600">
                                    <!--begin::Customer name-->
                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Company Name
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $customer->company->name }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Customer Name
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $customer->name }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Address
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $customer->address }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Phone
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $customer->phone_number }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-muted">
                                            <div class="d-flex align-items-center">Email
                                            </div>
                                        </td>
                                        <td class="fw-bold text-end">
                                            <div class="d-flex align-items-center justify-content-end">
                                                <!--begin::Name-->
                                                <a href="#"
                                                    class="text-gray-600 text-hover-primary">{{ $customer->email }}</a>
                                                <!--end::Name-->
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Order details-->
                    <!--end::Customer details-->
                </div>
                <!--end::Order summary-->
                <!--begin::Tab content-->
                <!--end::Tab content-->
            </div>
            <!--end::Order details page-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->

    
    <!-- begin::Chart -->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid col-md-10" id="kt_content">
            <div class="row gy-5 g-xl-10">
                <!--begin::Chart Product-->
                <div class="col-sm-12 mb-5 mb-xl-10">
                    <!--begin::List widget 1-->
                    <div class="card card-custom gutter-b mb-10">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Chart Customer</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <!--begin::Chart-->
                            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                                <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#customerByDate">Date</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#customerByMonth">Month</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#customerByYear">Year</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="tab-customer-chart">
                                <div class="tab-pane fade show active" id="customerByDate" role="tabpanel">
                                    <div id="customerByDateChart"></div>
                                </div>
                                <div class="tab-pane fade" id="customerByMonth" role="tabpanel">
                                    <div id="customerByMonthChart"></div>
                                </div>
                                <div class="tab-pane fade" id="customerByYear" role="tabpanel">
                                    <div id="customerByYearChart"></div>
                                </div>
                            </div>
                            <!--end::Chart-->
                        </div>
                    </div>
                    <!--end::LIst widget 1-->
                </div>
            </div>
        </div>
    </div>
    <!-- end::Chart -->
@endsection
@push('scripts')
    <script>
        var options = {
            series: [{
                name: "Total",
                data: {!! $customerByMonth !!}
            }],
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            title: {
                text: 'Customer by Month',
                align: 'left'
            },
            grid: {
                row: {
                    colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.5
                },
            },
            xaxis: {
                categories: {!! $months !!},
            }
        };

        var optionYears = {
            series: [{
                name: "Total",
                data: {!! $customerByYear !!}
            }],
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            title: {
                text: 'Customer by Year',
                align: 'left'
            },
            grid: {
                row: {
                    colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.5
                },
            },
            xaxis: {
                categories: {!! $years !!},
            }
        };

        var optionsByDate = {
            series: [{
                name: "Total",
                data: {!! $customerByDate !!}
            }],
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            title: {
                text: 'Customer by Date',
                align: 'left'
            },
            grid: {
                row: {
                    colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.5
                },
            },
            xaxis: {
                categories: {!! $dates !!},
            }
        };

        var chart = new ApexCharts(document.querySelector("#customerByMonthChart"), options);
        chart.render();

        var chartCustomerByYear = new ApexCharts(document.querySelector("#customerByYearChart"), optionYears);
        chartCustomerByYear.render();

        var chartCustomerByDate = new ApexCharts(document.querySelector("#customerByDateChart"), optionsByDate);
        chartCustomerByDate.render();
    </script>
@endpush
