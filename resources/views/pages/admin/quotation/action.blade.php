<div class="dropdown">
    <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown"
        aria-expanded="false">
        Action
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">
        @if ($status == 0)
            <li><a class="dropdown-item convert-order-btn" data-id="{{ $id }}" href="#">Convert to Order</a>
            <li>
                <hr class="dropdown-divider">
            </li>
            <li><a class="dropdown-item edit-btn" href="{{ route('admin.quotation.edit', $id) }}">Edit</a></li>
        @endif
        <li><a class="dropdown-item" href="{{ route('admin.quotation.show', $id) }}">Show</a></li>
        <li>
            <a class="dropdown-item" href="{{ route('admin.quotation.print-invoice', $id) }}" target="_blank">  Preview Print Invoice</a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('admin.quotation.print', $id) }}"> <i class="fa fa-download" aria-hidden="true"></i> Download Invoice</a>
        </li>
        </li>
        <li>
            <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item delete-btn" data-id="{{ $id }}" href="#">Delete</a></li>
    </ul>
</div>
