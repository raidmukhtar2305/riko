<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        * {
            font-size: 12px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .mt-3 {
            margin-top: 15px;
        }

        .font-weight-normal {
            margin-left: 20px;
        }

        table thead {
            background-color: #ececec;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        #logo {
            float: left;
        }

        #logo img {
            height: 40px;
        }

        #text-header p {
            color: #304a68;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            margin-top: 20px;
            text-align: left;
            font-size: 42px;
            line-height: 90%;
            font-weight: bold;
        }

        .text-footer p {
            color: #304a68;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            margin-top: 20px;
            text-align: center;
            font-size: 22px;
            line-height: 90%;
            font-weight: bold;
        }

        #text-company p {
            color: #616568;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            margin-top: 30px;
            text-align: left;
            font-size: 30px;
            line-height: 90%;
        }

        .text-date {
            color: #1c1f21;
            margin-bottom: 3px;
            /* margin-left: 50px; */
            margin-top: 10px;
            text-align: right;
            font-size: 15px;
            line-height: 90%;
            font-weight: bold;
        }

        .text-customer {
            color: black;
            font-size: 15px;
            line-height: 50%;
        }

        .row {
            margin-top: 20px;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="">
            @if (request()->route()->getName() == 'admin.quotation.print-invoice')
                <a class="dropdown-item" href="{{ route('admin.quotation.print', $quotation->id) }}"><i
                        class="fa fa-download" aria-hidden="true"></i>Download Invoice</a>
            @endif

            <div class="clearfix">
                <div id="text-header">
                    <p>Quotation</p>
                </div>
                <div id="text-company">
                    <p> {{ $quotation->company->name }} </p>
                </div>
                <div class="text-right">
                    <p class="text-date">Valid From : {{ date('l, j F Y') }}</p>
                    <p class="text-date">To : {{ date('l, j F Y', strtotime($valid_date)) }}</p>
                </div>

            </div>
            <hr>

            <table style="width: 100%">
                <tr>
                    <td>
                        <div class="text-left align-top">
                            <p class="mr-2">Detail Quotation :</p>
                            <p class="text-customer"><b> Date : {{ date('l, j F Y', strtotime($quotation->date)) }}</b>
                            </p>
                            <p class="text-customer"><b> #{{ $quotation->quotation_code }}</b></p>
                        </div>
                    </td>
                    <td>
                        <div class="text-right">
                            <p class="">Detail Customer :</p>
                            <p class="text-customer"><b> {{ @$quotation->customer->name }}</b></p>
                            <p class="text-customer"><b> {{ @$quotation->customer->phone_number }}</b></p>
                            <p class="text-customer"><b> {{ @$quotation->customer->email }}</b></p>
                            <p>{{ @$quotation->customer->address }}</p>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="">
                <div class="cart">
                    <div class="mt-3">
                        <p>List Product</p>
                        <table border="1" cellspacing="0" cellpadding="8" class="table table-bordered"
                            style="max-height: 100%">
                            <thead>
                                <tr>
                                    <td>Name Product</td>
                                    <td>Qty</td>
                                    <td>Price</td>
                                    <td>Description</td>
                                    <td>Subtotal</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($quotation->quotation_details as $key => $value)
                                    <tr>
                                        <td>
                                            <b>

                                                {{ $value->product_name }}
                                            </b>
                                        </td>
                                        <td class="text-end">{{ $value->qty }}</td>
                                        <td class="text-end">Rp. {{ number_format($value->price) }}</td>
                                        <td class="text-end">{{ $value->product_description }}</td>
                                        <td class="text-end">Rp. {{ number_format($value->price * $value->qty) }}</td>
                                    </tr>
                                @endforeach
                                @if (request()->route()->getName() == 'admin.quotation.print-invoice')
                                    @if (count($quotation->quotation_details) < 5)
                                        <tr style="height: 200px; overflow-y: auto;">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                @endif
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="1" class="total"> <b>Total</b> </td>
                                    <td class="total"><b>Rp. {{ number_format($quotation->total)}}</b> </td>
                                </tr >
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="">
                    <div class="cart">
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <div class="text-left align-top">
                                        <p class="text-customer"><b> Description : {{ $quotation->description }}</b>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <div class="text-footer">
                    <p>
                        Thankyou You For Your Bussiness!
                    </p>
                </div>
            </div>
        </div>
    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
</body>

</html>
