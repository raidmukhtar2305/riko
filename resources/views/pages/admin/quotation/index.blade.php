@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid">
            <!--begin::Card-->
            <div class="card">
                <div class="card-header border-0 pt-6 d-flex justify-content-end">
                    <a href="{{ route('admin.quotation.create') }}" class="btn btn-primary btn-hover-scale mt-3"> <i
                            class="la la-plus"></i> Create </a>
                </div>
                <!--begin::Card body-->
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    <script>
        $(document).ready(function() {
            let method = 'POST'

            //DELETE
            $(document).on('click', '.delete-btn', function() {
                Swal.fire({
                    title: 'Are you sure',
                    icon: 'info',
                    showDenyButton: true,
                    confirmButtonText: 'Yes',
                    denyButtonText: `No`,
                }).then((result) => {
                    if (result.isConfirmed) {
                        const url = '{{ url('admin/quotation/destroy') }}/' + $(this).data('id')
                        $.ajax({
                            url: url,
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            }
                        }).done(response => {
                            console.log(response)
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                message: response.message
                            })
                            $('.dataTable').DataTable().ajax.reload()
                        })
                    }
                })
            })

            $(document).on('click', '.convert-order-btn', function() {
                $.ajax({
                        url: '{{ url('admin/quotation/convert-order') }}/' + $(this).data('id'),
                        method: 'GET'
                    })
                    .done(response => {
                        if (response.success) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.message
                            })
                            // window.location.href = "{{ route('admin.orders.index') }}";
                            $('.dataTable').DataTable().ajax.reload()
                        }
                    })
                    .fail(response => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: response.responseJSON.message
                        })
                    })
            })
        })

        // $(function() {
        //     var table = $('.data-table').DataTable({
        //         columnDefs: [{
        //             targets: 'no-sort',
        //             orderable: false
        //         }],
        //         dom: '<"row"<"col-sm-6"Bl><"col-sm-6"f>>' +
        //             '<"row"<"col-sm-12"<"table-responsive"tr>>>' +
        //             '<"row"<"col-sm-5"i><"col-sm-7"p>>',
        //         fixedHeader: {
        //             header: true
        //         },
        //         dom: 'Bfrtip',
        //         buttons: [{
        //                 extend: 'excelHtml5',
        //                 className: 'export-button',
        //                 title: 'Excel Quotation',
        //                 footer: true,
        //                 exportOptions: {
        //                     columns: [0, 1, 2, 3, 4, 5, ]
        //                 }
        //             }, {
        //                 extend: 'csvHtml5',
        //                 className: 'export-button',
        //                 title: 'CSV Quotation',
        //                 footer: true,
        //                 exportOptions: {
        //                     columns: [0, 1, 2, 3, 4, 5, ]
        //                 }
        //             },
        //             {
        //                 extend: 'pdfHtml5',
        //                 className: 'export-button',
        //                 title: 'PDF Quotation',
        //                 footer: true,
        //                 exportOptions: {
        //                     columns: [0, 1, 2, 3, 4, 5, ]
        //                 }
        //             },

        //         ],
        //         processing: true,
        //         serverSide: true,
        //         ajax: {
        //             url: "{{ route('admin.quotation.index') }}",
        //         },
        //         columns: [{
        //                 data: 'id',
        //                 render: function(data, type, full, meta) {
        //                     return meta.settings._iDisplayStart + meta.row + 1;
        //                 }
        //             },
        //             {
        //                 data: 'customer.name',
        //                 name: 'customer.name'
        //             },
        //             {
        //                 data: 'company.name',
        //                 name: 'company.name',
        //             },
        //             {
        //                 data: 'quotation_code',
        //                 name: 'quotation_code'
        //             },
        //             {
        //                 data: 'date',
        //                 name: 'date'
        //             },
        //             {
        //                 data: 'status',
        //                 name: 'status'
        //             },
        //             {
        //                 data: 'action',
        //                 name: 'action',
        //                 orderable: false,
        //                 searchable: false
        //             },
        //         ]
        //     });
        // });
    </script>
@endpush
