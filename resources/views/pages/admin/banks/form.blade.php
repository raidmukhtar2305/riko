<!--begin::Modal-->
<div class="modal fade" tabindex="-1" id="bank-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        @csrf
        <form method="post" action="{{ route('admin.banks.store') }}" class="form needs-validation" id="bank-form" enctype="multipart/form-data" autocomplete="off" novalidate>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Bank</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Name</label>
                        <input type="text" id="name" class="form-control form-control-solid" name="name"
                            placeholder="Name" required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="account" class="required form-label">Account Number</label>
                        <input type="number" pattern="[0-9]+([\.,][0-9]+)?" name="account"
                            class="form-control form-control-solid" placeholder="Account Number" required>

                    </div>

                    <div class="mb-10 fv-row">
                        <label for="bank" class="required form-label">Bank Name</label>
                        <input type="text" class="form-control form-control-solid" name="bank" id="bank"
                            placeholder="Bank Name" required />
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!--end::Modal-->
