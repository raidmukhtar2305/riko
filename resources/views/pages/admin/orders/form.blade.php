<div class="w-100 flex-lg-row-auto w-lg-300px mb-7 me-7 me-lg-10">
    <!--begin::Order details-->
    <div class="card card-flush py-4">
        <!--begin::Card header-->
        <div class="card-header">
            <div class="card-title">
                <h2>Order Details</h2>
            </div>
        </div>
        <!--end::Card header-->

        <!--begin::Card body-->
        <div class="card-body pt-0">
            <div class="d-flex flex-column gap-10">
                <div class="fv-row">
                    <label class="form-label">Order Code</label>
                    @if (data_get($order, 'order_code'))
                        <div class="fw-bold fs-3">#{{ data_get($order, 'order_code') }}</div>
                    @else
                        <div class="text-muted fs-7">The code will be assigned after the order process.</div>
                    @endif
                </div>

                <div class="fv-row">
                    <label class="form-label">Order Date</label>
                    <input type="date" name="date" placeholder="Select a date" class="form-control mb-2" max="{{ date('Y-m-d', strtotime('tomorrow')) }}"
                        value="{{ data_get($order, 'date') }}" />
                    <div class="text-muted fs-7">Set the date of the order to process.</div>
                </div>

                @if (Auth::user()->hasRole('superadmin'))
                    <div class="fv-row">
                        <label class="form-label">Company</label>
                        <select name="company_id" id="company_id" class="form-select mb-4" required>
                            @isset($order)
                                <option value="{{ $order->company_id }}" selected>
                                    {{ $order->company->name }}
                                </option>
                            @endisset
                        </select>
                        <div class="text-muted fs-7">Set the company of the order to process.</div>
                    </div>
                @endif

                <div class="fv-row">
                    <label class="form-label">Outlet</label>
                    <select name="outlet_id" id="outlet_id" class="form-select mb-4" required>
                        @isset($order)
                            <option value="{{ $order->outlet_id }}" selected>
                                {{ data_get($order, 'outlet.name') }}
                            </option>
                        @endisset
                    </select>
                    <div class="text-muted fs-7">Set the outlet of the order to process.</div>
                </div>

                <div class="fv-row">
                    <label class="form-label">Customer</label>
                    <select name="customer_id" id="customer_id" class="form-select mb-4">
                        @isset($order)
                            <option value="{{ $order->customer_id }}" selected>
                                {{ $order->customer->name }}
                            </option>
                        @endisset
                    </select>
                    <div class="text-muted fs-7">Set the customer of the order to process.</div>
                </div>

                <div class="fv-row">
                    <input class="form-check-input" type="checkbox" value="true" name="is_paid" id="is_paid" />
                    <label class="form-label ms-2" for="is_paid">
                        Lunas
                    </label>
                </div>

                <div class="fv-row d-none" id="form-invoice-description">
                    <label class="form-label">Description</label>
                    <textarea name="invoice_description" class="form-control">{{ data_get($order, 'invoice_description') }}</textarea>
                    <div class="text-muted fs-7">Description for your invoice</div>
                </div>

                <div class="fv-row d-none" id="form-invoice-note">
                    <label class="form-label">Note</label>
                    <textarea name="invoice_note" class="form-control">{{ data_get($order, 'invoice_note') }}</textarea>
                    <div class="text-muted fs-7">Note for your invoice</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column flex-lg-row-fluid gap-7 gap-lg-10">
    <div class="card card-flush py-4">
        <div class="card-header">
            <div class="card-title">
                <h2>Select Products</h2>
            </div>
        </div>

        <div class="card-body pt-0">
            <div class="d-flex flex-column gap-10">
                <div>
                    <label class="form-label">Add products to this order</label>
                    <div class="row row-cols-1 row-cols-xl-3 row-cols-md-2 border border-dashed rounded pt-3 pb-1 px-2 mb-5 mh-300px overflow-scroll"
                        id="kt_ecommerce_edit_order_selected_products">
                        <span class="w-100 text-muted">Select one or more products from the list</span>
                    </div>

                    <div class="fv-row">
                        <table class="table table-responsive table-rounded border gy-5 gs-5">
                            <thead>
                                <tr class="fw-semibold border-bottom">
                                    <th class="col-md-9">Name Product</th>
                                    <th class="col-md-2">Qty</th>
                                    <th class="col-md-1">Action</th>
                                </tr>
                            </thead>

                            <tbody class="product-container">

                                @foreach (data_get($order, 'order_details', []) as $order_detail)
                                    <tr class="product-list" data-id="{{ $order_detail->id }}">
                                        <td>
                                            <select name="product_id[]" class="form-select" required>
                                                <option value="{{ $order_detail->product_id }}"
                                                    data-price="{{ $order_detail->price }}" selected>

                                                    @if (Auth::user()->hasRole('superadmin'))
                                                        {{ $order_detail->product->company->name }} -
                                                    @endif

                                                    {{ $order_detail->product_name . ' - ' . number_format($order_detail->price, 2) }}
                                                </option>
                                            </select>
                                        </td>

                                        <td>
                                            <input type="number" name="product_qty[]" value="{{ $order_detail->qty }}"
                                                min="1" class="form-control" placeholder="Qty" required>
                                        </td>

                                        <td class="btn-group">
                                            <a class="btn btn-sm btn-success add-product-list">
                                                <span class="fa-solid fa-plus"></span>
                                            </a>

                                            <a class="btn btn-sm btn-danger remove-product-list hide">
                                                <span class="fa-solid fa-minus"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr class="product-list">
                                    <td>
                                        <select name="product_id[]" class="form-select" required></select>
                                    </td>

                                    <td>
                                        <input type="number" name="product_qty[]" min="1" class="form-control"
                                            placeholder="Qty" required>
                                    </td>

                                    <td class="btn-group">
                                        <a class="btn btn-sm btn-success add-product-list">
                                            <span class="fa-solid fa-plus"></span>
                                        </a>

                                        <a class="btn btn-sm btn-danger remove-product-list hide">
                                            <span class="fa-solid fa-minus"></span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="separator"></div>

                    <div class="d-flex justify-content-end mt-4">
                        <div class="fw-bold fs-2">Total Rp :
                            <span id="order_total">0.00</span>
                        </div>
                    </div>
                </div>
                <div class="separator"></div>
            </div>
        </div>
    </div>

    <div class="card card-flush py-4">
        <div class="card-header">
            <div class="card-title">
                <h2>Delivery Details</h2>
            </div>
        </div>

        <div class="card-body pt-0">
            <div class="fv-row">
                <label class="form-label mt-2">Description</label>
                <textarea class="form-control mb-2" id="" name="description" rows="3">{{ data_get($order, 'description') }}</textarea>
            </div>
        </div>
    </div>

    <input type="hidden" name="order_detail_deleted">

    <div class="d-flex justify-content-end">
        <a href="{{ route('admin.orders.index') }}" class="btn btn-light me-5">Cancel</a>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
    <!--end::Order details-->
</div>

@push('scripts')
    <script>
        $(document).ready(function() {

            generateSelectOptionCompanies();
            generateSelectOptionOutlets();
            generateSelectOptionCustomers();
            generateProductValidation();

            function generateProductValidation() {
                let order_total = 0;

                $.each($(".product-list"), function(indexInArray, valueOfElement) {
                    $(this).find('.product-count').text(indexInArray + 1);

                    $(this).find('[name*="product_id"]').attr('name', 'product_id[' + indexInArray + ']');
                    $(this).find('[name*="product_qty"]').attr('name', 'product_qty[' + indexInArray + ']');

                    let product_price = $(this).find('[name*="product_id"] :selected').data('price');
                    let product_qty = $(this).find('[name*="product_qty"]').val();
                    let product_subtotal = product_price * product_qty;
                    if (product_subtotal >= 0) order_total += product_subtotal;
                });

                generateSelectOptionProducts();

                if (parseInt($(".product-list").length) > 1) {
                    $(".product-list .remove-product-list").removeClass('disabled');
                    $(".product-list .add-product-list").addClass('disabled');
                    $(".product-list:last .add-product-list").removeClass('disabled');
                } else {
                    $(".product-list .remove-product-list").addClass('disabled');
                    $(".product-list .add-product-list").removeClass('disabled');
                }

                $('#order_total').text(order_total);
            }

            function generateSelectOptionProducts() {
                $('[name*="product_id"]').select2({
                    placeholder: "Choose product...",
                    ajax: {
                        url: "{{ route('admin.products.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });

                $('[name*="product_id"]').off('select2:select');
                $('[name*="product_id"]').on("select2:select", function(e) {
                    $(e.currentTarget).find(':selected').attr('data-price', e.params.data.price)
                });
            }

            function generateSelectOptionCompanies() {
                $('[name*="company_id"]').select2({
                    placeholder: "Choose company...",
                    ajax: {
                        url: "{{ route('admin.company.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });
            }

            function generateSelectOptionOutlets() {
                $('[name*="outlet_id"]').select2({
                    placeholder: "Choose outlet...",
                    ajax: {
                        url: "{{ route('admin.outlet.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });
            }

            function generateSelectOptionCustomers() {
                $('[name*="customer_id"]').select2({
                    placeholder: "Choose customer...",
                    ajax: {
                        url: "{{ route('admin.customer.get-list') }}",
                        dataType: 'json',
                        data: function(params) {
                            return {
                                search: $.trim(params.term)
                            };
                        },
                        processResults: function(response) {
                            return {
                                results: response.data
                            };
                        },
                        cache: false
                    }
                });
            }

            $(document).on('click', '.add-product-list', function(e) {
                e.preventDefault();

                $('[name*="product_id"]').select2('destroy');
                var c = $(this).parents('.product-list').clone();
                c.find('input').val("").attr('class', 'form-control').removeAttr("aria-describedby");
                c.find('.invalid-feedback').remove();
                $('.product-container').append(c);
                generateProductValidation();
            });

            $(document).on('click', '.remove-product-list', function(e) {
                e.preventDefault();

                if ($('.product-container .product-list').length > 1) {
                    var c = $(this).parents('.product-list');
                    let order_detail_deleted_id = c.data('id');
                    if (order_detail_deleted_id) {
                        let order_detail_deleted = $('[name*="order_detail_deleted"]').val();
                        $('[name*="order_detail_deleted"]').val(
                            order_detail_deleted_id + ',' + order_detail_deleted
                        );
                    }
                    c.remove();

                    generateProductValidation();
                }
            });

            $(document).on('change', '[name*="product_id"], [name*="product_qty"]', function(e) {
                e.preventDefault();

                generateProductValidation();
            });

            $(document).on('click', '#is_paid', function(e) {
                if ($(this).is(':checked')) {
                    document.getElementById('form-invoice-note').classList.remove('d-none')
                    document.getElementById('form-invoice-description').classList.remove('d-none')
                } else {
                    document.getElementById('form-invoice-note').classList.add('d-none')
                    document.getElementById('form-invoice-description').classList.add('d-none')

                    $('[name*="invoice_description"]').val('');
                    $('[name*="invoice_note"]').val('');
                }
            })

            $(document).on('submit', '#order-form', function(e) {
                e.preventDefault()

                const payload = $(this).serialize()
                const url = $(this).attr('action')
                const validator = document.getElementById('order-form').checkValidity()

                if (validator) {
                    $.ajax({
                            url: url,
                            method: 'POST',
                            data: payload
                        })
                        .done(response => {
                            if (response.success) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: response.message
                                })
                                window.location.href = "{{ route('admin.orders.index') }}";
                            }
                        })
                        .fail(response => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.message
                            })
                        })
                }
            })
        });
    </script>
@endpush
