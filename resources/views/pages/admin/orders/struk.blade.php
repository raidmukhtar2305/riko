<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #invoice-POS {
            margin: auto;
            width: 44mm;
            background: #FFF;
        }

        #invoice-POS h1 {
            font-size: 16px;
            color: #000000;
            margin: 3px;
        }

        #invoice-POS h2 {
            font-size: 12px;
            margin: 0px;
        }

        #invoice-POS p {
            font-size: 11px;
            color: #000000;
            line-height: 1.2em;
            margin-top: 2px;
            margin-bottom: 2px;
        }

        #invoice-POS .info {
            display: block;
            margin-left: 0;
        }

        #invoice-POS table {
            width: 100%;
            border-collapse: collapse;
        }

        #invoice-POS .tabletitle {
            font-size: 12px;
            border-bottom: 1px solid #000000;
            border-top: 1px solid #000000;
        }

        #invoice-POS .service {
            border-bottom: 1px solid #626262;
        }

        #invoice-POS .item {
            width: 24mm;
        }

        #invoice-POS .itemtext {
            font-size: 12px;
            margin: 0px;
        }

        #invoice-POS #legalcopy {
            margin-top: 2mm;
            text-align: center
        }
    </style>
</head>

<body>
    <div id="invoice-POS">
        <div id="mid">
            <div class="info">
                {{-- @dump($order) --}}
                <h1 style="text-align: center">{{ $order->company->name }}</h1>
                <p>
                    Address : {{ $order->company->address }}</br>
                    Email : {{ $order->company->email }}</br>
                    Phone : {{ $order->company->phone }}</br>
                </p>
            </div>
        </div>
        <!--End Invoice Mid-->

        <div id="bot">
            <div id="table">
                <table>
                    <tr class="tabletitle">
                        <td class="item">
                            <h2>Item</h2>
                        </td>
                        <td class="Hours">
                            <h2>Qty</h2>
                        </td>
                        <td class="Rate">
                            <h2>Sub Total</h2>
                        </td>
                    </tr>

                    @foreach ($order_detail as $data)
                        <tr class="service">
                            <td class="tableitem">
                                <p class="itemtext">{{ $data->product->name }}</p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext">{{ $data->qty }}</p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext" style="text-align: right">{{ number_format($data->price * $data->qty) }}</p>
                            </td>
                        </tr>
                    @endforeach
                    <tr class="bg-gray">
                        <td class="Rate">
                            <h2>Total</h2>
                        </td>
                        <td></td>
                        <td class="payment">
                            <h2 style="text-align: right">{{ number_format($order->total) }}</h2>
                        </td>
                    </tr>

                </table>
            </div>
            <div id="legalcopy">
                <p class="legal">Thanks for your purchase!</p>
            </div>
            <p style="text-align: center">-</p>
        </div>
        <button id="btnPrint" class="hidden-print">Print</button>
        <script>
            const $btnPrint = document.querySelector("#btnPrint");
            $btnPrint.addEventListener("click", () => {
                window.print();
            });
        </script>
    </div>
</body>

</html>
