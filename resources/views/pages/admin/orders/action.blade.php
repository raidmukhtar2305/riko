<div class="dropdown">
    <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown"
        aria-expanded="false">
        Action
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">
        <li><a class="dropdown-item edit-btn" href="{{ route('admin.orders.edit', $id) }}">Edit</a></li>
        <li><a class="dropdown-item" href="{{ route('admin.orders.show', $id) }}">Show</a></li>
        <li><a class="dropdown-item" href="{{ route('admin.invoice.index', ['orderId' => $id]) }}">Invoice</a></li>
        <li>
            <a class="dropdown-item" href="{{ route('admin.orders.print-preview', $id) }}" target="_blank">Preview Print Order</a>
            <a class="dropdown-item" href="{{ route('admin.orders.print-struk', $id) }}" target="_blank">Preview Print Struk</a>
        </li>
        <li>
            <a class="dropdown-item" href="{{ route('admin.orders.print', $id) }}">Download Order</a>
        </li>
        <li>
            <hr class="dropdown-divider">
        </li>
        <li><a class="dropdown-item delete-btn" data-id="{{ $id }}" href="#">Delete</a></li>
    </ul>
</div>
