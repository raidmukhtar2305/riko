@extends('layouts.admin.app')
@section('content')
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Form-->
            <form method="post" action="{{ route('admin.orders.refunds.store', $order->id) }}" class="form d-flex flex-column flex-lg-row form needs-validation" id="refund-form" autocomplete="off" novalidate>
            <!-- <form id="kt_ecommerce_edit_order_form" class="" data-kt-redirect="/Adminin/demo20/../demo20/apps/ecommerce/sales/listing.html"> -->
                <!--begin::Aside column-->
                @csrf
                <div class="w-100 flex-lg-row-auto w-lg-300px mb-7 me-7 me-lg-10">
                    <!--begin::Order details-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>Refund Details</h2>
                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <div class="d-flex flex-column gap-10">
                                <div class="fv-row">
                                    <label class="form-label">Refund Date</label>
                                    <input type="date" id="tanggal" min="{{ \Carbon\Carbon::parse($order->date)->format('Y-m-d') }}" name="datetime" placeholder="Select a date" class="form-control mb-2" value="" required />
                                    <div class="text-muted fs-7">Set the date of the refund to process.</div>
                                </div>

                                <div class="fv-row">
                                    <label class="form-label">Remarks</label>
                                    <textarea name="remarks" id="" cols="30" rows="8" class="form-control"></textarea>
                                    <div class="text-muted fs-7">Set the Remarks of the refund to process.</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-column flex-lg-row-fluid gap-7 gap-lg-10">

                    <div class="card card-flush py-4">

                        <div class="card-header">
                            <div class="card-title">
                                <h2>Select Products</h2>
                            </div>
                        </div>

                        <div class="card-body pt-0">
                            <div class="d-flex flex-column gap-10">
                                <div>
                                    <label class="form-label">Add products to this refund</label>
                                    <div class="row row-cols-1 row-cols-xl-3 row-cols-md-2 border border-dashed rounded pt-3 pb-1 px-2 mb-5 mh-300px overflow-scroll" id="kt_ecommerce_edit_order_selected_products">
                                        <span class="w-100 text-muted">Select one or more products from the list</span>
                                    </div>
                                    <div class="fv-row">
                                                <table class="table table-responsive table-rounded border gy-5 gs-5">
                                            <thead>
                                                <tr class="fw-semibold border-bottom">
                                                    <th class="col-md-9">Name Product</th>
                                                    <th class="col-md-2">Qty</th>
                                                    <th class="col-md-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="DynamicTable">
                                                @foreach ($orderDetails as $key => $orderDetail)
                                                <tr id="row{{$key}}">
                                                    <td>
                                                        @if (Auth::user()->hasRole('superadmin'))
                                                        {{ @$orderDetail->product->company->name }} | 
                                                        @endif 
                                                        {{ @$orderDetail->product->name }} | Rp. {{ number_format(@$orderDetail->product->price) }}
                                                        <input type="hidden" name="product_id[]" value="{{ $orderDetail->product->id }}">
                                                    </td>
                                                    <td>
                                                        <input type="number" name="qty[]" id="qty{{$key}}" data-price="{{$orderDetail->price}}" value="{{ $orderDetail->qty }}" min="1" max="{{ $orderDetail->qty }}" class="form-control cart-qty"
                                                            placeholder="Qty">
                                                    </td>
                                                    <td><button type="button" id="{{ $key }}" class="btn btn-icon btn-danger remove_row"><i class="fa-solid fa-minus"></i></button></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="separator"></div>

                                    <div class="d-flex justify-content-end mt-4">
                                        <div class="fw-bold fs-2">Total Rp :
                                        <span id="cart-subtotal">0.0</span></div>
                                    </div>
                                </div>
                                <div class="separator"></div>
                            </div>
                        </div>
                    </div>

                    <!--end::Order details-->
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('admin.orders.show', $order->id) }}"  class="btn btn-light me-5">Cancel</a>
                        <button class="btn btn-primary" id="save-changes">Save changes</button>
                    </div>
                </div>
                <!--end::Main column-->
            </form>
            <!--end::Form-->
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {

            $('#refund-form').submit(function(e) {
                e.preventDefault()
                const payload = $(this).serialize()
                const url = $(this).attr('action')
                const validator = document.getElementById('refund-form').checkValidity()

                if(validator) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: payload
                    })
                    .done(response => {
                        if(response.success) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.message
                            })
                            setTimeout(() => {
                                window.location.href = "{{ route('admin.orders.show', $order->id) }}";
                            }, 500);
                        }
                    })
                    .fail(response => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: response.responseJSON.message
                        })
                    })
                }

            })

            var i = 0;
            $('#add').click(function() {
                i++;
                $('#cart-subtotal').text(getGrandTotal());

                $('#DynamicTable').append('<tr id="row' + i +
                    '"><td> <select name="product_id[]" class="form-select select2-multiple select-product products-data cart-price" data-placeholder="Select an product"><option value="" disabled selected>Select an product</option>@foreach ($orderDetails as $orderDetail) <option value="{{ $orderDetail->product->id }}" data-id="{{ $orderDetail->product->id }}" data-price="{{ $orderDetail->product->price }}">  @if (Auth::user()->type == "superadmin") {{ @$orderDetail->product->company->name }} | @endif {{ $orderDetail->product->name }} | Rp. {{ number_format($orderDetail->product->price) }} (max refund qty: {{ $orderDetail->qty }})</option> @endforeach </select></td><td><input type="number" name="qty[]" class="form-control cart-qty" id="qty'+i+'" value="1" min="1" placeholder="Qty"></td><td><button type="button" id="' +
                    i + '" class="btn btn-icon btn-danger remove_row"><i class="fa-solid fa-minus"></i></button></td></tr>');

                $('.select2-multiple').select2();
                getGrandTotal();
            });
            $(document).on('click', '.remove_row', function() {
                var row_id = $(this).attr("id");
                $('#row' + row_id + '').remove();
                getGrandTotal()
            });

            $(document).on('input','.cart-qty',function( e ) {
                var index   = $(".cart-qty").index(this)
                var qty     = $(".cart-qty").eq(index).val();
                var price   = $(".cart-qty").eq(index).data('price');
                var subtotal= qty * price;
                getGrandTotal()
            });

            let orderDetails = '{!! json_encode($orderDetails) !!}'
            orderDetails = JSON.parse(orderDetails)


            $(document).ready(function() {
                $('#qty{{$key}}').on('input', function() {
                var currentValue = parseInt($(this).val());
                var maxValue = parseInt($(this).attr('max'));

                if (currentValue > maxValue) {
                    $(this).val(maxValue);
                }
                });
            });


            $(document).on('change','.select-product',function() {
                let total = 0
                $('.select-product').each(function(index,e){
                    const price = orderDetails.find(orderDetail => orderDetail.product.id = e.value)
                    const qty = $('#qty'+index).val()
                    const subtotal = price * qty
                    if(!Number.isNaN(subtotal)) {
                        total += subtotal
                    }
                })

                $('#cart-subtotal').html(total);
                getGrandTotal()
            });

            $(document).on('change','cart-qty',function() {
                let total = 0
                $('table>tr').each(function(index,e){
                    const price = orderDetails.find(orderDetail => orderDetail.product.id = e.value)
                    const qty = $('#qty'+index).val()
                    const subtotal = price * qty
                    if(!Number.isNaN(subtotal)) {
                        total += subtotal
                    }
                })

                $('#cart-subtotal').html(total);
                getGrandTotal();
            });

        });

        function getGrandTotal(){
            var total   = 0;
            var price   = 0
            var qty     = 0;
            $('table tbody tr').each(function(index,e){
                price   = $('.cart-qty').eq(index).data('price');
                qty     = $('.cart-qty').eq(index).val();
                if(price != "undefined" && qty != "undefined")
                    total   += (parseInt(price) * parseInt(qty));

            });

            $('#cart-subtotal').html(formatter.format(total));
            return total;
        }

        getGrandTotal()
    </script>
@endpush