@extends('layouts.admin.app')
@section('content')
    <!--begin::Container-->
    <div id="container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid">
            <!--begin::Card-->
            <div class="card">
                <div class="card-body border-0 pt-6">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="d-flex justify-content-start">
                                <div class="m-1">
                                    <div class="input-group">
                                        <div class="input-group date">
                                            <span class="input-group-text bg-light d-block " id="basic-addon1">
                                                Start date
                                            </span>
                                            <input type="date" class="form-control" id="start_date"
                                                placeholder="Start Date"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="m-1">
                                    <div class="input-group ">
                                        <div class="input-group date">
                                            <span class="input-group-text bg-light d-block " id="basic-addon1">
                                                End date
                                            </span>
                                            <input type="date" class="form-control" id="end_date"
                                                placeholder="End Date"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="m-1">
                                    <select  name="customer_id" id="customer_id" class="form-select" data-control="select2" data-placeholder="Select an customer">
                                        <option value="" disabled selected>Select a customer</option>
                                        <option>ALL</option>
                                            @foreach ($customers as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="m-1">
                                    {{-- <label class="form-label">Cuctomer</label> --}}
                                    <select  name="user_id" id="user_id" class="form-select" data-control="select2" data-placeholder="Select an user">
                                        <option value="" disabled selected>Select a user</option>
                                        <option>ALL</option>
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="d-flex justify-content-end">
                                <button id="filter" class="m-1 mt-1 btn btn-primary">Submit</button>
                                <button id="reset" class="m-1 mt-1 btn btn-warning">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Card body-->
                <br>
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table table-hover table-rounded table-striped border gy-7 gs-7" id="records">
                            <thead>
                                <tr>
                                    <th class="fw-normal fs-6 text-gray-800">No</th>
                                    <th class="fw-normal fs-6 text-gray-800">Company Name</th>
                                    <th class="fw-normal fs-6 text-gray-800">Costumer Name</th>
                                    <th class="fw-normal fs-6 text-gray-800">Total</th>
                                    <th class="fw-normal fs-6 text-gray-800">Date</th>
                                    <th class="fw-normal fs-6 text-gray-800">Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
    {{-- @include("pages.".$path.".".$view.'.form') --}}
@endsection
@push('scripts')
    @include($view.'.script')
@endpush
