@extends('layouts.admin.app')
@section('content')
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">
            <!--begin::Form-->
            <form method="post" action="{{ route('admin.orders.update', $order->id) }}" enctype="multipart/form-data"
                class="form d-flex flex-column flex-lg-row form needs-validation" id="order-form" autocomplete="off"
                novalidate>
                <!-- <form id="kt_ecommerce_edit_order_form" class="" data-kt-redirect="/Adminin/demo20/../demo20/apps/ecommerce/sales/listing.html"> -->
                <!--begin::Aside column-->
                @csrf

                @include('pages.' . $path . '.' . $view . '.form')

                <!--end::Main column-->
            </form>
            <!--end::Form-->
        </div>
    </div>
@endsection
