<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script>
    $(function() {
        $("#start_date").datepicker({
            "dateFormat": "yy-mm-dd",
            
        });
        $("#end_date").datepicker({
            "dateFormat": "yy-mm-dd"
        });
    });

    function fetch(start_date, end_date, customer_id, user_id) {
        let par = {
            start_date : start_date,
            end_date : end_date,
            customer_id : customer_id,
            user_id : user_id,
        }
        let parameter = JSON.stringify(par)
        var table = $('#records').DataTable({
            ajax: `{{ url('admin/product/report/records?data=${parameter}')}}`,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                {data: 'product', name: 'product'},
                {data: 'saled', name: 'saled'},
            ],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'report product saled - '+ (new Date()).toISOString().split('T')[0]
                },
                {
                    extend: 'csvHtml5',
                    title: 'report product saled - '+ (new Date()).toISOString().split('T')[0]
                }
            ]
        });
    }

    fetch();

    $(document).on("click", "#filter", function(e) {
        e.preventDefault();
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var user_id = $("#user_id").val();
        var customer_id = $("#customer_id").val();
        // if (start_date == "" || end_date == "") {
        //     alert("isi tanggal");
        // } else {
        //     $('#records').DataTable().destroy();
        //     fetch(start_date, end_date, customer_id, user_id);
        // }
        
        if (start_date === "" && end_date === "") {
        start_date = null;
        end_date = null;
        } else if (start_date === "" || end_date === "") {
            alert("Pilih Tanggal Start And End");
            return;
        }
        
        $('#records').DataTable().destroy();
        fetch(start_date, end_date, customer_id, user_id);
    });

    $(document).on("click", "#reset", function(e) {
        e.preventDefault();
        $("#start_date").val('');
        $("#end_date").val('');
        $('#records').DataTable().destroy();
        fetch();
        $("#user_id").val(''); 
        $("#user_id option:selected").prop("selected", 'select'); 
        $("#user_id").trigger("change");

        $("#customer_id").val(''); 
        $("#customer_id option:selected").prop("selected", 'select');     
        $("#customer_id").trigger("change");
    });
</script>