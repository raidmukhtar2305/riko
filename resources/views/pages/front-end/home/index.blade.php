@extends('layouts.front-end.app')
@section('content')
   <!-- Start Banner
    ============================================= -->
    <div class="banner-area content-double transparent-nav bg-gradient-theme text-light small-text">
        <div class="box-table">
            <div class="box-cell">
                <div class="container">
                    <div class="double-items">
                        <div class="row align-center">
                            <div class="col-lg-5 left-info simple-video">
                                <div class="content" data-animation="animated fadeInUpBig">
                                    <h1>Let's Easier Control Your System!</h1>
                                    <p>
                                        Lakukan pengawasan kinerja bisnismu menjadi lebih mudah dengan Adminin. Mari wujudkan target bisnismu di masa mendatang!
                                    </p>
                                    <a class="btn circle btn-light border btn-md" target="_blank" href="https://adminin.online/admin/login">Mulai Sekarang</a>
                                </div>
                            </div>
                            <div class="col-lg-7 right-info width-max">
                                <img src="{{ asset('assets3/img/hero-img.png') }}" alt="Thumb">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wavesshape">
                    <img src="{{ asset('assets3/img/shape/2.png') }}" alt="Shape">
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->

    <!-- Start About
    ============================================= -->
    <div id="about" class="about-area default-padding-top">
        <div class="container">
            <div class="row">
                <div class="about-items text-center">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="about-content text-center">
                            <h4>About Us</h4>
                            <h2>Apa itu Adminin?</h2>
                            <p>
                                Adminin merupakan sebuah website dan aplikasi untuk mengontrol jalannya bisnis kamu, mulai dari product, order,
                                invoice sampai dengan pendapatan. Semua itu dapat di pantau dan dioperasikan dengan lebih efektif serta efisien oleh Adminin.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 text-center overview-items">
                    <div class="overview-carousel owl-carousel owl-theme">
                        <img src="{{ asset('assets3/img/overview/1.png') }}" alt="Thumb">
                        <img src="{{ asset('assets3/img/overview/2.png') }}" alt="Thumb">
                        <img src="{{ asset('assets3/img/overview/3.png') }}" alt="Thumb">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End About -->

    <!-- Start Why Chose Us
    ============================================= -->
    <div id="choseus" class="choseus-area default-padding bg-theme-small">
        <div class="container">
            <div class="choseus-items">
                <div class="row align-center">
                    <div class="col-lg-6 thumb">
                        <img src="{{ asset('assets3/img/illustration/1.png') }}" alt="Thumb">
                    </div>
                    <div class="col-lg-6 info">
                        <h5>Why Choose Us?</h5>
                        <h2>Kami Adalah Solusi untuk Mengontrol Bisnis dengan Lebih Efisien</h2>
                        <p>
                            Solusi terbaik untuk anda yang bingung bagaimana mengontrol bisnis dengan baik. Jadi, mulai dari sekarang mulailah pakai adminin. Adminin mudah digunakan, mudah pula melakukan pengontrollan.
                        </p>
                        <a class="btn circle btn-theme border btn-md" target="_blank" href="https://adminin.online/admin/login">Mulai Sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Why Us -->

    <!-- Start Service -->
    <div id="service" class="service-area default-padding bottom-small">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h2>Features</h2>
                    </div>
                </div>
            </div>
            <div class="service-items">
                <div class="row">
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ asset('assets3/img/icon/dashboard.png') }}" alt="icon">
                            </div>
                            <div class="info">
                                <h4>Dashboard</h4>
                                <p>
                                    Memantau dan menganalisis kinerja bisnis secara keseluruhan dengan menampilkan pendapatan, profit dan statistik
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ asset('assets3/img/icon/warehouse.png') }}" alt="icon">
                            </div>
                            <div class="info">
                                <h4>Warehouse</h4>
                                <p>
                                    Memantau stok barang diberbagai gudang dan mengelolanya di berbagai cabang
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ asset('assets3/img/icon/outlet.png') }}" alt="icon">
                            </div>
                            <div class="info">
                                <h4>Outlet</h4>
                                <p>
                                    Memungkinkan pengguna untuk melakukan transaksi secara lebih efisien di berbagai outlet yang ada
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ asset('assets3/img/icon/product.png') }}" alt="icon">
                            </div>
                            <div class="info">
                                <h4>Product Management</h4>
                                <p>
                                    Mengelola katalog produk dengan mudah. Menambah, mengedit, atau mengatur informasi terkait produk
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ asset('assets3/img/icon/crm.png') }}" alt="icon">
                            </div>
                            <div class="info">
                                <h4>Customer Management</h4>
                                <p>
                                    Memungkinkan pengguna membuat akun, nantinya akan berfungsi untuk memahami preferensi serta kebutuhan pnegguna
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="icon">
                                <img src="{{ asset('assets3/img/icon/cashier.png') }}" alt="icon">
                            </div>
                            <div class="info">
                                <h4>Cashier</h4>
                                <p>
                                    Memungkinkan transaksi melalui kasir untuk proses pembayaran, perhitungan serta pencatatan pesanan
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End service -->

    <!-- Start Subscribe
    ============================================= -->
    <div id="contact" class="subscribe-area bg-fixed shadow dark text-light default-padding text-center my-5" style="background-image: url(assets3/img/banner/4.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <h3>Contact Us</h3>
                    <p>
                        Hubungi Kami jika anda berminat menggunakan Adminin untuk bisnis anda, kami akan mengonfirmasi permintaan anda dan memandu anda mewujudkan target bisnis anda
                    </p>
                    <div class="contact-btn py-4">
                        <a href="https://wa.me/0895333581785" target="_blank" class="btn circle btn-theme effect btn-md">Hubungi Sekarang <i class="fas fa-phone px-3"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Subscribe -->
@endsection
