@extends('layouts.front-end.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
              <div class="container mb-5 mt-3">
                <div class="row d-flex align-items-baseline">
                  <div class="col-xl-9">
                    <p style="color: #7e8d9f;font-size: 20px;">Invoice >> <strong>ID: {{ $quotation->quotation_code }}</strong></p>
                  </div>
                  <hr>
                </div>
          
                <div class="container">
                  <div class="col-md-12">
                    <div class="text-center">
                      {{-- <i class="fab fa-mdb fa-4x ms-0" style="color:#5d9fc5 ;"></i> --}}
                      <h2 class="pt-0 fw-bold text-success">{{ $quotation->company->name }}</h2>
                    </div>
          
                  </div>
          
          
                  <div class="row">
                    <div class="col-xl-8">
                      <ul class="list-unstyled">
                        <li class="text-muted">To: <span style="color:#5d9fc5 ;">{{ $quotation->name_buyer }}</span></li>
                        <li class="text-muted">No Rekening : {{ $quotation->no_rek_buyer }}</li>
                        {{-- <li class="text-muted">State, Country</li>
                        <li class="text-muted"><i class="fas fa-phone"></i> 123-456-789</li> --}}
                      </ul>
                    </div>
                    <div class="col-xl-4">
                      <p class="text-muted">Invoice</p>
                      <ul class="list-unstyled">
                        <li class="text-muted"><i class="fas fa-circle text-success" style="text-success"></i> <span
                            class="fw-bold">ID:</span>#{{ $quotation->quotation_code }}</li>
                        <li class="text-muted"><i class="fas fa-circle text-success" style="text-success"></i> <span
                            class="fw-bold">Creation Date: </span>{{ $quotation->date }}</li>
                        <li class="text-muted"><i class="fas fa-circle text-success" style="text-success"></i> <span
                            class="me-1 fw-bold">Status:</span>
                            @if($quotation->status == 0)
                                <span class="badge bg-warning text-black fw-bold">
                                unpaid</span>
                                @else
                                <span class="badge bg-success text-black fw-bold">
                                approve</span>
                            @endif
                        </li>
                      </ul>
                    </div>
                  </div>
          
                  <div class="row my-2 mx-1 justify-content-center">
                    <table class="table table-striped table-borderless">
                      <thead style="background-color:#006400;" class="text-white">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Product Name</th>
                          <th scope="col">Qty</th>
                          <th scope="col">Price</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($quotation->quotation_details as $key => $item)     
                        <tr>
                          <th scope="row">{{ $key+1 }}</th>
                          <td>{{ $item->product_name }}</td>
                          <td>{{ $item->qty }}</td>
                          <td>Rp. {{ number_format($item->price) }}</td>
                          {{-- <td>$800</td> --}}
                        </tr>
                        @endforeach
                       
                      </tbody>
          
                    </table>
                  </div>
                  <div class="row">
                    <div class="col-xl-8">
                      <p class="ms-3">Add additional notes and payment information</p>
          
                    </div>
                    <div class="col-xl-3">
                      <ul class="list-unstyled">
                        <li class="text-muted ms-3"><span class="text-black me-4">SubTotal</span>Rp. {{ number_format($quotation->total) }}</li>
                      </ul>
                      <p class="text-black float-start"><span class="text-black me-3"> Total Amount</span><span
                          style="font-size: 25px;">Rp. {{ number_format($quotation->total) }}</span></p>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-xl-10">
                      <p>Terima Kasih telah berbelanja di toko kami</p>
                    </div>
                  
                  </div>
          
                </div>
              </div>
            </div>
          </div>
    </div>
@endsection
