@extends('layouts.front-end.app')

@section('content')
    <div class="container">
        <div class="mt-4 mb-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Checkout</li>
                    <li class="breadcrumb-item">Pembayaran</li>
                </ol>
            </nav>
        </div>
        <div class="d-flex justify-content-center align-items-center">
            <div class="card w-75 border-0">
                <form action="{{ route('order.checkout.submit') }}" class="checkout" method="POST" name="checkout">
                    @csrf
                    <div class="row">
                        {{-- <div class="col-md-4">
                            <div class="card border-0 rounded bg-light">
                                <div class="card-body px-4 mb-4">
                                    <ul class="px-4 list-unstyled">
                                        <h3 class="card-title mt-2 fw-bold">List Product</h3>
                                        @php
                                        $grand_total = 0;
                                    @endphp
                                    @foreach ($carts = session('cart') as $key => $data)
                                        <li class="d-flex justify-content-between mt-4">
                                            <p class="m-0 fw-bold">{{ $data['name'] }}</p>
                                            <p class="m-0 fw-bold">Rp. {{ number_format($data['price']) }}</p>
                                        </li>
                                        @php
                                            $grand_total += $data['price'] * $data['qty'];
                                        @endphp
                                    @endforeach
                                    <li class="d-flex justify-content-between mt-5">
                                        <p class="m-0 fw-bold">Total</p>
                                        <p class="m-0 fw-bold fs-5 custom-text">Rp. {{ number_format($grand_total) }}</p>
                                    </li>
                                    </ul>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-md-6">
                            <div class="card border-0 rounded bg-light">
                                <div class="card-body px-4 mb-4">
                                    <ul class="px-4 list-unstyled">
                                        <h3 class="card-title mt-2 fw-bold">Pembayaran</h3>
                                        <li class="d-flex justify-content-between mt-5">
                                            <p class="m-0 fw-bold">Bank Name</p>
                                            <p class="m-0 fw-bold">{{ $company->bank_name }}</p>
                                        </li>
                                        <li class="d-flex justify-content-between mt-5">
                                            <p class="m-0 fw-bold">No Rek</p>
                                            <p class="m-0 fw-bold c-a-nu">{{ $company->account_number }}</p>
                                        </li>
                                        <li class="d-flex justify-content-between mt-5">
                                            <p class="m-0 fw-bold">A/N</p>
                                            <p class="m-0 fw-bold c-a-na">{{ $company->account_name }}</p>
                                        </li>
                                        <li class="d-flex justify-content-between mt-5">
                                            <p class="m-0 fw-bold">Outlet</p>
                                            <p class="m-0 fw-bold">{{ $company->name }}</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card border">
                                <div class="card-body">
                                    <div class="mb-10 fv-row">
                                        <label for="outlet_id" class="form-label fw-bold">Outlet</label>
                                        <select name="outlet_id" id="outlet_id"
                                            class="form-control form-control-solid form-select mb-2">
                                            <option value="" disabled selected>--Pilih Outlet--</option>
                                            @foreach ($outlets as $data)
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label for="" class="form-label fw-bold">Kasir</label>
                                        <select name="user_id" id=""
                                            class="form-control form-control-solid form-select mb-2">
                                            <option value="" disabled selected>--Pilih Kasir--</option>
                                            @foreach ($cashiers as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="mb-10 fv-row">
                                        <label for="" class="form-label fw-bold">Bank</label>
                                        <select name="" id=""
                                            class="form-control form-control-solid form-select mb-2">
                                            <option value="" disabled selected>--Pilih Bank--</option>
                                            <option value="1">BCA</option>
                                        </select>
                                    </div> --}}
                                    <div class="mb-10 fv-row">
                                        <label for="name_buyer" class="form-label fw-bold">A/N</label>
                                        <input type="text" class="form-control form-control-solid" name="name_buyer"
                                            placeholder="Atas Nama" />
                                    </div>
                                    <div class="mb-10 fv-row">
                                        <label for="no_rek_buyer" class="form-label fw-bold">No Rekening</label>
                                        <input type="text" class="form-control form-control-solid" name="no_rek_buyer"
                                            placeholder="No Rekening" />
                                    </div>

                                    <button type="submit"
                                        class="btn btn-warning shadow-sm border-0 fw-bold text-white w-100 mt-5 p-2">Konfirmasi
                                        Pembayaran</button>
                                        <a href="{{ route('order.cart') }}" class="btn btn-danger shadow-sm border-0 fw-bold text-white w-100 mt-3 p-2">Batalkan<a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
<script>
    // $(document).ready(function() {
    //     $('input[name=no_rek_buyer]').on('keyup', function() {
    //         $('.c-a-nu').html($(this).val())
    //     })
    // })

    // $(document).ready(function() {
    //     $('input[name=name_buyer]').on('keyup', function() {
    //         $('.c-a-na').html($(this).val())
    //     })
    // })
</script>
@endpush
