@extends('layouts.front-end.app')

@section('content')
    <div class="container">
        <div class="mt-4 mb-4">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Product</li>
                    <li class="breadcrumb-item">checkout</li>
                </ol>
            </nav>
        </div>
        <div class="row gx-2">
            <div class="col-md-8">
                <div class="col-md-11">
                    <div class="row">
                        @if (!empty(session('cart')))
                            @foreach ($carts = session('cart') as $key => $data)
                            <div class="col-md-6 mb-3">
                                <div class="card shadow-sm">
                                    <div class="card-body mb-2">
                                        <div class="d-flex justify-content between">
                                            <h5 class="card-title mt-2 fw-bold"><i class="fas fa-gear gear-icon"></i>
                                                {{ $data['name'] }}</h5>
                                            {{-- <div class="form-check">
                                                <input class="custom-checkbox" type="checkbox" value="">
                                            </div> --}}
                                        </div>
                                        <div class="card-body bg-light border-0 rounded mt-2">
                                            <div class="d-flex justify-content-between mt-2">
                                                <p class="m-0 fw-bold">Nama :</p>
                                                <a href=""
                                                    class="m-0 btn p-0 fw-bold">{{ $data['name'] }}</a>
                                            </div>
                                            <div class="d-flex justify-content-between mt-2">
                                                <p class="m-0 fw-bold">Stok:</p>
                                                <p class="m-0 fw-bold">{{ $data['stock'] }}</p>
                                            </div>
                                            <div class="d-flex justify-content-between mt-2">
                                                <p class="m-0 fw-bold">Harga:</p>
                                                <p class="m-0 fw-bold">Rp. {{ number_format($data['price']) }}</p>
                                            </div>
                                            <a href="{{ route('order.cart.delete', $key) }}"
                                            class="btn btn-danger costum-danger shadow-sm border-0 fw-bold text-white w-100 mt-4 p-2">
                                            <i class="fas fa-trash"></i> Hapus</a>
                                        </div>
                                        <div class="card-body bg-light border-0 rounded mt-3" id="headingOne">
                                            <div class="accordion accordion-flush border-0 rounded" id="accordionFlushExample">
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header bg-light" id="flush-headingOne{{ $key }}">
                                                        <button class="accordion-button collapsed bg-light p-2" type="button"
                                                            data-bs-toggle="collapse" data-bs-target="#flush-collapseOne{{ $key }}"
                                                            aria-expanded="false" aria-controls="flush-collapseOne">
                                                            <span class="fw-bold">Deskripsi</span>
                                                        </button>
                                                    </h2>
                                                    <div id="flush-collapseOne{{ $key }}" class="accordion-collapse collapse"
                                                        aria-labelledby="flush-headingOne{{ $key }}"
                                                        data-bs-parent="#accordionFlushExample">
                                                        <div class="accordion-body bg-light">{{ $data['description'] }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-4">
                <div class="card shadow-sm">
                    <div class="card-body mb-2">
                        <h5 class="card-title mt-2 fw-bold"><i class="fas fa-cart-shopping"></i> Keranjang Anda</h5>
                        <ul class="list-unstyled">
                            @if (!empty(session('cart')))
                                @php
                                    $grand_total = 0;
                                @endphp
                                @foreach ($carts = session('cart') as $key => $data)
                                    <li class="d-flex justify-content-between mt-4">
                                        <p class="m-0 fw-bold">{{ $data['name'] }}</p>
                                        <p class="m-0 fw-bold">Rp. {{ number_format($data['price']) }}</p>
                                    </li>
                                    @php
                                        $grand_total += $data['price'] * $data['qty'];
                                    @endphp
                                @endforeach
                                <li class="d-flex justify-content-between mt-4">
                                    <p class="m-0 fw-bold">Total</p>
                                    <p class="m-0 fw-bold fs-5 custom-text">Rp. {{ number_format($grand_total) }}</p>
                                </li>
                                <a href="{{ route('order.checkout') }}" class="btn btn-warning shadow-sm fw-bold text-white w-100 mt-4">Bayar
                                    Sekarang</a>
                                @else
                                <p class="mt-4">Pilih Produk Terlebih Dahulu!</p>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

<script>
    function changeColor(checkbox) {
        var customCheckbox = checkbox.nextElementSibling;
        if (checkbox.checked) {
            customCheckbox.style.backgroundColor = "yellow";
        } else {
            customCheckbox.style.backgroundColor = "#006400";
        }
    }
</script>
