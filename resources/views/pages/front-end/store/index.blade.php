@extends('layouts.front-end.app')

@section('content')
    <div class="container">
        <div class="mt-4 mb-4">
            <nav aria-label="breadcrumb">
                {{-- <ol class="breadcrumb">
                    <li class="breadcrumb-item">Product</li>
                    <li class="breadcrumb-item">Detail Toko</li>
                </ol> --}}
            </nav>
        </div>
        <div class="row gx-5">
            <div class="col-md-4">
                <p class="fw-bold custom-text">Toko Kami</p>
                <div class="mt-3 mb-4">
                    <div class="card custom-card rounded">
                        {{-- @foreach ($company as $item) --}}
                        <div class="card-body">
                            <ul class="px-4 list-unstyled">
                                <h5 class="card-title mt-3"><i class="fas fa-shop"></i> {{ $company->name }}</h5>
                                <li class="d-flex justify-content-between mt-4">
                                    <p class="m-0 fw-bold">Nama Toko:</p>
                                    <p class="m-0 fw-bold">{{ $company->name }}</p>
                                </li>
                                <li class="d-flex justify-content-between mt-4">
                                    <p class="m-0 fw-bold">Pemilik Toko:</p>
                                    <p class="m-0 fw-bold">Dwi Septio</p>
                                </li>
                                <li class="d-flex justify-content-between mt-4">
                                    <p class="m-0 fw-bold">Email:</p>
                                    <p class="m-0 fw-bold">{{ $company->email }}</p>
                                </li>
                                <li class="d-flex justify-content-between mt-4">
                                    <p class="m-0 fw-bold">No Whatsapp:</p>
                                    <p class="m-0 fw-bold">{{ $company->phone }}</p>
                                </li>
                                <li class="d-flex justify-content-between mt-4">
                                    <p class="m-0 fw-bold">Alamat:</p>
                                    <p class="m-0 fw-bold">{{ $company->address }}</p>
                                </li>
                            </ul>
                        </div>
                        {{-- @endforeach --}}
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="col-md-11">
                    <div class="row">
                        <p class="fw-bold custom-text">Toko Kami</p>

                        @foreach ($product as $data)
                        <div class="col-md-6 mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body mb-2">
                                    <h5 class="card-title mt-2 fw-bold"><i class="fas fa-gear gear-icon"></i> {{ $data->name }}</h5>
                                    <div class="card-body bg-light border-0 rounded mt-2">
                                        <div class="d-flex justify-content-between mt-2">
                                            <p class="m-0 fw-bold">Nama:</p>
                                            <a href="#"
                                                class="m-0 btn p-0 fw-bold">{{ $data->name }}</a>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <p class="m-0 fw-bold">Stok:</p>
                                            <p class="m-0 fw-bold">{{ $data->stock }}</p>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <p class="m-0 fw-bold">Harga:</p>
                                            <p class="m-0 fw-bold custom-text">Rp.
                                                {{ number_format($data->price) }}</p>
                                        </div>
                                        <a href="{{ route('product.addToCart', $data->id) }}"
                                            class="btn btn-succes custom-btn fw-bold text-white w-100 mt-4"><i
                                            class="fas fa-cart-shopping"></i> Add to cart
                                        </a>
                                    </div>
                                    {{-- @foreach ($product as $value) --}}

                                    <div class="card-body bg-light border-0 rounded mt-3" id="headingOne">
                                        <div class="accordion accordion-flush border-0 rounded" id="accordionFlushExample {{ $data->id }}">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header bg-light" id="flush-headingOne {{ $data->id }}">
                                                    <button class="accordion-button collapsed bg-light p-2" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#flush-collapseOne{{ $data->id }}"
                                                        aria-expanded="false" aria-controls="flush-collapseOne {{ $data->id }}">
                                                        <span class="fw-bold">Deskripsi</span>
                                                    </button>
                                                </h2>
                                                <div id="flush-collapseOne{{ $data->id }}" class="accordion-collapse collapse"
                                                    aria-labelledby="flush-headingOne{{ $data->id }}"
                                                    data-bs-parent="#accordionFlushExample">
                                                    <div class="accordion-body bg-light">{{ $data->description }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
