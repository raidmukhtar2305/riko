@extends('layouts.front-end.app')

@section('content')
    <div class="container">
        <div class="p-3">

            <div class="mt-4 mb-4">
            </div>
            <div class="row gx-5">
                <div class="col-md-4">
                    <div class="mt-3 mb-4">
                        <div class="card custom-card rounded">
                            <div class="card-body">
                                <ul class="px-4 list-unstyled">
                                    <h5 class="card-title mt-3"><i class="fas fa-shop"></i> Produk 1</h5>
                                    <li class="d-flex justify-content-between mt-4">
                                        <p class="m-0 fw-bold">Nama Toko:</p>
                                        <p class="m-0 fw-bold">{{ $company->name }}</p>
                                    </li>
                                    <li class="d-flex justify-content-between mt-4">
                                        <p class="m-0 fw-bold">Pemilik Toko:</p>
                                        <p class="m-0 fw-bold">{{ $company->name }}</p>
                                    </li>
                                    <li class="d-flex justify-content-between mt-4">
                                        <p class="m-0 fw-bold">Email:</p>
                                        <p class="m-0 fw-bold">{{ $company->email }}</p>
                                    </li>
                                    <li class="d-flex justify-content-between mt-4">
                                        <p class="m-0 fw-bold">No Whatsapp:</p>
                                        <p class="m-0 fw-bold">{{ $company->phone }}</p>
                                    </li>
                                    <li class="d-flex justify-content-between mt-4">
                                        <p class="m-0 fw-bold">Alamat:</p>
                                        <p class="m-0 fw-bold">{{ $company->address }}</p>
                                    </li>
                                </ul>
                                <button type="button"
                                    class="btn btn-outline-light shadow-sm fw-bold text-white w-100 mt-5"></i> Lihat
                                    Toko</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-11">
                        <div class="mt-3 mb-4">
                            <div class="card rounded shadow-sm">
                                <div class="card-body mb-3">
                                    <h2 class="card-title mt-4 px-3 fw-bold"><i class="fas fa-gear gear-icon"></i>
                                        {{ $product->name }}</h2>
                                    <div class="row px-3 gx-3">
                                        <div class="col-md-5">
                                            <div class="card border-0">
                                                <div class="card-body rounded">
                                                    <div class="d-flex justify-content-between mt-4">
                                                        <p class="m-0 fw-bold">Nama:</p>
                                                        <a href=""
                                                            class="m-0 btn p-0 fw-bold">{{ $product->name }}</a>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-4">
                                                        <p class="m-0 fw-bold">Stok:</p>
                                                        <p class="m-0 fw-bold">{{ $product->stock }}</p>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-4 mb-3">
                                                        <p class="m-0 fw-bold">Harga:</p>
                                                        <p class="m-0 fw-bold custom-text">Rp.
                                                            {{ number_format($product->price) }}</p>
                                                    </div>
                                                    <a href="{{ route('product.addToCart', $product->id) }}"
                                                        class="btn btn-succes custom-btn shadow-sm fw-bold text-white w-100 mt-2"><i
                                                            class="fas fa-cart-shopping"></i> Add to cart</a>
                                                    <a href="{{ route('order.checkout') }}"
                                                        class="btn btn-succes bg-warning shadow-sm fw-bold text-white w-100 mt-4 mb-2">
                                                        Bayar Sekarang
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card-body bg-light border-0 rounded mt-2">
                                                <p class="fw-bold">Deskripsi</p>
                                                <p>{{ $product->description }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
