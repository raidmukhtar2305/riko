{{-- <footer class="green-footer mt-5 footer">
    <div class="container">
        <p class="fw-bold">&copy; 2023 <span class="fw-normal">Copyright Robust</span></p>
    </div>
</footer> --}}
 <!-- Start Footer
    ============================================= -->
    <footer class="default-padding bg-light">
        <div class="container">
            <div class="f-items">
                <div class="row">
                    <div class="col-lg-4 col-md-6 item d-flex justify-content-between">
                        <div class="f-item">
                            <img src="{{ asset('assets3/img/logo.svg') }}" alt="Logo">
                            <p>
                                Manage bisnis kamu dari sekarang! Bersama kami, mari capai target bisnis kamu dimasa mendatang!
                            </p>
                            <a href="https://wa.me/0895333581785" target="_blank" class="btn circle btn-theme effect btn-md">Request</a>
                        </div>
                    </div>
                    <div class="offset-lg-1 col-lg-3 col-md-6 item">
                        <div class="f-item link">
                            <h4>Navigation</h4>
                            <ul class="">
                                <li>
                                    <a href="#home"><i class="fas fa-angle-right"></i> Home</a>
                                </li>
                                <li>
                                    <a href="#about"><i class="fas fa-angle-right"></i> About</a>
                                </li>
                                <li>
                                    <a href="#service"><i class="fas fa-angle-right"></i> Features</a>
                                </li>
                                <li>
                                    <a href="#contact"><i class="fas fa-angle-right"></i> Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 item">
                        <div class="f-item twitter-widget">
                            <h4>Contact Info</h4>
                            <p>
                                Info lebih lanjut seputar Adminin, dapat hubungi kontak kami di bawah ini
                            </p>
                            <div class="address">
                                <ul>
                                    <!-- <li>
                                        <div class="icon">
                                            <i class="fas fa-home"></i>
                                        </div>
                                        <div class="info">
                                            <h5>Website:</h5>
                                            <span>www.web.com</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fas fa-envelope"></i>
                                        </div>
                                        <div class="info">
                                            <h5>Email:</h5>
                                            <span>support@web.com</span>
                                        </div>
                                    </li> -->
                                    <li>
                                        <div class="icon">
                                            <i class="fas fa-phone"></i>
                                        </div>
                                        <div class="info">
                                            <h5>Phone:</h5>
                                            <span>+62 895-3335-81785</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Footer Bottom -->
            <div class="footer-bottom">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p>&copy; Copyright 2023. All Rights Reserved by <a target="_blank" href="https://wa.me/0895333581785">adminin</a></p>
                        </div>
                        <!-- <div class="col-lg-6 text-right link">
                            <ul>
                                <li>
                                    <a href="#">Terms of user</a>
                                </li>
                                <li>
                                    <a href="#">License</a>
                                </li>
                                <li>
                                    <a href="#">Support</a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- End Footer Bottom -->
        </div>
    </footer>
    <!-- End Footer -->
