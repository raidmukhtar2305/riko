<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SOMS - Social Media Specialist">

    <!-- ========== Page Title ========== -->
    <title>Adminin - Maanage Your Business</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="assets3/img/favicon.png" type="image/x-icon">

    <!-- ========== Start Stylesheet ========== -->
    <link href="{{ asset('assets3/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets3/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets3/css/elegant-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets3/css/flaticon-set.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets3/css/owl.carousel.min.cs') }}s" rel="stylesheet" />
    <link href="{{ asset('assets3/css/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets3/css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets3/css/bootsnav.css') }}" rel="stylesheet" />
    <link href="{{ asset('style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets3/css/responsive.css') }}" rel="stylesheet" />
    <!-- ========== End Stylesheet ========== -->

    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">

</head>

<body>

    <!-- Preloader Start -->
    <div class="se-pre-con"></div>
    <!-- Preloader Ends -->

    <!-- Header
    ============================================= -->
    @include('layouts.front-end.inc.header')

    <!-- End Header -->

    @yield('content')

    <!-- Start Footer
    ============================================= -->
    @include('layouts.front-end.inc.footer')

    <!-- End Footer -->

    <!-- jQuery Frameworks
    ============================================= -->
    <script src="{{ asset('assets3/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('assets3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets3/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets3/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets3/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets3/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets3/js/modernizr.custom.13711.js') }}"></script>
    <script src="{{ asset('assets3/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets3/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets3/js/count-to.js') }}"></script>
    <script src="{{ asset('assets3/js/bootsnav.js') }}"></script>
    <script src="{{ asset('assets3/js/main.js') }}"></script>

</body>
</html>
