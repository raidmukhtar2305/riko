<?php

return [
    'role_permissions' => [
        'form_generator_values' => ['list', 'create', 'update', 'destroy']
    ],
    'order_status' => [
        'pending' => '0',
        'approve' => '1'
    ],
    'procurement_status' => [
        'pending' => '0',
        'approve' => '1'
    ],
    'table_references' => [
        'refund_detail' => 'refund_detail',
        'order_detail' => 'order_detail',
        'procurement_detail' => 'procurement_detail',
    ]
];
