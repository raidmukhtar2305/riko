<?php

namespace App\Services;

use App\Models\Invoice;
use App\Models\Order;
use App\Models\Product;
use App\Models\StockLog;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    public static function create($payload)
    {
        $order_data = [
            'order_code' => order_code(),
            'company_id' => data_get($payload, 'company_id'),
            'outlet_id' => data_get($payload, 'outlet_id'),
            'customer_id' => data_get($payload, 'customer_id'),
            'date' => $payload['date'],
            'description' => data_get($payload, 'description'),
            'status' => 0,
            'created_by' => Auth::user()->id,
        ];
        $order = Order::create($order_data);

        $total = 0;
        foreach ($payload['product_id'] as $key => $value) {
            $product = Product::find($value);
            $product_qty = $payload['product_qty'][$key];

            $order_detail = $order
                ->order_details()
                ->create([
                    'product_id'            => $value,
                    'qty'                   => $product_qty,
                    'price'                 => $product->price,
                    'profit_unit'           => floatval($product->profit),
                    'profit'                => $product->profit * $product_qty,
                    'product_name'          => $product->name,
                    'product_description'   => $product->description,
                ]);

            if (!$product->is_service) {
                StockLog::create([
                    'product_id' => $value,
                    'reference_id' => $order_detail->id,
                    'reference_slug' => config('constants.table_references.order_detail'),
                    'qty' => $order_detail->qty * -1,
                    'company_id' => data_get($order, 'company_id'),
                    'warehouse_id' => data_get($order, 'outlet.warehouse_id')
                ]);
            }

            $total += $product->price * $product_qty;
        }

        $order->update(['total' => $total]);

        // create invoice
        if (data_get($payload, 'is_paid')) {
            $payloadInv['reference'] = 'INV' . date('YmdHis');
            $payloadInv['company_id'] = $payload['company_id'];
            $payloadInv['date'] = $payload['date'];
            $payloadInv['note'] = data_get($payload, 'invoice_note');
            $payloadInv['description'] = data_get($payload, 'invoice_description');
            $payloadInv['nominal'] = $total;
            $payloadInv['status'] = '1';
            $payloadInv['order_id'] = $order->id;
            Invoice::create($payloadInv);
        }

        return $order;
    }
}
