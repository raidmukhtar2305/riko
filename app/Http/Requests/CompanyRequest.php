<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'logo' => 'required',
            'email' => 'required|email|',
            'address' => 'required',
            'phone' => 'required|max:13',
            'bank_name' => 'required',
            'description' => 'required',
            'account_name' => 'required',
            'account_number' => 'required',

        ];
        return $rules;
    }
}
