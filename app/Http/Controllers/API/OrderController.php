<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\StockLog;
use App\Models\OrderDetail;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $payload = $request->all();
            if (Auth::user()->hasRole('superadmin')) {
                $data = Order::orderBy('created_at', 'desc')->with('customer');
            } else {
                $data = Order::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->with('customer');
            }

            if (isset($payload['search']))
                $data = $data->whereHas('customer', function ($q) use ($payload) {
                    $q->where('name', 'like', '%' . $payload['search'] . '%');
                });

            $data = $data->paginate(10);

            $response["code"] = 200;
            $response["message"] = 'successfully';
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
            $data = null;
        }

        return response()->json($data, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                // 'customer_id' => ['required'],
                'date' => ['required'],
            ]);

            $input = $request->all();

            if (Auth::user()->hasRole('superadmin')) {
                // $company    = $input['company_id'];
                $outlet     = $input['outlet_id'];
            } else {
                $company    = Auth::user()->company_id;
                $outlet     = Auth::user()->company_id;
            }

            $order_data = [
                'order_code'    => order_code(),
                'date'          => $input['date'],
                'description'   => $input['description'],
                'customer_id'   => !empty($input['customer_id']) ? $input['customer_id'] : null,
                'status'        => 0,
                'company_id'    => Auth::user()->company_id,
                'created_by'    => Auth::user()->id,
                'outlet_id'     => $outlet,
            ];

            // dd($order_data);
            $order = Order::create($order_data);

            $total = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                if (isset($input['new_prices'][$key])) {
                    $productPrice = $input['new_prices'][$key];
                } else {
                    $productPrice = $product->price;
                }
                $order_detail_data = [
                    'order_id' => $order->id,
                    'product_id' => $value,
                    'qty' => $input['qty'][$key],
                    'price' => $productPrice,
                    'profit_unit' => floatval($product->profit),
                    'profit' => $product->profit * $input['qty'][$key],
                    'product_name' => $product->name,
                    'product_description' => $product->description,
                ];

                $order_detail_data = OrderDetail::create($order_detail_data);

                $total += $productPrice * $input['qty'][$key];
                $stock = $product->stock - $input['qty'][$key];
                $product->update(['stock' => $stock]);

                if (!$product->is_service) {
                    $stock_log_data = [
                        'product_id' => $value,
                        'reference_id' => $order_detail_data->id,
                        'reference_slug' => config('constants.table_references.order_detail'),
                        'qty' => $order_detail_data->qty,
                        'company_id' => $company
                    ];
                    StockLog::create($stock_log_data);
                }
            }

            Order::find($order->id)->update(['total' => $total]);

            if ($request->is_paid) {
                $payloadInv['reference'] = 'INV' . date('YmdHis');
                $payloadInv['company_id'] = $input['company_id'];
                $payloadInv['date'] = $input['date'];
                $payloadInv['note'] = $input['invoice_note'];
                $payloadInv['description'] = $input['invoice_description'];
                $payloadInv['nominal'] = $total;
                $payloadInv['status'] = '1';
                $payloadInv['order_id'] = $order->id;
                Invoice::create($payloadInv);
            }
            $order = Order::with(['order_details', 'customer'])->whereId($order->id)->first();
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $order
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = Order::with('customer', 'company', 'user', 'order_details', 'order_details.product')->find($id);

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $order = Order::find($id);
            $data = $order->delete();
            OrderDetail::whereOrderId($id)->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully deleted order ' . $order->order_code,
                'data' => $order
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function download($id)
    {
        try {
            $time = time();
            $date = date("Y-m-d");
            $order = Order::with('order_details', 'order_details.product', 'company', 'customer',)->findOrFail($id);
            $pdf = Pdf::loadview('pages.admin.orders.print', compact('order'))->setPaper('a4', 'potrait');
            return $pdf->download($date . ' - ' . $order->order_code . '.pdf');
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
            return response()->json($response, $response['code']);
        }
        return response()->json($response, $response['code']);
    }
}
