<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\DB;


class GrafikController extends Controller
{
    public function profitByMonth(){
        $months = ['January', 'February', 'March', 'April', 'May', 'Juny', 'July', 'Agust', 'September', 'October', 'November', 'December'];
        foreach ($months as $key => $month) {
            $profitByMonth[] = Order::query()
                ->where(DB::raw("MONTHNAME(date)"), $month)
                ->whereYear('date', now()->year)
                ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                ->sum('profit');
            }
        $response = [
            'code' => 200,
            'message' => 'successfully',
            'data' => $profitByMonth,
            'month' => $months,
        ];

        return response()->json($response, $response['code']);
    }
        
        
     public function orderByMonth(){
        $months = ['January', 'February', 'March', 'April', 'May', 'Juny', 'July', 'Agust', 'September', 'October', 'November', 'December'];
        foreach ($months as $key => $month) {
            $orderByMonth[] = Order::query()
                ->where(DB::raw("MONTHNAME(date)"), $month)
                ->whereYear('date', now()->year)
                ->count();
        }

        $response = [
            'code' => 200,
            'message' => 'successfully',
            'data' => $orderByMonth,
            'month' => $months,
        ];

        return response()->json($response, $response['code']);
    }
}
