<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\StockLog;
// use App\Models\ProductImage;
use App\Helpers\FileHelper;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $payload = $request->all();

            if (Auth::user()->hasRole('superadmin')) {
                $data = Product::orderBy('created_at', 'desc')->orderBy('name', 'ASC');
            } else {
                $data = Product::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->orderBy('name', 'ASC');
            }
            
            if ( isset($payload['sku_code'])) {
                $data = $data->where('sku_code', $payload['sku_code']);
            }elseif ( isset($payload['search'])){
                $data = $data->where('name', 'like', '%'.$payload['search'].'%');
            }
            
            $data = $data->paginate(10);

            $response["code"] = 200;
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($data, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name'          => ['required'],
                'price'         => ['required'],
                // 'stock'         => ['required'],
                // 'company_id'    => ['required'],
                'category_id'   => ['required'],
            ]);
            $input = $request->all();
            // dd($input);

            $input['company_id'] = Auth::user()->hasRole('superadmin') ? $request['company_id'] : $request['company_id'] = Auth::user()->company_id;

            isset($input['is_service']) && $input['is_service'] == 1 ? $input['stock'] = NULL : '' ;

            $product = Product::create($input);

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $product
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = Product::find($id);

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $product = Product::find($id);
            
            isset($input['is_service']) && $input['is_service'] == 1 ? $input['stock'] = NULL : '' ;

            $input['company_id'] = Auth::user()->hasRole('superadmin') ? $request['company_id'] : $request['company_id'] = Auth::user()->company_id;

            $data = $product->update($input);

            $response = [
                'code' => 200,
                'message' => 'successfully update product ' .$product->name,
                'data' => $product
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            // ProductImage::whereProductId($id)->delete();
            $product = Product::find($id);
            $product->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully deleted',
                'data' => $product
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function stockLog($product)
    {
        try {
            $data = StockLog::whereProductId($product)->get();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
