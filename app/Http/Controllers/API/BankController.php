<?php

namespace App\Http\Controllers\API;

use App\DataTables\BanksDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankRequest;
use Illuminate\Http\Request;
use App\Models\Bank;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class BankController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Bank $bank)
    {
        // $this->middleware('can:bank.list')->only('index');
        // $this->middleware('can:bank.create')->only('store');
        // $this->middleware('can:bank.update')->only('update');
        // $this->middleware('can:bank.delete')->only('destroy');

        $this->model = $bank;
        $this->view = 'banks';
        $this->path = 'admin';
        $this->route = 'admin.banks';
        $this->title = 'Data Master';

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(BanksDataTable $dataTable, Request $request)
    {
        View::share('breadcrumbs', [[$this->title, route($this->route . '.index')], ['Bank Management', route($this->route . '.index')]]);
        $categories = Auth::user()->hasRole('superadmin') ? Category::get() : Category::whereCompanyId(Auth::user()->company_id)->get();

        return $dataTable->render('pages.' . $this->path . '.' . $this->view . '.index', compact('categories'));
    }

    public function store(BankRequest $request)
    {
        try {
            $input = $request->all();
            $input['company_id'] = Auth::user()->company_id;
            if (Auth::user()->hasRole('superadmin')) $input['company_id'] = $request['company_id'];
            $bank = $this->model->create($input);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $bank,
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage(),
            ];
            return response()->json($response, 500);
        }
    }

    public function show($id)
    {
        try {
            $data = $this->model->with('category')->find($id);
            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data,
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage(),
            ];
            return response()->json($response, 500);
        }
    }

    public function detail($id)
    {
        $data = $this->model->find($id);
        $bank = [];
        $account = [];
        $bank = [];

        $name = json_encode($name);
        $account = json_encode($account);
        $bank = json_encode($bank);

        return view('pages.admin.banks.detail', compact('name', 'account', 'bank'));
    }

    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $bank = $this->model->find($id);

            if (Auth::user()->hasRole('superadmin')) {
                $input['company_id'] = $request['company_id'];
            }
            $data = $bank->update($input);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data,
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => [],
            ];
            return response()->json($response, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $bank = $this->model->find($id);
            $data = $bank->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data,
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => [],
            ];
            return response()->json($response, 500);
        }
    }
}
