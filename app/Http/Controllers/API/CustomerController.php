<?php

namespace App\Http\Controllers\API;

use App\Models\Customer;
use App\Helpers\FileHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\DataTables\CustomersDataTable;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    protected $model;
    public function __construct(Customer $customer){

        // $this->middleware('can:customer.list')->only('index');
        // $this->middleware('can:customer.create')->only('store');
        // $this->middleware('can:customer.update')->only('update');
        // $this->middleware('can:customer.delete')->only('destroy');

        $this->model    = $customer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $payload = $request->all();
            if (Auth::user()->hasRole('superadmin')) {
                $data = Customer::orderBy('name','ASC');
            } else {
                $data = Customer::where('company_id', Auth::user()->company_id)->orderBy('name','ASC');
            }

            isset($payload['search']) ? 
                $data = $data->where('name', 'like', '%'.$payload['search'].'%')->paginate(10) : 
                $data = $data->paginate(10);

            $response = [
                'code' => 200,
                'message' => 'Successfully'
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage()
            ];
        }

        return response()->json($data, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $input['company_id'] = Auth::user()->hasRole('superadmin') ? $request['company_id'] : $request['company_id'] = Auth::user()->company_id;

            $data = Customer::create($input);
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $payload = $request->all();
            $data = Customer::find($id);

            $input['company_id'] = Auth::user()->hasRole('superadmin') ? $request['company_id'] : $request['company_id'] = Auth::user()->company_id;

            $datas = $data->update($payload);
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Customer::findOrFail($id);
            $data->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully deleted customer account '. $data->name,
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
