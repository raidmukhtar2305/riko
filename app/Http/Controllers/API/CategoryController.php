<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            if (Auth::user()->hasRole('superadmin')) {
                $data = Category::orderBy('created_at', 'desc')->orderBy('name')->get();
            } else {
                $data = Category::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->orderBy('name')->get();
            }

            $response["code"] = 200;
            $response["data"] = $data;
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $input['company_id'] = Auth::user()->hasRole('superadmin') ? $request['company_id'] : $request['company_id'] = Auth::user()->company_id;

            $data = Category::create($input);

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $payload = $request->all();
            $data = Category::find($id);
            $data->update($payload);

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Category::findOrFail($id);
            $data->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
