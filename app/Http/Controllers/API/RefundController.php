<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Refund;
use App\Models\RefundDetail;
use App\Models\StockLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use PHPUnit\TextUI\XmlConfiguration\Constant;

class RefundController extends Controller
{
    protected $model;
    protected $view;

    public function __construct(Refund $refund){

        $this->middleware('can:order.list')->only('index');
        $this->middleware('can:order.create')->only('store');
        $this->middleware('can:order.update')->only('update');
        $this->middleware('can:order.delete')->only('destroy');

        $this->model    = $refund;
        $this->view     = "orders.refunds";
        $this->path     = "admin";
        $this->route    = "admin.orders.refunds";
        $this->title    = "Order Refund Management";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function store(Request $request, $orderId)
    {
        try {
            $input = $request->all();

            $company = Auth::user()->company_id;

            $refund = Refund::create([
                'order_id' => $orderId,
                'datetime' => Carbon::parse($input['datetime'])->format('Y-m-d H:i:s'),
                'remarks' => $input['remarks']
            ]);

            $order = Order::whereId($orderId)->first('total');

            $refundTotal = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                $orderDetail = OrderDetail::where('order_id', $orderId)->where('product_id', $value)->first();

                $refundDetailData = [
                    'refund_id'      => $refund->id,
                    'product_id'    => $value,
                    'qty'           => $input['qty'][$key],
                ];
                
                $refundDetailData = RefundDetail::create($refundDetailData);
                $refundTotal += $product->price * $input['qty'][$key]; //total ditambah

                $refundStock = $product->stock + $input['qty'][$key]; //stock ditambah
                $stock = $orderDetail->qty - $input['qty'][$key]; //qty order detail dikurang

                $product->update(['stock'=>$refundStock]); //update product stock
                $orderDetail->update(['qty'=>$stock]); //update orderDetail qty

                if (!$product->is_service) {
                    $stock_log_data = [
                        'product_id' => $value,
                        'reference_id' => $refundDetailData->id,
                        'reference_slug' => config('constants.table_references.refund_detail'),
                        'qty' => $refundDetailData->qty,
                        'company_id' => $company
                    ];
                    StockLog::create($stock_log_data);
                }
            }

            $orderTotal = $order->total - $refundTotal;

            Refund::find($refund->id)->update(['total'=>$refundTotal]);
            Order::find($orderId)->update(['total'=>$orderTotal]);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $refund
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }

    }

}
