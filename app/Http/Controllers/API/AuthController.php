<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\AuthResource;
use App\Http\Resources\UserResource;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
                $data = auth()->user();
                $data = (Auth::user()->hasRole('cashier')) ? $data : abort(500, 'You are not Cashier');
                $data['token'] = $data->createToken('token')->accessToken;

                $response = [
                    'code' => 200,
                    'message' => 'successfully',
                    'data' => new AuthResource($data)
                ];
            } else {
                $response = [
                    'code' => 401,
                    'message' => 'Email dan password Anda salah!',
                    'data' => null
                ];
            }
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    // public function logout (Request $request)
    // {
    //     if ($request->user()) {
    //         $request->user()->tokens()->delete();
    //     }
    //     return response()->json([
    //         'message' => 'Logout Success',
    //     ], 200);
    // }

    public function logout()
    {
        Auth::user()->tokens()->delete();
        return response()->json([
            'message' => 'logout success'
        ]);
    }

    public function profile()
    {
        try {
            $data = Auth::user();
            
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function update(Request $request)
    {
        try {
            $data = Auth::user();
            $data->name = data_get($request, 'name');
            $data->username = data_get($request, 'username');
            $data->email = data_get($request, 'email');
            $data->phone = data_get($request, 'phone');
            $data->address = data_get($request, 'address');
            $data->save();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

}
