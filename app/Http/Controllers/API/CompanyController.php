<?php

namespace App\Http\Controllers\API;

use App\Models\Company;
use App\Helpers\FileHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\DataTables\CustomersDataTable;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    protected $model;
    public function __construct(Company $company){

        // $this->middleware('can:company.list')->only('index');
        // $this->middleware('can:company.create')->only('store');
        // $this->middleware('can:company.update')->only('update');
        // $this->middleware('can:company.delete')->only('destroy');

        $this->model = $company;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $data = Company::orderBy('created_at', 'desc')->get();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name'          => ['required'],
                'email'         => ['required'],
                'address'       => ['required'],
                'phone'         => ['required'],
            ]);

            $input = $request->all();
            $data = Company::create($input);
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $input  = $request->all();
            $data   = Company::find($id);

            $datas  = $data->update($input);
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Company::findOrFail($id);
            $data->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully deleted company '. $data->name,
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
