<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Invoice;
use App\Models\OrderDetail;
use App\Http\Requests\InvoiceRequest;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    protected $model;
    public function __construct(Invoice $invoice){

        $this->model = $invoice;
    }
    
    public function invoice($id)
    {
        try {
            $data = Order::with(['order_details', 'customer','company','invoices'])->find($id);
            $sum_invoice = Invoice::whereOrderId($id)->where('status',1)->get()->sum('nominal');
            $remaining = $data->total - (int)$sum_invoice;

            if ($data) {
                $response = [
                    'code' => 200,
                    'message' => 'Successfully',
                    // 'data' => [
                    //     'order' => $data,
                    //     'remaining' => $remaining,
                    // ]    
                    'data' => $data,
                    'remaining' => $remaining
                ];
            } else {
                $response = [
                    'code' => 404,
                    'message' => 'Order not found',
                    'data' => null
                ];
            }
        } catch (\Exception $ex) {
            $data = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate(
                [
                    'order_id' => 'required',
                    'date' => 'required',
                    'nominal' => 'numeric'
                ],
                // [
                //     'nominal' => 'nomor handphone tidak valid',
                //     'date' => 'description wajib diisi',
                //     'order_id' => 'harap masukan alamat',
                // ]
            );
            $payload = $request->all();
            $order = Order::findOrFail($request->order_id);

            $payload['reference'] = 'INV' . date('YmdHis');
            $payload['company_id'] = $order->company_id;
            $data = $this->model->create($payload);
            
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }


    public function show($id)
    {
        try {
            $data = $this->model->with('company', 'order.order_details.product', 'order.customer')->find($id);

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $data
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function updateStatus(Request $request, $id){
        DB::beginTransaction();
        try {
            $payload = $request->status;
            $invoice = $this->model->find($id);
            $data = $invoice->update(['status' => $payload]);

            DB::commit();

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }


    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate(
                [
                    // 'order_id' => 'required',
                    'date' => 'required',
                    'nominal' => 'numeric'
                ]
            );
            $payload = $request->all();
            $invoice = $this->model->find($id);
            $update = $invoice->update($payload);
            $data = $this->model->find($id);

            DB::commit();

            $response = [
                'code' => 200,
                'message' => 'successfully save data',
                'update' => $update,
                'data' => $data,
            ];

        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }


    public function destroy($id)
    {   
        try {
            $invoice = $this->model->find($id);
            $data = $invoice->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully deleted',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
            return response()->json($response, 500);
        }

        return response()->json($response, $response['code']);
    }

    public function printInvoice($id)
    {   
        try {
            $date = date("Y-m-d");
            $currentDate = Carbon::now();
            $exp_date = $currentDate->addDays(7);
            $valid_date = $exp_date->toDateString();
            $invoice = Invoice::findOrfail($id);
            $order = Order::with('order_details', 'company')->findOrFail($invoice->order_id);
    
            $pdf = Pdf::loadview('pages.admin.invoice.print', compact('invoice','order','valid_date'));
            return $pdf->download($date.' - '.$invoice->reference.'.pdf');    

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $invoice
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
