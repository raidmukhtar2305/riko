<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Quotation;
use App\Models\QuotationDetail;
use App\Models\StockLog;
use App\Models\OrderDetail;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;

use Illuminate\Support\Facades\Auth;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            if (Auth::user()->hasRole('superadmin')) {
                $data = Quotation::orderBy('created_at', 'desc')->paginate(10);
            } else {
                $data = Quotation::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->paginate(10);
            }
            $response["code"] = 200;
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($data, $response['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'customer_id' => ['required'],
                'date' => ['required'],
            ]);

            $input = $request->all();

            $user = Auth::user()->hasRole('superadmin');
            $input['company_id'] = Auth::user()->hasRole('superadmin') ? $request['company_id'] : $request['company_id'] = Auth::user()->company_id;

            $quotation_data = [
                'quotation_code'    => quotation_code(),
                'order_code'        => order_code(),
                'date'              => $input['date'],
                'description'       => $input['description'],
                'customer_id'       => $input['customer_id'],
                'status'            => 0,
                'company_id'        => $company
            ];

            $quotation = Quotation::create($quotation_data);

            $total = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                $quotation_data = [
                    'quotation_id'          => $quotation->id,
                    'product_id'            => $value,
                    'qty'                   => $input['qty'][$key],
                    'price'                 => $product->price,
                    'laba'                  => $product->laba,
                    'product_name'          => $product->name,
                    'product_description'   => $product->description,
                ];

                $quotation_data = QuotationDetail::create($quotation_data);
                $total += $product->price * $input['qty'][$key];
            }

            Quotation::find($quotation->id)->update(['total'=>$total]);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $quotation
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $quotation = Quotation::find($id);
            $data = $quotation->delete();
            QuotationDetail::whereQuotationId($id)->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully deleted order '. $quotation->quotation_code,
                'data' => $quotation
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function convertOrder(Request $request, $id)
    {
        try {
            $input = $request->all();
            // dd($input);
            $quotation = Quotation::find($id);
            
            $quotation_data = [
                'order_code'        => order_code(),
                'date'              => $quotation->date,
                'description'       => $quotation->description,
                'customer_id'       => $quotation->customer_id,
                'status'            => 0,
                'company_id'        => $quotation->company_id,
                'total'             => $quotation->total,
            ];

            $order = Order::create($quotation_data);

            $quotation_detail = QuotationDetail::whereQuotationId($id)->get();
            foreach ($quotation_detail as $key => $value) {
                $product = Product::find($value->product_id);
                $order_detail_data = [
                    'order_id'              => $order->id,
                    'product_id'            => $value->product_id,
                    'qty'                   => $value->qty,
                    'price'                 => $value->price,
                    'laba'                  => $value->laba,
                    'product_name'          => $value->product_name,
                    'product_description'   => $value->product_description,
                ];
                $order_detail = OrderDetail::create($order_detail_data);
                
                $stock = $product->stock - $value->qty;
                $product->update(['stock' => $stock]);            

                if (!$product->is_service) {
                    $stock_log_data = [
                        'product_id'        => $product->id,
                        'reference_id'      => $order_detail->id,
                        'reference_slug'    => config('constants.table_references.order_detail'),
                        'qty'               => "- " .$order_detail->qty,
                        'company_id'        => $order->company_id
                    ];
                    StockLog::create($stock_log_data);
                }
            }

            if ($request->is_paid) {
                $payloadInv['reference'] = 'INV' . date('YmdHis');
                $payloadInv['company_id'] = $input['company_id'];
                $payloadInv['date'] = $input['date'];
                $payloadInv['note'] = $input['invoice_note'];
                $payloadInv['description'] = $input['invoice_description'];
                $payloadInv['nominal'] = $total;
                $payloadInv['status'] = '1';
                $payloadInv['order_id'] = $order->id;
                $invoice = Invoice::create($payloadInv);
            }

            $input['order_id'] = $order->id;
            $input['status']   = 1; 
            $data = $quotation->update($input);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $quotation
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }


    public function downloadInvoice($id)
    {
        try {
            $time = time();
            $date = date("Y-m-d");
            $quotation = Quotation::find($id);
            $quotation_detail = QuotationDetail::whereQuotationId($id)->get();
            $currentDate = Carbon::now();
            $exp_date = $currentDate->addDays(7);
            $valid_date = $exp_date->toDateString();
    
            $pdf = Pdf::loadview('pages.admin.quotation.print', compact('quotation', 'quotation_detail', 'valid_date'));
            return $pdf->download($date.' - '.$quotation->quotation_code.'.pdf');


            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => $quotation
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
