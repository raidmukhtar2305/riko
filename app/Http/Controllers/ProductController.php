<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Company;
use App\Models\Outlet;
use App\Models\User;
use App\Models\LogMemberView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use View;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->view     = "front-end.store.";

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index($name)
    {
        $company = Company::where('name', $name)->firstOrFail();
        $product = Product::where('company_id', $company->id)->get();

        return view('pages.'.$this->view.'index', compact('product', 'company'));
    }

    // public function show($name)
    // {
    //     $company = Company::where('name', $name)->firstOrFail();
    //     $product = Product::first();

    //     return view('pages.'.$this->view.'show', compact('product', 'company'));
    // }

}
