<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Member;
use Illuminate\Support\Facades\Session;

use View;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use App\Helpers\FileHelper;


use App\Models\Product;
use App\Models\MemberAddress;
use App\Models\Quotation;
use App\Models\Outlet;
use App\Models\User;
use App\Models\Company;
use App\Models\QuotationDetail;

use App\Http\Requests\CheckoutRequest;

class OrderController extends Controller
{
    protected $categories;

    public function __construct()
    {
        $this->view     = "front-end.order.";
        $this->categories = Category::all();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cart()
    {
        return view('pages.' . $this->view . 'cart');
    }

    public function checkout()
    {
        $company    = Company::first();
        $outlets    = Outlet::get();
        $cashiers   = User::whereType('cashier')->get();
        return view('pages.'. $this->view.'checkout', compact('company', 'outlets', 'cashiers'));
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $cart = session()->get('cart');
        // dd($cart);

        $cart[$id] = [
            'product_id'        => $product->id,
            "company_id "       => $product->company_id,
            "category_id"       => $product->category_id,
            "name"              => $product->name,
            "description"       => $product->description,
            "price"             => $product->price,
            "stock"             => $product->stock,
            "total"             => $product->price,
            "qty"               => 1,
        ];

        session(['cart' => $cart]);
        $this->cartTotalOrder();

        Alert::success('Berhasil', 'Barang berhasil ditambahkan ke keranjang');
        return redirect()->route('order.cart');
    }

    public function cartTotalOrder(){

        $carts = session('cart');
        if (!$carts) {
            return true;
        } else {
            $qty    = 0;
            $total  = 0;
            foreach($carts as $key=>$data){
                $qty += $data['qty'];
                $total += $data['total'];
            }

            session(['cart_total' => $qty]);
            session(['cart_grand_total' => $total]);
        }

        return true;
    }

    public function deleteCart($id) {
        if ($id == 'all') {
            session()->forget('cart');
        } else {
            $cart = session('cart');
            unset($cart[$id]);

            session(['cart' => $cart]);
        }

        $this->cartTotalOrder();

        return redirect()->route('order.cart');
    }

    public function checkoutStore(Request $request)
    {
        try {
            $carts = session('cart');
            $company = Company::first();

            $input = $request->all();

            $quotation_data = [
                'name_buyer'        => $input['name_buyer'],
                'no_rek_buyer'      => $input['no_rek_buyer'],
                'quotation_code'    => quotation_code(),
                'date'              => date('Y-m-d'),
                'status'            => 0,
                'company_id'        => $company->id,
                'outlet_id'         => $input['outlet_id'],
                'user_id'           => $input['user_id'],
            ];
            // dd($quotation_data);
            $quotation = Quotation::create($quotation_data);

            if ($quotation) {
                $total = 0;
                $stock = 0;
                foreach ($carts as $key => $cart) {
                    $product = Product::find($key);
                    // dd($product);
                    $quotation_detail_data = [
                        'quotation_id'          => $quotation->id,
                        'product_id'            => $key,
                        'qty'                   => $cart['qty'],
                        'price'                 => $product->price,
                        'laba'                  => $product->laba,
                        'product_name'          => $product->name,
                        'product_description'   => $product->description,
                    ];
                    $quotation_detail_data = QuotationDetail::create($quotation_detail_data);

                    $total += ($product->price * $cart['qty']);
                    $stock = ($product->stock - $cart['qty']);
                    $product->update(['stock'=> $stock]);
                }

                $quotation = Quotation::find($quotation->id);
                $quotation->update(['total'=> $total]);

                session()->forget('cart');
                session()->forget('cart_total');
                session()->forget('cart_grand_total');

                return redirect()->route('order.invoice', $quotation->id);
            }


            return back()->withInput();
          } catch (\Exception $e) {
            return  back()->withInput();
          }
    }

    public function invoice($id)
    {
        $quotation = Quotation::with('quotation_details')->find($id);

        return view('pages.'.$this->view. 'invoice', compact('quotation'));
    }

}
