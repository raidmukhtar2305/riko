<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Company;
use App\Models\Outlet;
use App\Models\User;
use App\Models\LogMemberView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use View;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->view     = "front-end.home.";

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        return view('pages.'.$this->view.'index');
    }

}
