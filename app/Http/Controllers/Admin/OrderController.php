<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OrdersDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Outlet;
use App\Models\StockLog;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Company;
use App\Services\OrderService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade\Pdf;
use Barryvdh\DomPDF\PDF as DomPDFPDF;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Order $order)
    {
        $this->middleware('can:order.list')->only('index');
        $this->middleware('can:order.create')->only('store');
        $this->middleware('can:order.update')->only('update');
        $this->middleware('can:order.delete')->only('destroy');

        $this->model    = $order;
        $this->view     = "orders";
        $this->path     = "admin";
        $this->route    = "admin.orders";
        $this->title    = "Transaction";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(OrdersDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Order', route($this->route . '.index')]
        ]);

        return $dataTable->render("pages.".$this->path.".".$this->view.'.index');

        // if ($request->ajax()) {
        //     $query = Order::query()
        //         ->with([
        //             'customer' => function ($query) {
        //                 $query->select('id', 'name');
        //             },
        //             'company' => function ($query) {
        //                 $query->select('id', 'name');
        //             },
        //         ])
        //         ->select([
        //             'id',
        //             'company_id',
        //             'customer_id',
        //             'order_code',
        //             'date',
        //             'total',
        //             'status',
        //         ]);

        //     if (!Auth::user()->hasRole('superadmin')) {
        //         $query = $query->where('company_id', Auth::user()->company_id);
        //     }

        //     return DataTables::of($query)
        //         ->editColumn('company.name', function ($data) {
        //             return data_get($data, 'company.name', '');
        //         })
        //         ->editColumn('customer.name', function ($data) {
        //             return data_get($data, 'customer.name', '');
        //         })
        //         ->editColumn('total', function ($data) {
        //             return  'Rp. ' . number_format($data->total);
        //         })
        //         ->addColumn('paid', function ($data) {
        //             $paid = $data
        //                 ->invoices()
        //                 ->where('status', 1)
        //                 ->sum('nominal');

        //             return  'Rp. ' . number_format($paid);
        //         })
        //         ->editColumn('status', function ($data) {
        //             $paid = $data
        //                 ->invoices()
        //                 ->where('status', 1)
        //                 ->sum('nominal');

        //             if ($paid < $data->total) return 'belum lunas';
        //             if ($paid == $data->total) return 'sudah lunas';
        //             if ($paid > $data->total) return 'lebih';
        //         })
        //         ->addColumn('action', function ($data) {
        //             $id = $data->id;

        //             $btn = '
        //                 <div class="dropdown">
        //                     <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown"
        //                         aria-expanded="false">
        //                         Action
        //                     </button>
        //                     <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">
        //                         <li><a class="dropdown-item edit-btn" href="' . route("admin.orders.edit", $id) . '">Edit</a></li>
        //                         <li><a class="dropdown-item" href="' . route("admin.orders.show", $id) . '">Show</a></li>
        //                         <li><a class="dropdown-item" href="' . route("admin.invoice.index", ['orderId' => $id]) . '">Invoice</a></li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.orders.print-preview", $id) . '" target="_blank">Preview Print Order</a>
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.orders.print", $id) . '">Download Order</a>
        //                         </li>
        //                         <li>
        //                             <hr class="dropdown-divider">
        //                         </li>
        //                         <li><a class="dropdown-item delete-btn" data-id="' . $id . '" href="#">Delete</a></li>
        //                     </ul>
        //                 </div>
        //                 ';

        //             return $btn;
        //         })
        //         ->rawColumns(['action', 'paid'])
        //         ->make(true);
        // }

        // return view('pages.admin.orders.index');
    }

    public function create(Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ["Create Order", route($this->route . '.create')]
        ]);

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            $products   = Product::get();
            $customers  = Customer::get();
            $companies  = Company::get();
            $outlets    = Outlet::get();
        } else {
            $products   = Product::where('company_id', Auth::user()->company_id)->get();
            $customers  = Customer::where('company_id', Auth::user()->company_id)->get();
            $companies  = Company::get();
            $outlets    = Outlet::get();
        }

        $product_list = [];
        foreach ($products as $product) {
            $product_list[$product->id] = $product->price;
        }

        $product_list = json_encode($product_list);
        $order = null;

        return view('pages.' . $this->path . "." . $this->view . '.create', compact('companies', 'outlets', 'products', 'product_list', 'customers', 'order'));
    }


    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $user = Auth::user()->hasRole('superadmin');
            // $outlet = Outlet::find($input['outlet_id']);


            // foreach ($input['product_id'] as $value) {
            //     $stock = StockLog::whereWarehouseId($outlet['warehouse_id'])->whereProductId($value)->get();
            //     foreach ($input['product_qty'] as $qty) {
            //         if ($stock->sum('qty') < $qty) {
            //             $product = Product::find($value);
            //             $response = [
            //                 'message' => 'Maaf stock ' . $product->name . ' habis!',
            //             ];
            //             return response()->json($response, 500);
            //         }
            //     }
            // }

            if ($user) {
                $company    = $input['company_id'];
                $outlet     = $input['outlet_id'];
            } else {
                $company    = Auth::user()->company_id;
                $outlet     = $input['outlet_id'];
            }

            $input['company_id'] = $company;
            $input['outlet_id'] = $outlet;

            $order = OrderService::create($input);

            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $order
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }

    public function show($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Order Detail', route($this->route . '.show', $id)]
        ]);

        $order = Order::with('order_details', 'refunds')->find($id);

        return view('pages.admin.orders.detail', compact('order'));
    }

    public function edit($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Edit Order', route($this->route . '.edit', $id)]
        ]);

        $order = Order::with('order_details')->find($id);

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            $products   = Product::get();
            $customers  = Customer::get();
            $companies  = Company::get();
            $outlets    = Outlet::get();
        } else {
            $products   = Product::where('company_id', Auth::user()->company_id)->get();
            $customers  = Customer::where('company_id', Auth::user()->company_id)->get();
            $companies  = Company::get();
            $outlets    = Outlet::get();
        }

        return view('pages.admin.orders.edit', compact('products', 'customers', 'order', 'companies', 'outlets'));
    }


    public function update(OrderRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $order = $this->model->find($id);

            if (data_get($request, 'company_id')) $order->company_id = $request->company_id;
            if (data_get($request, 'customer_id')) $order->customer_id = $request->customer_id;
            if (data_get($request, 'outlet_id')) $order->outlet_id = $request->outlet_id;
            $order->date = $request->date;
            $order->description = $request->description;

            foreach (explode(',', $request->order_detail_deleted) as $order_detail_deleted) {
                $order->order_details()->whereId($order_detail_deleted)->delete();
                StockLog::whereReferenceId($order_detail_deleted)->whereReferenceSlug(config('constants.table_references.order_detail'))->delete();
            }

            $total = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                $product_qty = $input['product_qty'][$key];

                $order_detail = $order
                    ->order_details()
                    ->updateOrCreate(
                        [
                            'product_id' => $value,
                        ],
                        [
                            'qty'                   => $product_qty,
                            'price'                 => $product->price,
                            'profit_unit'           => floatval($product->profit),
                            'profit'                => $product->profit * $product_qty,
                            'product_name'          => $product->name,
                            'product_description'   => $product->description,
                        ]
                    );

                if (!$product->is_service) {
                    StockLog::updateOrCreate(
                        [
                            'reference_id' => $order_detail->id,
                            'reference_slug' => config('constants.table_references.order_detail'),
                        ],
                        [
                            'company_id' => $order->company_id,
                            'warehouse_id' => data_get($order, 'outlet.warehouse_id'),
                            'product_id' => $value,
                            'qty' => $order_detail->qty * -1,
                        ]
                    );
                }


                $total += $order_detail->price * $product_qty;
            }

            $order->total = $total;
            $order->save();

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $order
            ];

            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }


    public function destroy($id)
    {
        try {
            $order = $this->model->find($id);
            $data = $order->delete();
            OrderDetail::whereOrderId($id)->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function getAddress(Request $request)
    {
        try {
            $data = MemberAddress::whereMemberId($request->member_id)->get();

            return response()->json($data);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function updateStatus($id)
    {
        try {
            $order = $this->model->find($id);
            $input = ['status' => $order->status + 1];
            $data = $order->update($input);

            $response = [
                'success' => true,
                'message' => 'Success update status',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function showResi($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }


    public function addResi(Request $request, $id)
    {
        try {
            $input = $request->all();
            $order = $this->model->find($id);
            $input['status'] = 3;
            $data = $order->update($input);

            $response = [
                'success' => true,
                'message' => 'Success update status',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function printOrder($id)
    {
        $order = Order::with('order_details', 'order_details.product', 'company', 'customer',)->find($id);
        $invoices = Invoice::whereOrderId($order->id)->whereStatus(1)->get()->sum('nominal');
        $payment = (int) $order->total - (int)$invoices;

        // Set options to enable embedded PHP
        $options = new Options();
        $options->set('isPhpEnabled', 'true');

        // Instantiate dompdf class
        $dompdf = PDF::loadview('pages.admin.orders.print', compact('order','payment'));

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        if ($invoices >= $order->total) {
            // Instantiate canvas instance
            $canvas = $dompdf->getCanvas();

            // Get height and width of page
            $w = $canvas->get_width();
            $h = $canvas->get_height();

            // Specify watermark image
            $imageURL = 'assets2/images/lunas.png';
            $imgWidth = 300;
            $imgHeight = 200;

            // Set image opacity
            $canvas->set_opacity(.1);

            // Specify horizontal and vertical position
            $x = (($w-$imgWidth)/2);
            $y = (($h-$imgHeight)/2);

            // Add an image to the pdf
            $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);
        }

        // Output the generated PDF (1 = download and 0 = preview)
        return $dompdf->stream('document.pdf', array("Attachment" => 0));

    }

    public function generatePrintOrder($id)
    {

        $time = time();
        $date = date("Y-m-d");
        $order = Order::with('order_details', 'order_details.product', 'company', 'customer')->find($id);
        $invoices = Invoice::whereOrderId($order->id)->whereStatus(1)->get()->sum('nominal');
        $payment = (int) $order->total - (int)$invoices;

        // Set the Content-Type header to display the PDF inline
        return view('pages.admin.orders.print', compact('order','payment'));
    }



    public function PrintStruck($id)
    {
        $order = Order::with('order_details', 'order_details.product', 'company', 'customer',)->find($id);
        $order_detail = OrderDetail::whereOrderId($order->id)->get();

        return view('pages.admin.orders.struk', compact('order', 'order_detail'));
    }
}
