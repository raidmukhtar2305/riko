<?php

namespace App\Http\Controllers\Admin;

use View;
use App\Models\Package;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\DataTables\PackagesDataTable;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    protected $model;
    protected $view;

    public function __construct(Package $package){

        $this->middleware('can:package.list')->only('index');
        $this->middleware('can:package.create')->only('store');
        $this->middleware('can:package.update')->only('update');
        $this->middleware('can:package.delete')->only('destroy');

        $this->model    = $package;
        $this->view     = "packages";
        $this->path     = "admin";
        $this->route    = "admin.package";
        $this->title    = "Data Master";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(PackagesDatatable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'.index')],
            ['Package Management', route($this->route.'.index')]
        ]);

        return $dataTable->render("pages.".$this->path.".".$this->view.'.index');
    }

    public function store(PackageRequest $request)
    {
        try {
            $payload    = $request->all();
            // $image      = $request->file();

            // if($request->file('image'))
            //     $payload['image'] = FileHelper::saveImage($request->file('image'),500, public_path("categories"));

            $data = $this->model->create($payload);
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }

    }


    public function show($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }


    public function update(Request $request, $id)
    {
        try {
            $payload = $request->all();
            $package = $this->model->find($id);

            // if($request->file('image'))
            // $payload['image'] = FileHelper::saveImage($request->file('image'),500, public_path("categories"));

            $data = $package->update($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }


    public function destroy($id)
    {
        try {
            $package = $this->model->find($id);
            $data = $package->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }
}
