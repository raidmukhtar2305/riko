<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OutletDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Outlet;
use App\Http\Requests\OutletRequest;
use App\Models\Company;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class OutletController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;


    public function __construct(Outlet $outlet)
    {

        $this->middleware('can:outlet.list')->only('index');
        $this->middleware('can:outlet.create')->only('store');
        $this->middleware('can:outlet.update')->only('update');
        $this->middleware('can:outlet.delete')->only('destroy');

        $this->model    = $outlet;
        $this->view     = "outlet";
        $this->path     = "admin";
        $this->route    = "admin.outlet";
        $this->title    = "Data Master";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OutletDataTable $dataTable, Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Outlet Management', route($this->route . '.index')]
        ]);

        $warehouses = (Auth::user()->hasRole('superadmin')) ? Warehouse::get() : Warehouse::whereCompanyId(Auth::user()->company_id)->get();

        return $dataTable->render("pages.".$this->path.".".$this->view.'.index', compact('warehouses'));
    }


    public function create()
    {
        //
    }


    public function store(OutletRequest $request)
    {
        try {
            $payload = (Auth::user()->hasRole('superadmin')) ? $request->all() : $request->merge(['company_id' => Auth::user()->company_id])->all();

            $data = $this->model->create($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response);
        }
    }


    public function show($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response);
        }
    }


    public function edit($id)
    {
        //
    }


    public function update(OutletRequest $request, $id)
    {
        try {
            $payload = $request->all();
            $outlet = $this->model->find($id);

            $data = $outlet->update($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }


    public function destroy($id)
    {
        try {
            $outlet = $this->model->find($id);
            $data = $outlet->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function detailOutlet($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Outlet Detail', route($this->route . '.show', $id)]
        ]);

        $outlet = Outlet::with(['company', 'warehouse'])->find($id);
        return view('pages.admin.outlet.show', compact('outlet'));
    }

    public function getList(Request $request)
    {
        try {
            $data = $this->model
                ->select([
                    'outlets.id',
                    'outlets.name',
                    'companies.name as company_name',
                ])
                ->join('companies', 'companies.id', 'outlets.company_id');

            if ($search = data_get($request, 'search')) {
                $data = $data->where(function ($query) use ($search) {
                    return $query
                        ->orWhere('outlets.name', 'like', '%' . $search . '%')
                        ->orWhere('companies.name', 'like', '%' . $search . '%');
                });
            }

            if (!Auth::user()->hasRole('superadmin')) $data = $data->whereCompanyId(Auth::user()->company_id);

            $data = $data
                ->limit(20)
                ->orderBy('name')
                ->get()
                ->map(function ($data) {
                    $text = '';

                    if (Auth::user()->hasRole('superadmin'))
                        $text = data_get($data, 'company_name') . ' - ';

                    $data->text = $text . $data->name;

                    return $data;
                });

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }
}
