<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReportDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Order;
use Carbon\Carbon;
use View;

class ReportController extends Controller
{
    protected $model;
    protected $view;

    public function __construct(Order $order)
    {

        // $this->middleware('can:reports.list')->only('index');
        // $this->middleware('can:reports.create')->only('store');
        // $this->middleware('can:reports.update')->only('update');
        // $this->middleware('can:reports.delete')->only('destroy');

        $this->model    = $order;
        $this->view     = "report";
        $this->path     = "admin";
        $this->route    = "admin.report";
        $this->title    = "Report Management";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')]
        ]);
        return view("pages." . $this->path . "." . $this->view . '.index');

    }
    public function records(Request $request)
    {
        if ($request->ajax()) {
            // dd($request);

            if ($request->input('start_date') && $request->input('end_date')) {

                $start_date = Carbon::parse($request->input('start_date'));
                $end_date = Carbon::parse($request->input('end_date'));
                // dd($start_date);

                if ($end_date->greaterThan($start_date)) {
                    $order = Order::with('company', 'customer')->whereBetween('date', [$start_date, $end_date])->get();
                } else {
                    $order = Order::with('company','customer')->latest()->get();
                }
            } else {
                $order = Order::with('company', 'customer')->latest()->get();
            }

            return response()->json([
                'orders' => $order
            ]);
        } else {
            abort(403);
        }
    }
}
