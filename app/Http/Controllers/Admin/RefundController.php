<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Refund;
use App\Models\RefundDetail;
use App\Models\StockLog;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use PHPUnit\TextUI\XmlConfiguration\Constant;

class RefundController extends Controller
{
    protected $model;
    protected $view;

    public function __construct(Refund $refund){

        $this->middleware('can:order.list')->only('index');
        $this->middleware('can:order.create')->only('store');
        $this->middleware('can:order.update')->only('update');
        $this->middleware('can:order.delete')->only('destroy');

        $this->model    = $refund;
        $this->view     = "orders.refunds";
        $this->path     = "admin";
        $this->route    = "admin.orders.refunds";
        $this->title    = "Order Refund Management";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function create($orderId) {
        $order = Order::whereId($orderId)->first();

        View::share('breadcrumbs', [
            ['#'.$order->order_code, route('admin.orders.show', $orderId)],
            ["Create Order Refund", route($this->route.'.create', $orderId)]
        ]);

        $user = Auth::user()->hasRole('superadmin');
        $orderDetails = OrderDetail::where('order_id', $orderId)->with('product')->get();

        return view('pages.' . $this->path . "." . $this->view . '.create', compact('orderDetails', 'order'));
    }

    public function store(Request $request, $orderId)
    {
        if (empty($request->product_id)){
            return response()->json([
                'success' => false,
                'message' => 'No product selected',
                'data' => []
            ], 400);
        }

        try {
            $request->validate([
                'datetime' => ['required'],
            ]);
            $input = $request->all();
         

            $company = Auth::user()->company_id;

            $refund = Refund::create([
                'order_id' => $orderId,
                'datetime' => Carbon::parse($input['datetime'])->format('Y-m-d H:i:s'),
                'remarks' => $input['remarks']
            ]);

            $order = Order::whereId($orderId)->first('total');

            $refundTotal = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                $orderDetail = OrderDetail::where('order_id', $orderId)->where('product_id', $value)->first();

                $refundDetailData = [
                    'refund_id'      => $refund->id,
                    'product_id'    => $value,
                    'qty'           => $input['qty'][$key],
                ];
                
                $refundDetailData = RefundDetail::create($refundDetailData);
                $refundTotal += $product->price * $input['qty'][$key]; //total ditambah

                $refundStock = $product->stock + $input['qty'][$key]; //stock ditambah
                $stock = $orderDetail->qty - $input['qty'][$key]; //qty order detail dikurang

                $product->update(['stock'=>$refundStock]); //update product stock
                $orderDetail->update(['qty'=>$stock]); //update orderDetail qty

                if (!$product->is_service) {
                    $stock_log_data = [
                        'product_id' => $value,
                        'reference_id' => $refundDetailData->id,
                        'reference_slug' => config('constants.table_references.refund_detail'),
                        'qty' => $refundDetailData->qty,
                        'company_id' => $company
                    ];
                    StockLog::create($stock_log_data);
                }
            }

            $orderTotal = $order->total - $refundTotal;

            Refund::find($refund->id)->update(['total'=>$refundTotal]);
            Order::find($orderId)->update(['total'=>$orderTotal]);

            $response = [
                'success' => true,
                'message' => 'Success create data',
                'data' => $refund
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }

    }

    //RESTORES GRAND TOTAL to related orders table
    //RESTORES EACH QTY to related order_details table
    public function destroy($orderId, $refundId)
    {
        try {
            $refund = Refund::with('refund_details', 'order')->find($refundId);
            
            $total = 0;
            foreach ($refund->refund_details as $refundDetail) {
                $product = Product::find($refundDetail->product_id);
                $orderDetail = OrderDetail::where('order_id', $orderId)->where('product_id', $refundDetail->product_id)->first();

                $total += $product->price * $refundDetail->qty; //total ditambah

                $refundStock = $product->stock - $refundDetail->qty; //stock dikurang
                $orderDetailQty = $orderDetail->qty + $refundDetail->qty; //qty order detail ditambah

                $product->update(['stock'=>$refundStock]); //update product stock
                $orderDetail->update(['qty'=>$orderDetailQty]); //update orderDetail qty

                if (!$product->is_service) {
                    $stockLogData = [
                        'product_id' => $product->id,
                        'reference_id' => $refundDetail->id,
                        'reference_slug' => config('constants.table_references.order_detail'),
                        'qty' => $refundDetail->qty,
                        'company_id' => $refund->order->company_id ?? null
                    ];
                    StockLog::create($stockLogData);
                }
            }

            $orderTotal = $refund->order->total + $total;

            Order::find($orderId)->update(['total'=>$orderTotal]);
            $refund->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete refund. Your items has been restored.',
                'data' => []
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }

    }

    public function getDetails(string $refundId) {
        try {
            $refundDetails = RefundDetail::where('refund_id', $refundId)->with('product')->get();
            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $refundDetails
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function print(string $refundId)
    {
        $time = time();
        $date = date("Y-m-d");
        $refund = Refund::with('refund_details.product', 'order.company', 'order.customer')->find($refundId);

        $pdf = Pdf::loadview('pages.admin.orders.refunds.print', compact('refund'))->setPaper('a4','potrait');
        return $pdf->download($date.' - '.$refund->order->order_code.' Refunds.pdf');
    }

}
