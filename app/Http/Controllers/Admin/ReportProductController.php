<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReportDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use View;
use DataTables;


class ReportProductController extends Controller
{
    protected $view;

    public function __construct()
    {

        $this->middleware('can:reports.products.list')->only('index');
        // $this->middleware('can:reports.create')->only('store');
        // $this->middleware('can:reports.update')->only('update');
        // $this->middleware('can:reports.delete')->only('destroy');

        $this->customers     = Customer::get();
        $this->users     = User::get();
        $this->view     = "pages.admin.products.report";
        $this->path     = "admin";
        $this->route    = "admin.product";
        $this->title    = "Report";

        View::share('customers', $this->customers);
        View::share('users', $this->users);
        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'.report')],
            ['Product Report', route($this->route.'.report')]
        ]);
        return view($this->view.'.index');

    }
    public function records(Request $request,OrderDetail $model)
    {
        if ($request->ajax()) {
            $payload = json_decode($request->data);
            $data = OrderDetail::with('product')->groupBy('product_id')->select(DB::raw('SUM(qty) as saled'),'product_id',);
            
            if (isset($payload->start_date) && isset($payload->end_date))
                $data = $data->whereHas('order', function($q_order) use ($payload){
                        $q_order->whereBetween('date', [$payload->start_date, $payload->end_date]);
                    });

            if ( isset($payload->customer_id) && $payload->customer_id != 'ALL')
                $data = $data->whereHas('order', function($q_order) use ($payload){
                            $q_order->whereCustomerId($payload->customer_id);
                        });

            if ( isset($payload->user_id) && $payload->user_id != "ALL")
                $data = $data->whereHas('order', function($q_order) use ($payload){
                            $q_order->whereCreatedBy($payload->user_id);
                        });

            $data = $data->orderBy('saled', 'DESC')->get();

            // return;
            // $data = OrderDetail::with('product')->groupBy('product_id')->select(DB::raw('SUM(qty) as saled'),'product_id',)->get();

            return Datatables::of($data)->addIndexColumn()
                ->editColumn('product', function ($row) {  
                    $productname = !empty($row->product->name) ? $row->product->name : '-';
                    return $productname;
                })
                ->make(true);
        }
    }
}
