<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\DataTables\InvoiceDataTable;
use App\Http\Requests\InvoiceRequest;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DataTables;

class InvoiceController extends Controller
{

    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Invoice $invoice)
    {
        $this->middleware('can:invoice.list')->only('index');
        $this->middleware('can:invoice.create')->only('store');
        $this->middleware('can:invoice.update')->only('update');
        $this->middleware('can:invoice.delete')->only('destroy');

        $this->model    = $invoice;
        $this->view     = "invoice";
        $this->path     = "admin";
        $this->route    = "admin.invoice";
        $this->title    = "Invoice Management";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(InvoiceDataTable $dataTable, Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')]
        ]);
        $orderId = $request->orderId;
        $order = Order::where('id', $orderId)->first();
        $sum_invoice = Invoice::whereOrderId($orderId)->get()->sum('nominal');
        $remaining = $order->total - (int)$sum_invoice;
        return $dataTable->with(['orderId'=> $orderId])->with('order', $orderId)->render("pages." . $this->path . "." . $this->view . '.index', compact('order', 'remaining'));


        // if ($request->ajax()) {

        //     if(Auth::user()->hasRole('superadmin')){
        //         $data = Invoice::with(['order','company'])->get();
        //     } else {
        //         $data = Invoice::with(['order','company'])->where('company_id', Auth::user()->company_id)->get();
        //     }

        //     if ($orderId = $request->orderId) {
        //         $data = Invoice::where('order_id', $orderId)->get();
        //     }

        //     return Datatables::of($data)
        //         ->addIndexColumn()
        //         ->editColumn('company_name', function ($row) {
        //             $company_name = empty($row->company->name) ? '' : $row->company->name;
        //             return $company_name;
        //         })
        //         ->editColumn('order_code', function ($row) {
        //             $order_code = empty($row->order->order_code) ? '' : $row->order->order_code;
        //             return $order_code;
        //         })
        //         ->editColumn('nominal', function($query) {
        //             return  'Rp. '. number_format($query->nominal);
        //         })
        //         ->filter(function ($instance) use ($request) {
        //             if (!empty($request->get('search'))) {
        //                 $instance->collection = $instance->collection->filter(function ($row) use ($request) {
        //                     if (Str::contains(Str::lower($row['company_name']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     } else if (Str::contains(Str::lower($row['order_code']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     } else if (Str::contains(Str::lower($row['nominal']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     } else if (Str::contains(Str::lower($row['reference']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     } else if (Str::contains(Str::lower($row['date']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     } else if (Str::contains(Str::lower($row['description']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     } else if (Str::contains(Str::lower($row['note']), Str::lower($request->get('search')))) {
        //                         return true;
        //                     }

        //                     return false;
        //                 });
        //             }
        //         })

        //         ->addColumn('action', function ($row) {
        //             $status = 0;
        //             $id = $row->id;

        //             $btn = '
        //                 <div class="dropdown">
        //                     <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown" aria-expanded="false">
        //                         Action
        //                     </button>
        //                     <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">';

        //             if ($row->status == 0) {
        //                 $btn .= '
        //                 <li><a class="dropdown-item status-btn" href="#" data-status="1" data-id="' . $id . '" >Lunas</a></li>';
        //             } else {
        //                 $btn .= '
        //                 <li><a class="dropdown-item status-btn" href="#" data-status="0" data-id="' . $id . '" >Belum Lunas</a></li>';
        //             }

        //             $btn .= '
        //                     <li>
        //                     <hr class="dropdown-divider">
        //                     </li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.invoice.show", $id) . '">Show</a>
        //                         </li>
        //                         <li><a class="dropdown-item edit-btn" href="#" data-id="' . $id . '">Edit</a></li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.invoice.print-preview", $id) . '" target="_blank">Preview Print Invoice</a>
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.invoice.print", $id) . '"><i class="fa fa-download" aria-hidden="true"></i> Download Invoice</a>
        //                         </li>
        //                         <li>
        //                             <hr class="dropdown-divider">
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item delete-btn" data-id="' . $id . '" href="#">Delete</a>
        //                         </li>
        //                     </ul>
        //                 </div>';

        //             return $btn;
        //         })
        //         ->make(true);
        // }

        // return view('pages.admin.invoice.index', compact('order'));
    }

    public function store(InvoiceRequest $request)
    {
        DB::beginTransaction();
        try {
            $payload = $request->all();
            $order = Order::findOrFail($request->order_id);
            $sum_invoice = Invoice::whereOrderId($request->order_id)->get()->sum('nominal');
            $arrear = $order->total - $sum_invoice;

            if ($sum_invoice + $request->nominal > $order->total && $sum_invoice < $order->total) {
                $response = [
                    'message' => 'Sisa dari invoice Rp.'.number_format($arrear),
                ];
                return response()->json($response, 500);
            } elseif ($sum_invoice == $order->total) {
                $response = [
                    'message' => 'Harap konfirmasi lunas!',
                ];
                return response()->json($response, 500);
            } else {
                $payload;
            }

            $payload['reference'] = 'INV' . date('YmdHis');
            $payload['company_id'] = $order->company_id;
            $data = $this->model->create($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function show($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Order Detail', route($this->route . '.show', $id)]
        ]);

        $invoice = $this->model->with('company', 'order')->find($id);

        return view("pages." . $this->path . "." . $this->view . '.show', compact('invoice'));
    }

    public function update(InvoiceRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $payload = $request->all();
            $invoice = $this->model->find($id);
            $sum_invoice = Invoice::whereOrderId($invoice->order_id)->get()->sum('nominal');
            $order = Order::findOrFail($invoice->order_id);
            $arrear = $order->total - $sum_invoice;

            if ($sum_invoice - $invoice->nominal + $request->nominal > $order->total){
                $response = [
                    'message' => 'Sisa dari invoice Rp.'.number_format($arrear + $invoice->nominal),
                ];
                return response()->json($response, 500);
            } else {
                $payload;
            }

            $data = $invoice->update($payload);

            DB::commit();

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $invoice = $this->model->find($id);
            $data = $invoice->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function getDetail($id)
    {
        try {
            $data = $this->model->with('company', 'order')->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }

    public function updateStatus(Request $request, $id){
        DB::beginTransaction();
        try {
            $payload = $request->status;
            $invoice = $this->model->find($id);
            $data = $invoice->update(['status' => $payload]);

            DB::commit();

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function generatePrintInvoice($id)
    {
        $currentDate = Carbon::now();
        $exp_date = $currentDate->addDays(7);
        $valid_date = $exp_date->toDateString();
        $invoice = Invoice::findOrfail($id);
        $order = Order::with('order_details', 'company')->findOrFail($invoice->order_id);

        // Set the Content-Type header to display the PDF inline
        return view('pages.admin.invoice.print', compact('invoice','order','valid_date'));
    }

    public function printInvoice($id)
    {
        $date = date("Y-m-d");
        $currentDate = Carbon::now();
        $exp_date = $currentDate->addDays(7);
        $valid_date = $exp_date->toDateString();
        $invoice = Invoice::findOrfail($id);
        $order = Order::with('order_details', 'company')->findOrFail($invoice->order_id);

        $pdf = Pdf::loadview('pages.admin.invoice.print', compact('invoice','order','valid_date'))->setpaper('a4','potrait');
        return $pdf->download($date.' - '.$invoice->reference.'.pdf');
    }

    public function allInvoice(InvoiceDataTable $dataTable, Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route('admin.invoice.all')]
        ]);

        $orderId = $request->orderId;

        $order = Order::whereId($orderId)->first();

        return $dataTable->render("pages." . $this->path . "." . $this->view . '.all-invoice', compact('order'));
    }
}
