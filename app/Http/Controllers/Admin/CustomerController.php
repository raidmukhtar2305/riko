<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CustomersDataTable;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Models\Company;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Customer $customer)
    {

        $this->middleware('can:customer.list')->only('index');
        $this->middleware('can:customer.create')->only('store');
        $this->middleware('can:customer.update')->only('update');
        $this->middleware('can:customer.delete')->only('destroy');

        $this->model    = $customer;
        $this->view     = "customer";
        $this->path     = "admin";
        $this->route    = "admin.customer";
        $this->title    = "Data Master";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomersDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Customer Management', route($this->route . '.index')]
        ]);

        return $dataTable->render("pages.".$this->path.".".$this->view.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Customer::all();
        return  to_route('', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        try {
            $payload                = $request->all();

            if (Auth::user()->hasRole('superadmin')) {
                $payload['company_id'] = $request['company_id'];
            } else {
                $payload['company_id'] = Auth::user()->company_id;
            }

            $data                   = $this->model->create($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
        try {
            $payload = $request->all();
            $user = $this->model->find($id);

            if (Auth::user()->hasRole('superadmin')) {
                $payload['company_id'] = $request['company_id'];
            } else {
                $payload['company_id'] = Auth::user()->company_id;
            }

            // if (!empty($payload['password'])) $payload['password'] = Hash::make($payload['password']);
            // else unset($payload['password']);

            $data = $user->update($payload);
            // DB::table('model_has_roles')->where('model_id',$user->id)->delete();

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = $this->model->find($id);
            $data = $user->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function detailCustomer($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Customer Detail', route($this->route . '.show', $id)]
        ]);

        $customer = $this->model->find($id);
        $datas       = [];
        $dates       = [];
        $customerByDate = [];
        $months      = ['January', 'February', 'March', 'April', 'May', 'Juny', 'July', 'August', 'September', 'October', 'November', 'December'];
        $years       = range(date('Y') - 5, date('Y'));
        $days        = Carbon::now()->month(date('m'))->daysInMonth;
        $today       = now()->toDateString();
        $thisMonth   = Carbon::now()->format('m');
        $thisYear    = Carbon::now()->format('Y');

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            for ($i=1; $i <= $days; $i++) {
                $dates[] = $i;
                $customerByDate[] = Order::
                    whereDate('date', date('Y').'-'.date('m').'-'.str_pad($i, 2, 0, STR_PAD_LEFT))
                    ->count();
            }
            for ($i = 1; $i <= 12; $i++) {
                $customerByMonth[] = Order::whereMonth('date', $i)
                    ->whereYear('date', $thisYear)
                    ->count();
            }
            foreach ($years as $year) {
                $customerByYear[] = Order::whereYear('date', $year)->count();
            }
        } else {
            for ($i = 1; $i <= $days; $i++) {
                $dates[] = $i;
                $customerByDate[] = Order::whereDate('date', date('Y') . '-' . date('m') . '-' . str_pad($i, 2, 0, STR_PAD_LEFT))
                ->whereCompanyId(Auth::user()->company_id)->count();
            }
            for ($i = 1; $i <= 12; $i++) {
                $customerByMonth[] = Order::whereMonth('date', $i)->whereCompanyId(Auth::user()->company_id)->whereYear('date', $thisYear)->count();
            }
            foreach ($years as $year) {
                $customerByYear[] = Order::whereYear('date', $year)->whereCompanyId(Auth::user()->company_id)->count();
            }
        }

        $dates          = json_encode($dates);
        $customerByDate    = json_encode($customerByDate);
        $customerByMonth   = json_encode($customerByMonth);
        $customerByYear    = json_encode($customerByYear);
        $months         = json_encode($months);
        $years          = json_encode($years);

        return view(
            'pages.admin.customer.show',
            compact(
                'customer',
                'customerByMonth',
                'customerByYear',
                'datas',
                'months',
                'years',
                'dates',
                'customerByDate',
            )
        );
    }

    public function getList(Request $request)
    {
        try {
            $data = $this->model
                ->select([
                    'customers.id',
                    'customers.name',
                    'companies.name as company_name',
                ])
                ->join('companies', 'companies.id', 'customers.company_id');

            if ($search = data_get($request, 'search')) {
                $data = $data->where(function ($query) use ($search) {
                    return $query
                        ->orWhere('customers.name', 'like', '%' . $search . '%')
                        ->orWhere('companies.name', 'like', '%' . $search . '%');
                });
            }

            if (!Auth::user()->hasRole('superadmin')) $data = $data->whereCompanyId(Auth::user()->company_id);

            $data = $data
                ->limit(20)
                ->orderBy('name')
                ->get()
                ->map(function ($data) {
                    $text = '';

                    if (Auth::user()->hasRole('superadmin'))
                        $text = data_get($data, 'company_name') . ' - ';

                    $data->text = $text . $data->name;

                    return $data;
                });

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }
}
