<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\QuotationDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use App\Models\Quotation;
use App\Models\StockLog;
use App\Models\QuotationDetail;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Outlet;
use App\Services\OrderService;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class QuotationController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Quotation $quotation)
    {

        $this->middleware('can:quotation.list')->only('index');
        $this->middleware('can:quotation.create')->only('store');
        $this->middleware('can:quotation.update')->only('update');
        $this->middleware('can:quotation.delete')->only('destroy');

        $this->model    = $quotation;
        $this->view     = "quotation";
        $this->path     = "admin";
        $this->route    = "admin.quotation";
        $this->title    = "Transaction";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(QuotationDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Quotation Management', route($this->route . '.index')]
        ]);


        return $dataTable->render("pages." . $this->path . "." . $this->view . '.index');

        // if ($request->ajax()) {

        //     $data = Quotation::with([
        //         'customer' => function ($query) {
        //             $query->select('id', 'name');
        //         },
        //         'company' => function ($query) {
        //             $query->select('id', 'name');
        //         },
        //     ]);

        //     if (!Auth::user()->hasRole('superadmin')) $data = $data->where('company_id', Auth::user()->company_id);

        //     return DataTables::of($data)
        //         ->editColumn('company.name', function ($data) {
        //             return data_get($data, 'company.name', '');
        //         })
        //         ->editColumn('customer.name', function ($data) {
        //             return data_get($data, 'customer.name', '');
        //         })
        //         ->editColumn('status', function ($query) {
        //             if ($query->status == 0) {
        //                 $status = 'Pending Order';
        //             } elseif ($query->status == 1) {
        //                 $status = 'Approve Order';
        //             }
        //             return $status;
        //         })
        //         ->addColumn('action', function ($row) {
        //             $id = $row->id;

        //             $btn = '
        //                 <div class="dropdown">
        //                     <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown" aria-expanded="false">
        //                         Action
        //                     </button>
        //                     <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">';

        //             if ($row->status == 0) {
        //                 $btn .= '
        //                         <li>
        //                             <a class="dropdown-item convert-order-btn" data-id="' . $id . '" href="#">Convert to Order</a>
        //                         </li>
        //                         <li>
        //                             <hr class="dropdown-divider">
        //                         </li>';
        //             }

        //             $btn .= '
        //                         <li><a class="dropdown-item edit-btn" href="' . route("admin.quotation.edit", $id) . '">Edit</a></li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.quotation.show", $id) . '">Show</a>
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.quotation.print-invoice", $id) . '" target="_blank">Preview Print Invoice</a>
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.quotation.print", $id) . '"><i class="fa fa-download" aria-hidden="true"></i> Download Invoice</a>
        //                         </li>
        //                         <li>
        //                             <hr class="dropdown-divider">
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item delete-btn" data-id="' . $id . '" href="#">Delete</a>
        //                         </li>
        //                     </ul>
        //                 </div>';

        //             return $btn;
        //         })
        //         ->rawColumns(['action'])
        //         ->make(true);
        // }

        // return view('pages.admin.quotation.index');
    }

    public function create(Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ["Create Quotation", route($this->route . '.create')]
        ]);

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            $products   = Product::get();
            $customers  = Customer::get();
            $companies  = Company::get();
        } else {
            $products   = Product::where('company_id', Auth::user()->company_id)->get();
            $customers  = Customer::where('company_id', Auth::user()->company_id)->get();
            $companies  = Company::get();
        }

        $outlets    = Outlet::get();

        $product_list = [];
        foreach ($products as $product) {
            $product_list[$product->id] = $product->price;
        }

        $product_list = json_encode($product_list);
        $quotation = null;

        return view('pages.' . $this->path . "." . $this->view . '.create', compact('companies', 'products', 'product_list', 'customers', 'quotation', 'outlets'));
    }


    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            // dd($input);
            $user = Auth::user()->hasRole('superadmin');
            if ($user) {
                $company = $input['company_id'];
            } else {
                $company = Auth::user()->company_id;
            }

            // dd(Auth::user());

            $quotation_data = [
                'quotation_code'    => quotation_code(),
                'company_id'        => $company,
                'customer_id'       => $input['customer_id'],
                'outlet_id'         => $input['outlet_id'],
                'date'              => $input['date'],
                'description'       => $input['description'],
                'status'            => 0,
            ];
            $quotation = Quotation::create($quotation_data);

            $total = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                $product_qty = $input['product_qty'][$key];

                $quotation
                    ->quotation_details()
                    ->create([
                        'product_id'            => $value,
                        'qty'                   => $product_qty,
                        'price'                 => $product->price,
                        'laba'                  => $product->laba,
                        'product_name'          => $product->name,
                        'product_description'   => $product->description,
                    ]);

                $total += $product->price * $product_qty;
            }

            $quotation->update(['total' => $total]);

            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $quotation
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ];

            return response()->json($response, 500);
        }
    }

    public function show($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Order Detail', route($this->route . '.show', $id)]
        ]);

        $quotation = Quotation::with('quotation_details')->find($id);

        return view('pages.admin.quotation.detail', compact('quotation'));
    }

    public function edit($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Edit Quotation', route($this->route . '.edit', $id)]
        ]);

        $quotation = Quotation::with('quotation_details')->find($id);

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            $products   = Product::get();
            $customers  = Customer::get();
        } else {
            $products   = Product::where('company_id', Auth::user()->company_id)->get();
            $customers  = Customer::where('company_id', Auth::user()->company_id)->get();
        }
        $companies  = Company::get();
        $outlets    = Outlet::get();

        return view('pages.admin.quotation.edit', compact('products', 'customers', 'quotation', 'companies', 'outlets'));
    }


    public function update(OrderRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $quotation = $this->model->find($id);

            if (data_get($request, 'company_id')) $quotation->company_id = $request->company_id;
            if (data_get($request, 'customer_id')) $quotation->customer_id = $request->customer_id;
            if (data_get($request, 'outlet_id')) $quotation->outlet_id = $request->outlet_id;
            $quotation->date = $request->date;
            $quotation->description = $request->description;

            foreach (explode(',', $request->quotation_detail_deleted) as $quotation_detail_deleted) {
                $quotation->quotation_details()->whereId($quotation_detail_deleted)->delete();
            }

            $total = 0;
            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);
                $product_qty = $input['product_qty'][$key];

                $quotation_detail = $quotation
                    ->quotation_details()
                    ->updateOrCreate(
                        [
                            'product_id' => $value,
                        ],
                        [
                            'qty'                   => $product_qty,
                            'price'                 => $product->price,
                            'laba'                  => $product->laba,
                            'product_name'          => $product->name,
                            'product_description'   => $product->description
                        ]
                    );

                $total += $quotation_detail->price * $product_qty;
            }

            $quotation->total = $total;
            $quotation->save();

            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $quotation
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ];

            return response()->json($response, 500);
        }
    }


    public function destroy($id)
    {
        try {
            $quotation = $this->model->find($id);
            $data = $quotation->delete();
            QuotationDetail::whereQuotationId($id)->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function getAddress(Request $request)
    {
        try {
            $data = MemberAddress::whereMemberId($request->member_id)->get();

            return response()->json($data);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function convertOrder($id)
    {
        DB::beginTransaction();
        try {
            $quotation = $this->model->with(['quotation_details'])->find($id);

            $order = OrderService::create([
                'date' => date('Y-m-d'),
                'company_id' => $quotation->company_id,
                'outlet_id' => $quotation->outlet_id,
                'customer_id' => $quotation->customer_id,
                'product_id' => $quotation->quotation_details()->orderBy('id')->pluck('product_id'),
                'product_qty' => $quotation->quotation_details()->orderBy('id')->pluck('qty'),
            ]);

            $quotation->status = 1;
            $quotation->order_id = $order->id;
            $quotation->save();

            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Success convert to order',
                'data' => $quotation
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];

            return response()->json($response, 500);
        }
    }

    public function printQuotation($id)
    {
        $time = time();
        $date = date("Y-m-d");
        $quotation = Quotation::find($id);
        $quotation_detail = QuotationDetail::whereQuotationId($id)->get();
        $currentDate = Carbon::now();
        $exp_date = $currentDate->addDays(7);
        $valid_date = $exp_date->toDateString();

        // $html = view('pages.admin.quotation.print', compact('quotation', 'quotation_detail'))->render();

        // // Instantiate Dompdf with the default configuration
        // $dompdf = new Dompdf();

        // // Load the HTML content
        // $dompdf->loadHtml($html);

        // // Set paper size and orientation
        // $dompdf->setPaper('A4', 'portrait');

        // // Render the HTML as PDF
        // $dompdf->render();

        // // Stream the generated PDF to the browser
        // return $dompdf->stream($date.' - '.$quotation->quotation_code.'.pdf');
        $pdf = Pdf::loadview('pages.admin.quotation.print', compact('quotation', 'quotation_detail', 'valid_date'));
        return $pdf->download($date . ' - ' . $quotation->quotation_code . '.pdf');
    }

    public function generatePrintInvoice($id)
    {

        $time = time();
        $date = date("Y-m-d");
        $currentDate = Carbon::now();
        $exp_date = $currentDate->addDays(7);
        $valid_date = $exp_date->toDateString();
        // dd($valid_date->toDateString());
        $quotation = Quotation::findOrFail($id);
        $quotation_detail = QuotationDetail::whereQuotationId($id)->get();

        // Set the Content-Type header to display the PDF inline
        return view('pages.admin.quotation.print', compact('quotation', 'quotation_detail', 'valid_date'));
    }
}
