<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\WarehouseDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Warehouse;
use App\Http\Requests\WarehouseRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class WarehouseController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Warehouse $warehouse)
    {

        $this->middleware('can:warehouse.list')->only('index');
        $this->middleware('can:warehouse.create')->only('store');
        $this->middleware('can:warehouse.update')->only('update');
        $this->middleware('can:warehouse.delete')->only('destroy');

        $this->model    = $warehouse;
        $this->view     = "warehouse";
        $this->path     = "admin";
        $this->route    = "admin.warehouse";
        $this->title    = "Data Master";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }


    public function index(WarehouseDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Warehouse Management', route($this->route . '.index')]
        ]);

        return $dataTable->render("pages.".$this->path.".".$this->view.'.index');
    }


    public function create()
    {
        //
    }


    public function store(WarehouseRequest $request)
    {
        try {
            $payload = (Auth::user()->hasRole('superadmin')) ? $request->all() : $request->merge(['company_id' => Auth::user()->company_id])->all();

            $data = $this->model->create($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response);
        }
    }


    public function show($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response);
        }
    }


    public function edit(Warehouse $warehouse)
    {
        //
    }


    public function update(WarehouseRequest $request, $id)
    {
        try {
            $payload = $request->all();
            $warehouse = $this->model->find($id);

            $data = $warehouse->update($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }


    public function destroy($id)
    {
        try {
            $warehouse = $this->model->find($id);
            $data = $warehouse->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function detailWarehouse($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Warehouse Detail', route($this->route . '.show', $id)]
        ]);

        $warehouse = Warehouse::with(['stock_log_groups'])->find($id);

        return view('pages.admin.warehouse.show', compact('warehouse'));
    }

    public function getList(Request $request)
    {
        try {
            $data = $this->model
                ->select([
                    'warehouses.id',
                    'warehouses.name',
                    'companies.name as company_name',
                ])
                ->join('companies', 'companies.id', 'warehouses.company_id');

            if ($search = data_get($request, 'search')) {
                $data = $data->where(function ($query) use ($search) {
                    return $query
                        ->orWhere('warehouses.name', 'like', '%' . $search . '%')
                        ->orWhere('companies.name', 'like', '%' . $search . '%');
                });
            }

            if (!Auth::user()->hasRole('superadmin')) $data = $data->whereCompanyId(Auth::user()->company_id);
            $data = $data
                ->limit(20)
                ->orderBy('name')
                ->get()
                ->map(function ($data) {
                    $text = '';

                    if (Auth::user()->type == 'superadmin')
                        $text = data_get($data, 'company_name') . ' - ';

                    $data->text = $text . $data->name;

                    return $data;
                });

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }
}
