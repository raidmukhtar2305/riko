<?php

namespace App\Http\Controllers\Admin;

use App\Models\Level;
use App\Models\Outlet;
use App\Models\Order;
use App\Models\Member;
use App\Models\Invoice;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\DataTables\ListOrderDataTable;
use Carbon\Carbon;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(ListOrderDataTable $dataTable)
    {
        $datas       = [];
        $dates       = [];
        $months      = ['January', 'February', 'March', 'April', 'May', 'Juny', 'July', 'August', 'September', 'October', 'November', 'December'];
        $years       = range(date('Y') - 5, date('Y'));
        $days        = Carbon::now()->month(date('m'))->daysInMonth;
        $orderByDate = [];

        $today      = now()->toDateString();
        $thisMonth  = Carbon::now()->format('m');
        $thisYear   = Carbon::now()->format('Y');

        $user = Auth::user()->hasRole('superadmin');
        if ($user)
        {
            $totalOrdersToday = Order::whereDate('date',$today)->count('id');
            $totalOrdersThisMonth = Order::whereMonth('date',$thisMonth)->whereYear('date',$thisYear)->count('id');
            $totalOrdersThisYear = Order::whereYear('date',$thisYear)->count('id');

            $totalProfitByToday = OrderDetail::whereHas('order', function ($query) use ($today){
                $query->where('date',$today);
            })->sum('profit');
            $totalProfitByThisMonth = OrderDetail::whereHas('order', function ($query) use ($thisMonth, $thisYear){
                $query->whereMonth('date',$thisMonth)->whereYear('date',$thisYear);
            })->sum('profit');
            $totalProfitByThisYear = OrderDetail::whereHas('order', function ($query) use ($thisYear){
                $query->whereYear('date',$thisYear);
            })->sum('profit');
            for ($i=1; $i <= $days; $i++) {
                $dates[] = $i;
                $orderByDate[] = Order::
                    whereDate('date', date('Y').'-'.date('m').'-'.str_pad($i, 2, 0, STR_PAD_LEFT))
                    ->count();
                $profitByDate[] = OrderDetail::
                whereHas('order', function ($query) use($i){
                    $query->whereDate('date', date('Y').'-'.date('m').'-'.str_pad($i, 2, 0, STR_PAD_LEFT));
                })->sum('profit');
            }

            for ($i=1; $i <= 12; $i++) {
                $orderByMonth[] = Order::whereMonth('date', $i)
                    ->whereYear('date', $thisYear)
                    ->count();
                $profitByMonth[] = OrderDetail::
                whereHas('order', function ($query) use($i,$thisYear){
                    $query->whereMonth('date', $i)->whereYear('date', $thisYear);
                })->sum('profit');
            }

            foreach ($years as $year) {
                $orderByYear[] = Order::whereYear('date', $year)->count();

                $profitByYear[] = OrderDetail::
                whereHas('order', function ($query) use($year){
                    $query->whereYear('date', $year);
                })->sum('profit');
            }
        }
        else
        {
            $totalOrdersToday = Order::whereDate('date',$today)->whereCompanyId(Auth::user()->company_id)->count('id');
            $totalOrdersThisMonth = Order::whereMonth('date',$thisMonth)->whereCompanyId(Auth::user()->company_id)->whereYear('date',$thisYear)->count('id');
            $totalOrdersThisYear = Order::whereYear('date',$thisYear)->whereCompanyId(Auth::user()->company_id)->count('id');

            $totalProfitByToday = OrderDetail::whereHas('order', function ($query) use ($today){
                $query->where('date',$today)->whereCompanyId(Auth::user()->company_id);
            })->sum('profit');
            $totalProfitByThisMonth = OrderDetail::whereHas('order', function ($query) use ($thisMonth, $thisYear){
                $query->whereMonth('date',$thisMonth)->whereYear('date',$thisYear)->whereCompanyId(Auth::user()->company_id);
            })->sum('profit');
            $totalProfitByThisYear = OrderDetail::whereHas('order', function ($query) use ($thisYear){
                $query->whereYear('date',$thisYear)->whereCompanyId(Auth::user()->company_id);
            })->sum('profit');
            for ($i=1; $i <= $days; $i++) {
                $dates[] = $i;
                $orderByDate[] = Order::whereDate('date', date('Y').'-'.date('m').'-'.str_pad($i, 2, 0, STR_PAD_LEFT))
                    ->whereCompanyId(Auth::user()->company_id)
                    ->count();
                $profitByDate[] = OrderDetail::
                whereHas('order', function ($query) use($i){
                    $query
                        ->whereCompanyId(Auth::user()->company_id)
                        ->whereDate('date', date('Y').'-'.date('m').'-'.str_pad($i, 2, 0, STR_PAD_LEFT));
                })->sum('profit');
            }

            for ($i=1; $i <= 12; $i++) {
                $orderByMonth[] = Order::whereMonth('date', $i)
                    ->whereCompanyId(Auth::user()->company_id)
                    ->whereYear('date', $thisYear)
                    ->count();
                $profitByMonth[] = OrderDetail::
                whereHas('order', function ($query) use($i,$thisYear){
                    $query
                        ->whereMonth('date', $i)
                        ->whereYear('date', $thisYear)
                        ->whereCompanyId(Auth::user()->company_id);
                })->sum('profit');
            }

            foreach ($years as $year) {
                $orderByYear[] = Order::whereYear('date', $year)
                    ->whereCompanyId(Auth::user()->company_id)
                    ->count();

                $profitByYear[] = OrderDetail::
                whereHas('order', function ($query) use($year){
                    $query
                        ->whereYear('date', $year)
                        ->whereCompanyId(Auth::user()->company_id);
                })->sum('profit');
            }
        }

        $dates          = json_encode($dates);
        $orderByDate    = json_encode($orderByDate);
        $orderByMonth   = json_encode($orderByMonth);
        $orderByYear    = json_encode($orderByYear);
        $profitByDate   = json_encode($profitByDate);
        $profitByMonth  = json_encode($profitByMonth);
        $profitByYear   = json_encode($profitByYear);
        $months         = json_encode($months);
        $years          = json_encode($years);

        return $dataTable->render('pages.admin.dashboard',
        compact('orderByMonth',
                'orderByYear',
                'profitByMonth',
                'profitByYear',
                'datas',
                'months',
                'years',
                'dates',
                'orderByDate',
                'profitByDate',
                'totalOrdersToday',
                'totalOrdersThisMonth',
                'totalOrdersThisYear',
                'totalProfitByToday',
                'totalProfitByThisMonth',
                'totalProfitByThisYear',
            ));
    }
}
