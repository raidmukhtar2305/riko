<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Company;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Milon\Barcode\Facades\DNS1DFacade;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Product $product)
    {

        $this->middleware('can:product.list')->only('index');
        $this->middleware('can:product.create')->only('store');
        $this->middleware('can:product.update')->only('update');
        $this->middleware('can:product.delete')->only('destroy');

        $this->model    = $product;
        $this->view     = "products";
        $this->path     = "admin";
        $this->route    = "admin.products";
        $this->title    = "Data Master";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(ProductsDataTable $dataTable, Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Product Management', route($this->route . '.index')]
        ]);

        $categories = (Auth::user()->hasRole('superadmin')) ? Category::get() : Category::whereCompanyId(Auth::user()->company_id)->get();

        return $dataTable->render("pages." . $this->path . "." . $this->view . '.index', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        try {
            $input = $request->all();

            $input['company_id'] = Auth::user()->company_id;
            if (Auth::user()->hasRole('superadmin')) $input['company_id'] = $request['company_id'];
            $product = $this->model->create($input);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $product
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function show($id)
    {
        try {
            $data = $this->model->with('category')->find($id);
            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function detail($id)
    {
        $data = $this->model->find($id);
        $datas       = [];
        $dates       = [];
        $productByDate = [];
        $months      = ['January', 'February', 'March', 'April', 'May', 'Juny', 'July', 'August', 'September', 'October', 'November', 'December'];
        $years       = range(date('Y') - 5, date('Y'));
        $days        = Carbon::now()->month(date('m'))->daysInMonth;
        $today       = now()->toDateString();
        $thisMonth   = Carbon::now()->format('m');
        $thisYear    = Carbon::now()->format('Y');

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            for ($i=1; $i <= $days; $i++) {
                $dates[] = $i;
                $productByDate[] = OrderDetail::
                    whereDate('created_at', date('Y').'-'.date('m').'-'.str_pad($i, 2, 0, STR_PAD_LEFT))
                    ->count();
            }
            for ($i = 1; $i <= 12; $i++) {
                $productByMonth[] = OrderDetail::whereMonth('created_at', $i)
                    ->whereYear('created_at', $thisYear)
                    ->count();
            }
            foreach ($years as $year) {
                $productByYear[] = OrderDetail::whereYear('created_at', $year)->count();
            }
        } else {
            for ($i = 1; $i <= $days; $i++) {
                $dates[] = $i;
                $productByDate[] = OrderDetail::whereDate('created_at', date('Y') . '-' . date('m') . '-' . str_pad($i, 2, 0, STR_PAD_LEFT))->whereHas('order', function ($query) {
                    $query->whereCompanyId(Auth::user()->company_id);
                })->count();
            }
            for ($i = 1; $i <= 12; $i++) {
                $productByMonth[] = OrderDetail::whereMonth('created_at', $i)->whereHas('order', function ($query) use ($thisYear) {
                    $query->whereCompanyId(Auth::user()->company_id)->whereYear('created_at', $thisYear);
                })->count();
            }
            foreach ($years as $year) {
                $productByYear[] = OrderDetail::whereYear('created_at', $year)->whereHas('order', function ($query) {
                    $query->whereCompanyId(Auth::user()->company_id);
                })->count();
            }
        }

        $dates          = json_encode($dates);
        $productByDate    = json_encode($productByDate);
        $productByMonth   = json_encode($productByMonth);
        $productByYear    = json_encode($productByYear);
        $months         = json_encode($months);
        $years          = json_encode($years);

        return view(
            'pages.admin.products.detail',
            compact(
                'data',
                'productByMonth',
                'productByYear',
                'datas',
                'months',
                'years',
                'dates',
                'productByDate',
            )
        );
    }


    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $product = $this->model->find($id);

            if (Auth::user()->hasRole('superadmin')) $input['company_id'] = $request['company_id'];
            $data = $product->update($input);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }


    public function destroy($id)
    {
        try {
            // ProductImage::whereProductId($id)->delete();
            $product = $this->model->find($id);
            $data = $product->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function productImages($id)
    {
        try {
            $data = ProductImage::where('product_id', $id)->get();
            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function getList(Request $request)
    {
        try {
            $data = $this->model
                ->select([
                    'products.id',
                    'products.name',
                    'products.price',
                    'products.company_id',
                    'companies.name as company_name',
                ])
                ->join('companies', 'companies.id', 'products.company_id');

            if ($search = data_get($request, 'search')) {
                $data = $data->where(function ($query) use ($search) {
                    return $query
                        ->orWhere('products.name', 'like', '%' . $search . '%')
                        ->orWhere('products.price', 'like', '%' . $search . '%')
                        ->orWhere('companies.name', 'like', '%' . $search . '%');
                });
            }

            if (!Auth::user()->hasRole('superadmin')) $data = $data->whereCompanyId(Auth::user()->company_id);

            $data = $data
                ->limit(20)
                ->orderBy('name')
                ->get()
                ->map(function ($data) {
                    $text = '';

                    if (Auth::user()->hasRole('superadmin'))
                        $text = data_get($data, 'company_name') . ' - ';

                    $data->text = $text . $data->name . ' - ' . number_format($data->price, 2);

                    return $data;
                });

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }
}
