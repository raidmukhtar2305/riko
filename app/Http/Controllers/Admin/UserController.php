<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\UserDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\Company;
use App\Models\Outlet;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DataTables;

class UserController extends Controller
{
    protected $model;
    protected $view;

    public function __construct(User $user)
    {
        $this->middleware('can:user.list')->only('index');
        $this->middleware('can:user.create')->only('store');
        $this->middleware('can:user.update')->only('update');
        $this->middleware('can:user.delete')->only('destroy');

        $this->model        = $user;
        $this->view         = "pages.admin.users";
        $this->route        = "admin.users";
        $this->title        = "User Management";
        $this->roles        = Role::all();
        $this->companies    = Company::all();
        $this->outlets      = Outlet::all();


        View::share('route', $this->route);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
        View::share('roles', $this->roles);
        View::share('companies', $this->companies);
        View::share('outlets', $this->outlets);


    }

    public function index(UserDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'.index')],
            ['User List', route($this->route.'.index')]
        ]);

        $roles = (Auth::user()->hasRole('superadmin')) ? Role::get() : Role::whereNotIn('name', ['superadmin'])->get();
        $companies = (Auth::user()->hasRole('superadmin')) ? Company::get() : Company::whereId(Auth::user()->company_id)->get();
        $outlets = (Auth::user()->hasRole('superadmin')) ? Outlet::get() : Outlet::whereCompanyId(Auth::user()->company_id)->get();

        return $dataTable->render($this->view . '.index', compact('roles', 'companies', 'outlets'));
    }

    public function create()
    {
        //
    }

    public function store(UserRequest $request , UserDataTable $dataTable)
    {
        try {
            $payload = (Auth::user()->hasRole('superadmin')) ? $request->all() : $request->merge(['company_id' => Auth::user()->company_id])->all();
            $payload['password'] = Hash::make($payload['password']);
            $payload['type'] = $payload['role'];

            // if($request->file('image')) {
            //     $filename = $request->file('image')->getClientOriginalName();
            //     Storage::putFileAs(
            //         'public/images',
            //         $request->file('image'),
            //         $filename
            //     );
            //     $payload['image'] = $filename;
            // }

            $data = $this->model->create($payload);
            $data->assignRole($payload['role']);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }


    public function show($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }


    public function edit(User $user)
    {
        //
    }


    public function update(Request $request, $id)
    {
        try {
            $payload = $request->all();
            $user = $this->model->find($id);

            if (!empty($payload['password'])) $payload['password'] = Hash::make($payload['password']);
            else unset($payload['password']);
            $payload['type'] = $payload['role'];

            // if($request->file('image')) {
            //     $filename = $request->file('image')->getClientOriginalName();
            //     Storage::putFileAs(
            //         'public/images',
            //         $request->file('image'),
            //         $filename
            //     );
            //     $payload['image'] = $filename;
            // }

            $data = $user->update($payload);
            DB::table('model_has_roles')->where('model_id',$user->id)->delete();
            $user->assignRole($payload['role']);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $user = $this->model->find($id);
            $data = $user->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function userDetail($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'.index')],
            ['User Detail', route($this->route.'.show', $id)]
        ]);

        $user = User::find($id);
        return view('pages.admin.users.detail', compact('user'));
    }
}
