<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReportDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Company;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use View;
use DataTables;
use Illuminate\Support\Facades\Auth;

class ReportOrderController extends Controller
{
    protected $view;

    public function __construct()
    {
        $this->middleware('can:reports.orders.list')->only('index');

        $this->view     = "pages.admin.orders.report";
        $this->path     = "admin";
        $this->route    = "admin.order";
        $this->title    = "Report Order Management";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('title', $this->title);
    }

    public function index()
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'.report')]
        ]);
        
        if (Auth::user()->hasRole('superadmin')) {
            $customers = Customer::get();
            $users = User::get();
        } else {
            $customers = Customer::whereCompanyId(Auth::user()->company_id)->get();
            $users = User::whereCompanyId(Auth::user()->company_id)->get();
        }

        return view($this->view.'.index', compact('customers', 'users'));

    }
    public function records(Request $request,Order $model)
    {
        if ($request->ajax()) {
            $payload = json_decode($request->data);
            $data = Order::with('customer', 'company');

            if (isset($payload->start_date) && isset($payload->end_date))
                $data = $data->where(function($q_order) use ($payload){
                        $q_order->whereBetween('date', [$payload->start_date, $payload->end_date]);
                    });

            if ( isset($payload->customer_id) && $payload->customer_id != 'ALL')
                $data = $data->where(function($q_order) use ($payload){
                            $q_order->whereCustomerId($payload->customer_id);
                        });

            if ( isset($payload->user_id) && $payload->user_id != "ALL")
                $data = $data->where(function($q_order) use ($payload){
                            $q_order->whereCreatedBy($payload->user_id);
                        });

            $data = $data->withCount(['invoices as paid' => function ($query) {
                $query->where('status', '1')->select(DB::raw('SUM(nominal) as total'));
            }])->get();

            return Datatables::of($data)->addIndexColumn()
                ->editColumn('company', function ($row) {
                    $companyname = !empty($row->company->name) ? $row->company->name : '-';
                    return $companyname;
                })
                ->editColumn('customer', function ($row) {
                    $customername = !empty($row->customer->name) ? $row->customer->name : '-';
                    return $customername;
                })
                ->editColumn('total', function ($row){
                        return  'Rp. '. number_format($row->total);
                })
                ->editColumn('status', function ($row) {
                            if ($row->paid < $row->total)
                                return 'belum lunas';
                                if ($row->paid == $row->total)
                                return 'lunas';
                                if ($row->paid > $row->total)
                                return 'lebih';
                })
                ->make(true);

        }
    }
}
