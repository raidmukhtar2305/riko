<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProcurementDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcurementRequest;
use Illuminate\Http\Request;
use App\Models\Procurement;
use App\Models\StockLog;
use App\Models\ProcurementDetail;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class ProcurementController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Procurement $procurement)
    {

        $this->middleware('can:procurement.list')->only('index');
        $this->middleware('can:procurement.create')->only('store');
        $this->middleware('can:procurement.update')->only('update');
        $this->middleware('can:procurement.delete')->only('destroy');

        $this->model    = $procurement;
        $this->view     = "procurement";
        $this->path     = "admin";
        $this->route    = "admin.procurement";
        $this->title    = "Procurement";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index(ProcurementDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Procurement', route($this->route . '.index')]
        ]);


        return $dataTable->render("pages.".$this->path.".".$this->view.'.index');

        // if ($request->ajax()) {

        //     $data = Procurement::query()
        //         ->select([
        //             'procurements.id',
        //             'procurements.company_id',
        //             'procurements.procurement_code',
        //             'procurements.date',
        //             'procurements.description',
        //             'companies.name as company_name',
        //         ])
        //         ->join('companies', 'companies.id', 'procurements.company_id');

        //     if (!Auth::user()->hasRole('superadmin')) {
        //         $data = $data->where('company_id', Auth::user()->company_id);
        //     }

        //     if ($search = $request->get('search')) {
        //         $data = $data->where(function ($query) use ($search) {
        //             return $query
        //                 ->orWhere('status', 'like', '%' . $search . '%')
        //                 ->orWhere('companies.name', 'like', '%' . $search . '%')
        //                 ->orWhere('procurement_code', 'like', '%' . $search . '%')
        //                 ->orWhere('date', 'like', '%' . $search . '%');
        //         });
        //     }

        //     return DataTables::of($data)
        //         ->addColumn('action', function ($row) {
        //             $id = $row->id;

        //             $btn = '
        //                 <div class="dropdown">
        //                     <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuAction" data-bs-toggle="dropdown"
        //                         aria-expanded="false">
        //                         Action
        //                     </button>
        //                     <ul class="dropdown-menu" aria-labelledby="dropdownMenuAction">
        //                         <li><a class="dropdown-item edit-btn" href="' . route("admin.procurement.edit", $id)  . '">Edit</a></li>
        //                         <li><a class="dropdown-item" href="' . route("admin.procurement.show", $id) . '">Show</a></li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.procurement.print-preview", $id) . '" target="_blank">Preview Print Procurement</a>
        //                         </li>
        //                         <li>
        //                             <a class="dropdown-item" href="' . route("admin.procurement.print", $id) . '"><i class="fa fa-download" aria-hidden="true"></i> Download Procurement</a>
        //                         </li>
        //                         <li>
        //                             <hr class="dropdown-divider">
        //                         </li>
        //                         <li><a class="dropdown-item delete-btn" data-id="' . $id . '" href="#">Delete</a></li>
        //                     </ul>
        //                 </div>
        //                 ';

        //             return $btn;
        //         })
        //         ->make(true);
        // }

        // return view('pages.admin.procurement.index');
    }

    public function create(Request $request)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ["Create Procurement", route($this->route . '.create')]
        ]);

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            $products   = Product::get();
            $companies  = Company::get();
        } else {
            $products   = Product::where('company_id', Auth::user()->company_id)->get();
            $companies  = Company::get();
        }

        $product_list = [];
        foreach ($products as $product) {
            $product_list[$product->id] = $product->price;
        }

        $product_list = json_encode($product_list);
        $procurement = null;

        return view('pages.' . $this->path . "." . $this->view . '.create', compact('companies', 'products', 'product_list', 'procurement'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $user = Auth::user()->hasRole('superadmin');
            if ($user) {
                $company = $input['company_id'];
            } else {
                $company = Auth::user()->company_id;
            }

            $procurement_data = [
                'procurement_code' => procurement_code(),
                'warehouse_id' => $input['warehouse_id'],
                'date' => $input['date'],
                'description' => $input['description'],
                'status' => 0,
                'company_id' => $company,
                'created_by' => Auth::user()->id,
            ];
            $procurement = Procurement::create($procurement_data);

            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);

                $procurement_detail = $procurement
                    ->procurement_details()
                    ->create([
                        'product_id'            => $value,
                        'qty'                   => $input['product_qty'][$key],
                        'price'                 => $product->price,
                        'product_name'          => $product->name,
                        'product_description'   => $product->description,
                    ]);

                if (!$product->is_service) {
                    StockLog::create([
                        'product_id' => $value,
                        'reference_id' => $procurement_detail->id,
                        'reference_slug' => config('constants.table_references.procurement_detail'),
                        'qty' => $procurement_detail->qty,
                        'company_id' => $procurement->company_id,
                        'warehouse_id' => $procurement->warehouse_id,
                    ]);
                }
            }

            // create invoice
            if ($request->is_paid) {
                $payloadInv['reference'] = 'INV' . date('YmdHis');
                $payloadInv['company_id'] = $input['company_id'];
                $payloadInv['date'] = $input['date'];
                $payloadInv['note'] = $input['invoice_note'];
                $payloadInv['description'] = $input['invoice_description'];
                $payloadInv['nominal'] = 0;
                $payloadInv['status'] = '1';
                $payloadInv['procurement_id'] = $procurement->id;
                Invoice::create($payloadInv);
            }

            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $procurement
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }

    public function show($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Procurement Detail', route($this->route . '.show', $id)]
        ]);

        $procurement = Procurement::with('procurement_details', 'refunds')->find($id);

        return view('pages.admin.procurement.detail', compact('procurement'));
    }

    public function edit($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Edit Procurement', route($this->route . '.edit', $id)]
        ]);

        $procurement = Procurement::with('procurement_details')->find($id);

        $user = Auth::user()->hasRole('superadmin');
        if ($user) {
            $products   = Product::get();
        } else {
            $products   = Product::where('company_id', Auth::user()->company_id)->get();
        }

        $companies  = Company::get();

        return view('pages.admin.procurement.edit', compact('products', 'procurement', 'companies'));
    }

    public function update(ProcurementRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $procurement = $this->model->find($id);
            $procurement->date = $request->date;
            $procurement->description = $request->description;
            if (data_get($request, 'company_id')) $procurement->company_id = $request->company_id;
            if (data_get($request, 'warehouse_id')) $procurement->warehouse_id = $request->warehouse_id;

            foreach (explode(',', $request->procurement_detail_deleted) as $procurement_detail_deleted) {
                $procurement->procurement_details()->whereId($procurement_detail_deleted)->delete();
                StockLog::whereReferenceId($procurement_detail_deleted)->whereReferenceSlug(config('constants.table_references.procurement_detail'))->delete();
            }

            foreach ($input['product_id'] as $key => $value) {
                $product = Product::find($value);

                $procurement_detail = $procurement
                    ->procurement_details()
                    ->updateOrCreate(
                        [
                            'product_id' => $value,
                        ],
                        [
                            'qty' => $input['product_qty'][$key],
                            'price' => $product->price,
                            'product_name' => $product->name,
                            'product_description' => $product->description
                        ]
                    );

                if (!$product->is_service) {
                    StockLog::updateOrCreate(
                        [
                            'reference_id' => $procurement_detail->id,
                            'reference_slug' => config('constants.table_references.procurement_detail'),
                        ],
                        [
                            'company_id' => $procurement->company_id,
                            'warehouse_id' => $procurement->warehouse_id,
                            'product_id' => $value,
                            'qty' => $procurement_detail->qty,
                        ]
                    );
                }
            }

            $procurement->save();
            DB::commit();

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $procurement
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $procurement = $this->model->find($id);
            $data = $procurement->delete();
            ProcurementDetail::whereProcurementId($id)->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function updateStatus($id)
    {
        try {
            $procurement = $this->model->find($id);
            $input = ['status' => $procurement->status + 1];
            $data = $procurement->update($input);

            $response = [
                'success' => true,
                'message' => 'Success update status',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function showResi($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function getAddress(Request $request)
    {
        try {
            $data = MemberAddress::whereMemberId($request->member_id)->get();

            return response()->json($data);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function addResi(Request $request, $id)
    {
        try {
            $input = $request->all();
            $procurement = $this->model->find($id);
            $input['status'] = 3;
            $data = $procurement->update($input);

            $response = [
                'success' => true,
                'message' => 'Success update status',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function printProcurement($id)
    {
        $time = time();
        $date = date("Y-m-d");
        $procurement = Procurement::with('procurement_details', 'procurement_details.product', 'company')->find($id);

        $pdf = Pdf::loadview('pages.admin.procurement.print', compact('procurement'))->setPaper('a4', 'potrait');
        return $pdf->download($date . ' - ' . $procurement->procurement_code . '.pdf');
    }

    public function generatePrintProcurement($id)
    {

        $time = time();
        $date = date("Y-m-d");
        $procurement = Procurement::with('procurement_details', 'procurement_details.product', 'company')->find($id);

        // Set the Content-Type header to display the PDF inline
        return view('pages.admin.procurement.print', compact('procurement'));
    }
}
