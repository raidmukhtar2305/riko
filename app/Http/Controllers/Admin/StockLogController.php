<?php

namespace App\Http\Controllers\Admin;

use View;
use App\Models\StockLog;
use App\Models\Product;
use App\Helpers\FileHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\DataTables\StockLogDataTable;
use Illuminate\Support\Facades\Auth;

class StockLogController extends Controller
{
    protected $model;
    protected $view;

    public function __construct(StockLog $stocklog){

        $this->middleware('can:stock-log.list')->only('index');
        // $this->middleware('can:stock-log.create')->only('store');
        // $this->middleware('can:stock-log.update')->only('update');
        $this->middleware('can:stock-log.delete')->only('destroy');

        $this->model    = $stocklog;
        $this->view     = "stock-log";
        $this->path     = "admin";
        $this->route    = "admin.stock-log";
        $this->title    = "Stock Log Management";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    public function index($product, StockLogDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'.index')]
        ]);

        return $dataTable->with(['product' => $product])->render("pages.".$this->path.".".$this->view.'.index');
    }

    public function show($id)
    {
        try {
            $data = $this->model->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $category = $this->model->find($id);
            $data = $category->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }
}
