<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\DataTables\CompanyDataTable;
use App\Models\CompanyPackage;
use App\Models\Package;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CompanyController extends Controller
{
    protected $model;
    protected $view;
    protected $path;
    protected $route;
    protected $title;

    public function __construct(Company $company)
    {

        $this->middleware('can:company.list')->only('index');
        $this->middleware('can:company.create')->only('store');
        $this->middleware('can:company.update')->only('update');
        $this->middleware('can:company.delete')->only('destroy');

        $this->model    = $company;
        $this->view     = "company";
        $this->path     = "admin";
        $this->route    = "admin.company";
        $this->title    = "Data Master";

        View::share('path', $this->path);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
        View::share('packages', Package::orderby('name', 'asc')->get());
    }

    public function index(CompanyDataTable $dataTable)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Company Management', route($this->route . '.index')]
        ]);

        return $dataTable->render("pages." . $this->path . "." . $this->view . '.index');
    }

    public function store(CompanyRequest $request)
    {
        DB::beginTransaction();
        try {
            $payload = $request->all();
            
            if ($request->hasFile('logo')) {
                $logoPath = $request->file('logo');


                $logoName = time() . '.' . $logoPath->getClientOriginalExtension();
                $logoPath->move(public_path('/storage/logos/'), $logoName);
                $payload['logo'] = $logoName;
            }
            
            $data = $this->model->create($payload);
            
            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function showPage($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route . '.index')],
            ['Company Detail', route($this->route . '.show', $id)]
        ]);

        $detail = Company::with('company_packages.package')->find($id);
        return view("pages." . $this->path . "." . $this->view . '.detail', compact('detail'));
    }

    public function show($id)
    {
        try {
            $data = $this->model->with('company_packages')->find($id);

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $payload = $request->all();
            $company = $this->model->find($id);

             if ($company->logo) {
                Storage::disk('public')->delete('logos/' . $company->logo);
            }
            if ($request->hasFile('logo')) {
                $logoPath = $request->file('logo');


                $logoName = time() . '.' . $logoPath->getClientOriginalExtension();
                $logoPath->move(public_path('/storage/logos/'), $logoName);
                $payload['logo'] = $logoName;
            }   

            $data = $company->update($payload);

            $response = [
                'success' => true,
                'message' => 'Success save data',
                'data' => $data
            ];

            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $company = $this->model->find($id);
            Storage::disk('public')->delete('logos/' . $company->logo);
            $data = $company->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function deletePackage($companyId, $id)
    {
        try {
            $data = CompanyPackage::whereId($id)->delete();

            $response = [
                'success' => true,
                'message' => 'Success delete data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json($response, 500);
        }
    }

    public function storePackage(Request $request, $companyId)
    {
        DB::beginTransaction();
        try {
            $payload    = $request->all();
            $payload['company_id'] = $companyId;

            $latestCompanyPackage = CompanyPackage::where('company_id', $companyId)->with('package')->latest('expire_date')->first();
            $package = Package::find($payload['package_id']);

            if ($latestCompanyPackage && $latestCompanyPackage->package_id == $payload['package_id']) {
                $expireDate = Carbon::createFromDate($latestCompanyPackage->expire_date)->addDays($package->summary_days);
                $latestCompanyPackage->update(['expire_date' => $expireDate]);
            } else {
                $payload['expire_date'] = Carbon::now()->addDays($package->summary_days);
                $data = CompanyPackage::create($payload);
            }

            DB::commit();
            return to_route($this->route . '.show-page', $companyId);
        } catch (\Exception $e) {
            DB::rollback();

            return to_route($this->route . '.show-page', $companyId);
        }
    }

    public function getList(Request $request)
    {
        try {
            $data = $this->model
                ->select([
                    'id',
                    'name as text',
                ]);

            if ($search = data_get($request, 'search')) {
                $data = $data->where('name', 'like', '%' . $search . '%');
            }

            $data = $data
                ->limit(20)
                ->orderBy('name')
                ->get();

            $response = [
                'success' => true,
                'message' => 'Success retrieve data',
                'data' => $data
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => 'Server Error',
                'data' => $e->getMessage()
            ];

            return response()->json($response, 500);
        }
    }
}
