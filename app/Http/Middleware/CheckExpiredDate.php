<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class CheckExpiredDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $today = date('Y-m-d');

        if (!empty(Auth::user()->company))
            if (!empty(Auth::user()->company->selected_package->expire_date)) {
                if ($today > Auth::user()->company->selected_package->expire_date) {
                    Auth::logout();
                    redirect(route('admin.login'))->withErrors([
                        'error' => "Your package has expired, Please contact admin.",
                    ]);
                }
            } else {
                Auth::logout();
                redirect(route('admin.login'))->withErrors([
                    'error' => "You don't have a package, Please contact admin.",
                ]);
            }

        if (Auth::check() == false) {
            Auth::logout();
            redirect(route('admin.login'));
        }
        return $next($request);
    }
}
