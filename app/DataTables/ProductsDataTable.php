<?php

namespace App\DataTables;

use App\Models\Product;
use App\Models\Category;
use App\Models\Company;
use App\Models\StockLog;
use App\Models\Unit;
use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use View;
use Illuminate\Support\Facades\Auth;

class ProductsDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct()
    {
        $this->view     = "products";
        $this->path     = "admin";

        // View::share('categories', Category::where('company_id', Auth::user()->company_id));
        View::share('companies', Company::all());
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('iteration', function ($query) {
                static $i = 1;
                return $i++;
            })
            ->addColumn('action', function ($query) {
                $product = $query;
                return view('pages.admin.products.action', compact('product'));
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['price', 'company_name', 'category_name', 'action']);
    }

    public function query(Product $model)
    {
        if (Auth::user()->hasRole('superadmin')) {
            return $model->orderBy('created_at', 'desc')->newQuery();
        } else {
            return $model
                ->where('company_id', Auth::user()->company_id)
                ->orderBy('created_at', 'desc');
        }
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('products-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(0)
            ->buttons(['export']);
    }


    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name' => 'iteration',
                'title' => 'No',
                'data' => 'iteration',
                'orderable' => false,
            ],
            [
                'name' => 'name',
                'title' => 'Nama ',
                'data' => 'name',
            ],
            Column::computed('action')
             ->exportable(false)
             ->printable(false)
             ->width(30)
             ->addClass('text-end')
             ->title('Action'),
        ];
    }

}
