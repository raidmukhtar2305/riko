<?php

namespace App\DataTables;

use App\Models\Invoice;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class InvoiceDataTable extends DataTable
{
    protected $model;
    protected $view;
    protected $path;

    public function __construct()
    {
        $this->view     = "invoice";
        $this->path     = "admin";
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($query) {
                $invoice = $query;
                // dd($invoice);
                return view("pages." . $this->path . "." . $this->view . ".action", compact('invoice'));
            })
            ->addColumn('actions', function ($query) {
                $invoice = $query;
                return view("pages." . $this->path . "." . $this->view . ".action-for-invoice-managament", compact('invoice'));
            })
            ->addColumn('total', function ($query) {
                $order = Order::whereId($query->order_id)->get();
                return  'Rp. ' . number_format($order->sum('total'));
            })
            ->addColumn('nominal', function ($query) {
                return  'Rp. ' . number_format($query->nominal);
            })
            ->addColumn('aging', function ($query) {
                return date_diff(date_create($query->date), date_create(date('Y-m-d')))->format('%r%a days');
            })
            ->addColumn('status', function ($query) {
                if ($query->status == 0 ) return 'Belum lunas';
                if ($query->status == 1) return 'Lunas';
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['total', 'nominal', 'action']);
    }

    public function query(Invoice $model)
    {
        $model = $model->select([
                                'invoices.id',
                                'invoices.company_id',
                                'invoices.order_id',
                                'invoices.reference',
                                'invoices.date',
                                'invoices.nominal',
                                'invoices.description',
                                'invoices.note',
                                'invoices.created_at',
                                'invoices.status',
                                'companies.name as company_name',
                                'orders.order_code as order_code',
                                ])
                        ->join('companies', 'companies.id', '=', 'invoices.company_id')
                        ->join('orders', 'orders.id', '=', 'invoices.order_id');

        $data = ($this->orderId != null) ? $model->whereOrderId($this->orderId) : $model;

        $datas = (Auth::user()->hasRole('superadmin')) ? $data : $data->where('companies.id', Auth::user()->company_id);

        return $datas->orderBy('invoices.created_at', 'desc')->newQuery();
    }

    public function html()
    {
        return $this
            ->builder()
            ->setTableId('invoice-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(0)
            ->buttons(['export']);
    }

    protected function getColumns()
    {
        $columns = collect();
        if (Auth::user()->hasRole('superadmin')) {
            $columns->add(Column::make('company_name'));
        }
        $columns->add(Column::make('order_code'));
        $columns->add(Column::make('reference')->title('Nomor Order'));
        $columns->add(Column::make('date'));
        $columns->add(Column::make('total'));
        $columns->add(Column::make('nominal'));
        $columns->add(Column::make('description'));
        $columns->add(Column::make('note'));
        $columns->add(Column::make('status'));
        if ($this->orderId == null) {
            $columns->add(Column::make('aging'));
            $columns->add(
                Column::computed('actions')
                    ->title('Action')
                    ->exportable(false)
                    ->printable(false)
                    ->width(60)
                    ->addClass('text-center')
            );
        } else {
            $columns->add(
                Column::computed('action')
                    ->exportable(false)
                    ->printable(false)
                    ->width(60)
                    ->addClass('text-center')
            );
        }
        return $columns->toArray();
    }
}
