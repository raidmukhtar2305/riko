<?php

namespace App\DataTables;

use App\Models\Quotation;
use App\Models\Category;
use App\Models\ShippingVendor;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use View;
use Illuminate\Support\Facades\Auth;

class QuotationDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "quotation";
        $this->path     = "admin";
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->addColumn('status', function ($query) {
                if ($query->status == 0) {
                    $status = 'Pending Order';
                }elseif ($query->status == 1){
                    $status = 'Approve Order';
                }
                return $status;
            })
            ->addColumn('total', function($query) { 
                return  'Rp. '. number_format($query->total); 
            })
            ->addColumn('company_name', function($query) { 
                return @$query->company->name; 
            })
            ->addColumn('customer_name', function($query) { 
                return @$query->customer->name; 
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns([
                        'company_name',
                        'customer_name',
                        'status',
                        'total', 
                        'action'
                    ]);
    }

    public function query(Quotation $model)
    {
        $datas = (Auth::user()->hasRole('superadmin')) ? $model : $model->where('company_id', Auth::user()->company_id);
        return $datas->orderBy('created_at', 'desc')->newQuery();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('quotation-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy('0')
                    ->buttons(['export']);
    }


    protected function getColumns()
    {

        $user = Auth::user()->hasRole('superadmin');
        $columns = [
            Column::make('customer_name'),
            Column::make('quotation_code'),
            Column::make('date'),
            Column::make('total'),
            Column::make('status'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];

        if ($user) { 
            array_unshift($columns, Column::make('company_name'));
        }
        
        return $columns;
    }
}
