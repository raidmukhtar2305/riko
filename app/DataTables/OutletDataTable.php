<?php

namespace App\DataTables;

use App\Models\Outlet;
use App\Models\Company;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use View;
use Illuminate\Support\Facades\Auth;

class OutletDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "outlet";
        $this->path     = "admin";

        View::share('companies', Company::all());
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->addColumn('company_name', function($query) {
                return @$query->company->name;
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['company_name','action']);
    }

    public function query(Outlet $model)
    {
        $data = $model->select([
            'outlets.id',
            'outlets.company_id',
            'outlets.warehouse_id',
            'outlets.name',
            'outlets.address',
            'outlets.phone',
            'outlets.created_at',
            'companies.name as company_name',
        ])
            ->join('companies', 'companies.id', '=', 'outlets.company_id');

        $datas = (Auth::user()->hasRole('superadmin')) ? $data : $data->whereCompanyId(Auth::user()->company_id);

        return $datas->orderBy('outlets.created_at', 'desc')->newQuery();
    }


    public function html()
    {
        return $this->builder()
                    ->setTableId('outlet-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }


    protected function getColumns()
    {
        $columns = collect();
        if (Auth::user()->hasRole('superadmin')) {
            $columns->add(Column::make('company_name'));
        }
        $columns->add(Column::make('name'));
        $columns->add(Column::make('address'));
        $columns->add(Column::make('phone'));
        $columns->add(
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
        );
        return $columns->toArray();
    }

}
