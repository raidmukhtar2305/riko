<?php

namespace App\DataTables;

use App\Models\Category;
use Carbon\Carbon;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use View;
use Illuminate\Support\Facades\Auth;

class CategoriesDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct()
    {
        $this->view = "categories";
        $this->path     = "admin";
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('iteration', function ($query) {
                static $i = 1;
                return $i++;
            })
            ->addColumn('action', function ($query) {
                $category = $query;
                return view('pages.admin.categories.action', compact('category'));
            })
            ->rawColumns(['name','action']);
    }

    public function query(Category $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html()
    {
        return $this->builder()
       
        ->setTableId('category-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(1)
        ->buttons(['export']);
}


    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            [
                'name' => 'iteration',
                'title' => 'No',
                'data' => 'iteration',
                'orderable' => false,
            ],
            [
                'name' => 'name',
                'title' => 'Nama',
                'data' => 'name',
            ],
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(30)
            ->addClass('text-end')
            ->title('Action'),
        ];
    }
}
