<?php

namespace App\DataTables;

use App\Models\Package;
use Carbon\Carbon;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PackagesDatatable extends DataTable
{
    protected $view;
    protected $path;

    public function __construct(){
        $this->view     = "packages";
        $this->path     = "admin";
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->addColumn('price', function($query) {
                return  'Rp. '. number_format($query->price);
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['action']);
    }

    public function query(Package $model)
    {
        return $model->orderBy('created_at', 'desc')->newQuery();
     
    }


    public function html()
    {
        return $this->builder()
                    ->setTableId('packages-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }


    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('price'),
            Column::make('description'),
            Column::make('summary_days'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

}
