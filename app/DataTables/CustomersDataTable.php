<?php

namespace App\DataTables;

use App\Models\Customer;
use App\Models\Company;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Auth;
use View;

class CustomersDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "customer";
        $this->path     = "admin";

        View::share('companies', Company::all());
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->addColumn('company_name', function($query) { 
                return @$query->company->name; 
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['company_name', 'action']);
    }

    public function query(Customer $model)
    {
        if (Auth::user()->hasRole('superadmin')) {
            return $model->orderBy('created_at', 'desc')->newQuery();
        } else {
            return $model
            ->where('company_id', Auth::user()->company_id)
            ->orderBy('created_at', 'desc');
        }
    }


    public function html()
    {
        return $this->builder()
                    ->setTableId('customer-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }


    protected function getColumns()
    {
        $user = Auth::user()->hasRole('superadmin');
        $columns = [
            Column::make('name'),
            Column::make('phone_number'),
            Column::make('email'),
            Column::make('address'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];

        if ($user) { 
            array_unshift($columns, Column::make('company_name'));
        }
        
        return $columns;
    }

}
