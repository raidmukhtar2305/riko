<?php

namespace App\DataTables;

use App\Models\Bank;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use View;

class BanksDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct()
    {
        $this->view = "bank";
        $this->path     = "admin";
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('iteration', function ($query) {
                static $i = 1;
                return $i++;
            })
            ->addColumn('action', function ($query) {
                $bank = $query;
                return view('pages.admin.banks.action', compact('bank'));
            })
            ->rawColumns(['name','action']);
    }

    public function query(Bank $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html()
    {
        return $this->builder()
       
        ->setTableId('banks-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(1)
        ->buttons(['export']);
}


    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            [
                'name' => 'iteration',
                'title' => 'No',
                'data' => 'iteration',
                'width' => '5%',
                'orderable' => false,
            ],
            [
                'name' => 'name',
                'title' => 'Nama',
                'data' => 'name',
                'width' => '30%',
            ],
            [
                'name' => 'account',
                'title' => 'Rekening',
                'data' => 'account',
                'width' => '20%',
            ],
            [
                'name' => 'bank',
                'title' => 'Nama Bank',
                'data' => 'bank',
                'width' => '20%',
            ],
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    } 
}
