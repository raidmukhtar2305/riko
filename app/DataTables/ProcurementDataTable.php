<?php

namespace App\DataTables;

use App\Models\Procurement;
use App\Models\Category;
use App\Models\ProcurementDetail;
use App\Models\ShippingVendor;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProcurementDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "procurement";
        $this->path     = "admin";
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->addColumn('status', function ($query) {
                if ($query->paid < $query->total) return 'belum lunas';
                if ($query->paid == $query->total) return 'lunas';
                if ($query->paid > $query->total) return 'lebih';
            })
            ->addColumn('total', function($query) {
                return  'Rp. '. number_format($query->total);
            })
            ->addColumn('company_name', function($query) {
                return @$query->company->name;
            })
            ->addColumn('customer_name', function($query) {
                return @$query->customer->name;
            })
            ->addColumn('product', function ($query) {
                $procurement_details = ProcurementDetail::whereProcurementId($query->id)->get();
                // dd($procurement_details);
                foreach ($procurement_details as $key => $value) {
                    return @$value->product->name;
                }
            })
            ->addColumn('qty', function ($query) {
                $procurement_details = ProcurementDetail::whereProcurementId($query->id)->get();
                foreach ($procurement_details as $key => $value) {
                    return $value->qty;
                }
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns([
                        'company_name',
                        'customer_name',
                        'product',
                        'qty',
                        'status',
                        'total',
                        'action'
                    ]);
    }

    public function query(Procurement $model)
    {
        if (Auth::user()->hasRole('superadmin')) {
            $model = $model->orderBy('created_at', 'desc')->newQuery();
        } else {
            $model = $model
            ->where('company_id', Auth::user()->company_id)
            ->orderBy('created_at', 'desc');
        }
        return $model = $model->withCount(['invoices AS paid' => function ($query) {
            $query->where('status', '1')->select(DB::raw('SUM(nominal) as total'));
        }]);
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('procurement-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy('0')
                    ->buttons(['export']);
    }


    protected function getColumns()
    {

        $user = Auth::user()->hasRole('superadmin');
        $columns = [
            Column::make('procurement_code'),
            Column::make('product'),
            Column::make('qty'),
            Column::make('date'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];

        if ($user) {
            array_unshift($columns, Column::make('company_name'));
        }

        return $columns;
    }
}
