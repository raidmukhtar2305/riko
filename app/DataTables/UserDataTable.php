<?php
namespace App\DataTables;

use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Auth;

class UserDataTable extends DataTable
{

    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "users";
        $this->path     = "admin";
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('company_name', function($query) { 
                return @$query->company->name;
            })
            ->addColumn('outlet_name', function($query) { 
                return @$query->outlet->name;
            })
            ->addColumn('role', function($query) { 
                return @$query->type; 
            })
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['company_name', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $datas = (Auth::user()->hasRole('superadmin')) ? $model : $model->whereCompanyId(Auth::user()->company_id);

        return $datas->orderBy('created_at', 'desc')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $user = Auth::user()->hasRole('superadmin');
        $columns = [
            Column::make('name'),
            Column::make('username'),
            Column::make('email'),
            Column::make('role'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];

        if ($user) {
            array_unshift($columns, Column::make('company_name'));
            array_unshift($columns, Column::make('outlet_name'));
        }

        return $columns;
    }


}
