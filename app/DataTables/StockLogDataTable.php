<?php

namespace App\DataTables;

use App\Models\StockLog;
use App\Models\Company;
use Carbon\Carbon;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class StockLogDataTable extends DataTable
{
    protected $model;
    protected $view;
    protected $path;

    public function __construct()
    {
        $this->view     = "stock-log";
        $this->path     = "admin";

        View::share('companies', Company::all());
    }

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('company_name', function ($query) {
                return data_get($query, 'company.name');
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d'))
            ->addIndexColumn();
    }

    public function query(StockLog $model)
    {
        if (Auth::user()->hasRole('superadmin')) {
            return $model->whereProductId($this->product)->orderBy('created_at', 'desc')->newQuery();
        } else {
            return $model
                ->where('company_id', Auth::user()->company_id)
                ->whereProductId($this->product)
                ->orderBy('created_at', 'desc');
        }
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('stocklog-table')
            ->orderBy(1)
            ->columns($this->getColumns())
            ->languagePaginatePrevious('&larr;')
            ->languagePaginateNext('&rarr;')
            ->minifiedAjax()
            ->initComplete("function (settings, json) {
                        this.api().columns([1]).every(function (index) {
                            var column = this;
                            var colDef = settings['aoColumns'][index];
                            var colTitle = settings['aoColumns'][index]['title'];

                            console.log(column, colDef)

                            var input = document.createElement(\"input\");
                            $(input).attr( 'class', 'form-control form-control-solid');
                            $(input).attr( 'placeholder', 'Search ' + colTitle);
                            $(input).appendTo($(column.footer()).empty())
                            .on('keyup', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                        });
                    }");
    }


    protected function getColumns()
    {
        $columns = [
            Column::make('warehouse_id'),
            Column::make('reference_id'),
            Column::make('reference_slug'),
            Column::make('qty'),
            Column::make('created_at'),
        ];

        if (Auth::user()->type == 'superadmin') {
            array_unshift($columns, Column::make('company_name'));
        }

        return $columns;
    }
}
