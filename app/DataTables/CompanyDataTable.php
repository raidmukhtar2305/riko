<?php

namespace App\DataTables;

use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;


class CompanyDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "company";
        $this->path     = "admin";
    }


     /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

     public function dataTable($query)
     {
         return datatables()
             ->eloquent($query)
             ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
             ->addColumn('selected_packages', function($row) { 
                 return $row->company_packages->map(function($q) {
                     return "<span class='badge bg-primary m-1'>".@$q->package->name."</span>";
                 })->implode("");
             })           
             ->editColumn('created_at', function($row) {
                 return Carbon::parse($row->created_at)->format('Y-m-d H:i');
             })
             ->editColumn('updated_at', function($row) {
                 return Carbon::parse($row->updated_at)->format('Y-m-d H:i');
             })
             ->rawColumns(['action', 'logo', 'selected_packages']);
     }
     

    public function query(Company $model)
    {
        $datas = (Auth::user()->hasRole('superadmin')) ? $model : $model->whereId(Auth::user()->company_id);

        return $datas->orderBy('created_at', 'desc')->newQuery();
    }


    public function html()
    {
        return $this->builder()
                    ->setTableId('company-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }


    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('email'),
            Column::make('address'),
            Column::make('phone'),
            Column::make('selected_packages'),
            Column::make('description'),
            Column::make('bank_name'),
            Column::make('account_name'),
            Column::make('account_number'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

}
