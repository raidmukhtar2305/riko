<?php

namespace App\DataTables;

use App\Models\Warehouse;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use View;

class WarehouseDataTable extends DataTable
{
    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "warehouse";
        $this->path     = "admin";

        View::share('companies', Company::all());
    }

    
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', "pages.".$this->path.".".$this->view.'.action')
            ->addColumn('company_name', function($query) {
                return @$query->company->name;
            })
            ->editColumn('created_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->editColumn('updated_at', Carbon::parse($this->created_at)->format('Y-m-d H:i'))
            ->rawColumns(['company_name','action']);
    }


    public function query(Warehouse $model)
    {
        $data = $model->select([
            'warehouses.id',
            'warehouses.company_id',
            'warehouses.name',
            'warehouses.address',
            'warehouses.phone',
            'warehouses.created_at',
            'companies.name as company_name',
        ])
            ->join('companies', 'companies.id', '=', 'warehouses.company_id');

        $datas = (Auth::user()->hasRole('superadmin')) ? $data : $data->whereCompanyId(Auth::user()->company_id);

        return $datas->orderBy('warehouses.created_at', 'desc')->newQuery();
    }


    public function html()
    {
        return $this->builder()
                    ->setTableId('warehouse-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }


    protected function getColumns()
    {
        $columns = collect();
        if (Auth::user()->hasRole('superadmin')) {
            $columns->add(Column::make('company_name'));
        }
        $columns->add(Column::make('name'));
        $columns->add(Column::make('address'));
        $columns->add(Column::make('phone'));
        $columns->add(
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
        );
        return $columns->toArray();
    }
}
