<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class Menu extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    protected $fillable = [
        'name',
        'route',
        'parent_id',
        'permission_id',
    ];

    protected $appends = ['slug'];

    public function getSlugAttribute()
    {
        return Str::slug($this->name, '_');
    }

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }

    public function permission()
    {
        return $this->belongsTo(\Spatie\Permission\Models\Permission::class);
    }

    public function role_has_permission()
    {
        if(!empty(Auth::user()))
            return $this->belongsTo(RoleHasPermission::class, 'permission_id', 'permission_id')->where('role_id', @Auth::user()->roles->pluck('id')->first());
        else
            return $this->belongsTo(RoleHasPermission::class, 'permission_id', 'permission_id');
    }
}
