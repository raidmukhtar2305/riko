<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class ProductImage extends Model
{
    use HasFactory,Uuids;
    protected $tabel = 'product_images';
    protected $fillable = [
        'product_id',
        'image',
    ];

    protected $appends = ['file'];
    public function getFileAttribute(){
        $data =  asset('products'). '/'. $this->image;
        return $data;
    }
}
