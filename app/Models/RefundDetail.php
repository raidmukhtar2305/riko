<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefundDetail extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    protected $fillable = [
        'refund_id',
        'product_id',
        'qty'
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
