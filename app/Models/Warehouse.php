<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Warehouse extends Model
{
    use HasFactory, SoftDeletes, Uuids;

    protected $fillable = [
        'name',
        'company_id',
        'address',
        'phone',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function stock_logs()
    {
        return $this
            ->hasMany(StockLog::class, 'warehouse_id', 'id');
    }

    public function stock_log_groups()
    {
        return $this
            ->stock_logs()
            ->select([
                'warehouse_id',
                'product_id',
                DB::raw('sum(qty) as stock')
            ])
            ->groupBy([
                'product_id',
                'warehouse_id'
            ]);
    }
}
