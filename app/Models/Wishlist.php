<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Wishlist extends Model
{
    use HasFactory,Uuids;

    protected $fillable = [
        'product_id',
        'member_id'
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function member(){
        return $this->belongsTo(Member::class);
    }
}
