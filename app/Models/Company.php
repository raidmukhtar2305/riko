<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Company extends Model
{
    use HasFactory,SoftDeletes,Uuids;

    protected $fillable = [
        'name',
        'email',
        'logo',
        'address',
        'phone',
        'bank_name',
        'account_name',
        'account_number',
        'description',
    ];

    protected $appends = [
        'selected_packages'
    ];

    public function company_packages() {
        return $this->hasMany(CompanyPackage::class);
    }

    public function getSelectedPackagesAttribute() {
        return $this->company_packages->map(function($q) {
            return '<span class=\"badge bg-primary m-1\">'.@$q->package->name.'</span>';
        })->implode("");
    }

    public function selected_package() {
        return $this->hasOne(CompanyPackage::class)->orderBy('expire_date', 'desc');
    }

}
