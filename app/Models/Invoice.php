<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends BaseModel
{
    use HasFactory,SoftDeletes,Uuids;

    protected $fillable = [
        'company_id',
        'order_id',
        'reference',
        'date',
        'nominal',
        'status',
        'description',
        'note',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
