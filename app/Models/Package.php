<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Package extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'description',
        'summary_days'
    ];
}
