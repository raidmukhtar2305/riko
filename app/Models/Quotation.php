<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    protected $fillable = [
        'id',
        'customer_id',
        'company_id',
        'user_id',
        'outlet_id',
        'order_id',
        'quotation_code',
        'date',
        'no_rek_buyer',
        'name_buyer',
        'description',
        'total',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function quotation_details()
    {
        return $this->hasMany(QuotationDetail::class, 'quotation_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id', 'id');
    }
}
