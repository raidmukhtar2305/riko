<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyPackage extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'package_id',
        'expire_date'
    ];

    public function package() {
        return $this->belongsTo(Package::class);
    }
}
