<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    protected $fillable = [

        'name',

    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

 
    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'product_id', 'id');
    }

    public function stock_logs()
    {
        return $this
            ->hasMany(StockLog::class, 'product_id', 'id')
            ->where('warehouse_id', '<>', '')
            ->where('product_id', '<>', '');
    }

    public function stock_log_groups()
    {
        return $this
            ->stock_logs()
            ->select([
                'product_id',
                'warehouse_id',
                DB::raw('sum(qty) as stock')
            ])
            ->groupBy([
                'product_id',
                'warehouse_id'
            ]);
    }
}
