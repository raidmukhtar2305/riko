<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Refund extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    protected $fillable = [
        'order_id',
        'datetime',
        'total',
        'remarks'
    ];

    public function refund_details(): HasMany
    {
        return $this->hasMany(RefundDetail::class);
    }

    public function order(): BelongsTo 
    {
        return $this->belongsTo(Order::class);
    }
}
