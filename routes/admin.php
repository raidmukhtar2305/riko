<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\MerchantController;
use App\Http\Controllers\Admin\MemberController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\LevelController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\MemberAddressController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\TrackingOrderController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\UnitController;
use App\Http\Controllers\Admin\ShippingVendorController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\CompanyPackageController;
use App\Http\Controllers\Admin\ForgotPasswordController;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\StockLogController;
use App\Http\Controllers\Admin\RefundController;
use App\Http\Controllers\Admin\OutletController;
use App\Http\Controllers\Admin\WarehouseController;
use App\Http\Controllers\Admin\QuotationController;
use App\Http\Controllers\Admin\ReportOrderController;
use App\Http\Controllers\Admin\ReportProductController;
use App\Http\Controllers\Admin\ProcurementController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\BankController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/login', [AuthController::class, 'index'])->name('login');
    Route::post('/login', [AuthController::class, 'login'])->name('login.submit');

    Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
    Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
    Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
    Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

    Route::middleware(['auth', 'check.expired'])->group(function () {
        Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
        Route::get('/', [HomeController::class, 'index'])->name('dashboard');
        //timpa route update karena upload image formdata harus pake post biar inputannya masuk ke controller
        Route::post('roles/{role}', [RoleController::class, 'update'])->name('admin.roles.update');
        Route::resource('roles', RoleController::class);

        Route::post('permissions/{permission}', [PermissionController::class, 'update'])->name('admin.permissions.update');
        Route::resource('permissions', PermissionController::class);

        // Route::get('member/diagram/{member_id}', [MemberController::class, 'diagram'])->name('member.diagram');
        // Route::get('member/level/{member_id}', [MemberController::class, 'level'])->name('member.level');
        // Route::resource('member', MemberController::class);

        // Route::get('member/member-address/{member_id}', [MemberAddressController::class, 'index'])->name('member.member-address');
        // Route::get('member/member-address/show/{id}', [MemberAddressController::class, 'show'])->name('member.member-address.show');
        // Route::post('member/member-address/{member_id}/store', [MemberAddressController::class, 'store'])->name('member.member-address.store');
        // Route::put('member/member-address/update/{id}', [MemberAddressController::class, 'update'])->name('member.member-address.update');
        // Route::delete('member/member-address/destroy/{id}', [MemberAddressController::class, 'destroy'])->name('member.member-address.destroy');
        // Route::post('banks/{bank}', [BankController::class, 'update'])->name('banks.update');
        Route::resource('banks', BankController::class);
        Route::get('banks/detail/{id}', [BankController::class, 'detail'])->name('banks.detail');
        


        Route::post('menus/{menu}', [MenuController::class, 'update'])->name('admin.menus.update');
        Route::resource('menus', MenuController::class);

        Route::post('users/{user}', [UserController::class, 'update'])->name('admin.users.update');
        Route::get('users/detail/{id}', [UserController::class, 'userDetail'])->name('users.detail');
        Route::resource('users', UserController::class);

        // Route::post('levels/{level}', [LevelController::class, 'update'])->name('admin.levels.update');
        // Route::resource('levels', LevelController::class);

        // Route::resource('merchants', MerchantController::class);
        // Route::get('products/{id}', [ProductController::class, 'show'])->name('admin.products.show');
        // Route::post('products/{id}', [ProductController::class, 'update'])->name('admin.products.update');
        Route::get('products/product-images/{id}', [ProductController::class, 'productImages'])->name('admin.products.product-images');
        Route::get('product/get-list', [ProductController::class, 'getList'])->name('products.get-list');
        Route::post('products', [ProductController::class, 'update'])->name('products.update');
        Route::resource('products', ProductController::class);
        Route::get('products/show/{id}', [ProductController::class, 'detail'])->name('products.detail');
        Route::get('product/report', [ReportProductController::class, 'index'])->name('product.report');
        Route::get('product/report/records', [ReportProductController::class, 'records'])->name('product.report.records');

        Route::resource('categories', CategoryController::class);
        Route::get('category/detail/{id}', [CategoryController::class, 'detailCategory'])->name('category.show');

        // Route::resource('orders', OrderController::class);

        Route::prefix('orders')->name('orders.')->group(function () {
            Route::get('', [OrderController::class, 'index'])->name('index');
            Route::get('/create', [OrderController::class, 'create'])->name('create');
            Route::post('/store', [OrderController::class, 'store'])->name('store');
            Route::post('/update/{id}', [OrderController::class, 'update'])->name('update');
            Route::delete('/destroy/{id}', [OrderController::class, 'destroy'])->name('destroy');
            Route::get('/get-address', [OrderController::class, 'getAddress'])->name('get-address');
            Route::get('/show/{order_id}', [OrderController::class, 'show'])->name('show');
            Route::get('/edit/{order_id}', [OrderController::class, 'edit'])->name('edit');
            Route::get('/update-status/{order_id}', [OrderController::class, 'updateStatus'])->name('update-status');
            Route::get('/edit-resi/{order_id}', [OrderController::class, 'showResi'])->name('show-resi');
            Route::put('/edit-resi/{order_id}', [OrderController::class, 'addResi'])->name('edit-resi');
            Route::resource('{orderId}/refunds', RefundController::class);
            Route::get('refunds/{refund_id}/details', [RefundController::class, 'getDetails'])->name('refunds.get-details');
            Route::get('refunds/{refund_id}/print', [RefundController::class, 'print'])->name('refunds.print');
            Route::get('/print/{id}', [OrderController::class, 'printOrder'])->name('print');
            Route::get('/print-preview/{id}', [OrderController::class, 'generatePrintOrder'])->name('print-preview');
            Route::get('/print-struk/{id}', [OrderController::class, 'PrintStruck'])->name('print-struk');
        });

        Route::get('quotation', [QuotationController::class, 'index'])->name('quotation.index');
        Route::get('quotation/create', [QuotationController::class, 'create'])->name('quotation.create');
        Route::post('quotation/store', [QuotationController::class, 'store'])->name('quotation.store');
        Route::post('quotation/update/{id}', [QuotationController::class, 'update'])->name('quotation.update');
        Route::delete('quotation/destroy/{id}', [QuotationController::class, 'destroy'])->name('quotation.destroy');
        Route::get('quotation/show/{id}', [QuotationController::class, 'show'])->name('quotation.show');
        Route::get('quotation/edit/{id}', [QuotationController::class, 'edit'])->name('quotation.edit');
        Route::get('quotation/convert-order/{id}', [QuotationController::class, 'convertOrder'])->name('quotation.convert-order');
        Route::get('quotation/print-pdf/{id}', [QuotationController::class, 'printQuotation'])->name('quotation.print');
        Route::get('quotation/print-invoice/{id}', [QuotationController::class, 'generatePrintInvoice'])->name('quotation.print-invoice');

        // Route::get('quotation/print/{id}', [QuotationController::class, 'printget'])->name('quotation.printget');

        Route::get('profile', [ProfileController::class, 'index'])->name('profile');
        Route::post('profile', [ProfileController::class, 'update'])->name('profile.update');

        Route::get('customer/get-list', [CustomerController::class, 'getList'])->name('customer.get-list');
        Route::resource('customer', CustomerController::class);
        Route::get('customer/detail/{id}', [CustomerController::class, 'detailCustomer'])->name('customer.detail');

        Route::get('company/get-list', [CompanyController::class, 'getList'])->name('company.get-list');
        Route::post('company', [CompanyController::class, 'update'])->name('company.update');
        Route::resource('company', CompanyController::class);
        Route::get('company/{companyId}/show', [CompanyController::class, 'showPage'])->name('company.show-page');
        Route::delete('company/{companyId}/packages/{id}', [CompanyController::class, 'deletePackage'])->name('company.packages.delete');
        Route::post('company/{companyId}/packages', [CompanyController::class, 'storePackage'])->name('company.packages.store');
        Route::get('stock-log/by-product/{product}', [StockLogController::class, 'index'])->name('stock-log.index-by-product');
        Route::resource('stock-log', StockLogController::class);
        Route::resource('package', PackageController::class);

        Route::resource('invoice', InvoiceController::class);
        Route::get('invoice-all', [InvoiceController::class, 'allInvoice'])->name('invoice.all');
        Route::get('invoice/get-detail/{id}', [InvoiceController::class, 'getDetail'])->name('invoice.get-detail');
        Route::get('invoice/print-pdf/{id}', [invoiceController::class, 'printInvoice'])->name('invoice.print');
        Route::get('invoice/print-preview/{id}', [invoiceController::class, 'generatePrintInvoice'])->name('invoice.print-preview');
        Route::put('invoice/update-status/{id}', [invoiceController::class, 'updateStatus'])->name('invoice.update-status');


        Route::get('outlet/get-list', [OutletController::class, 'getList'])->name('outlet.get-list');
        Route::resource('outlet', OutletController::class);
        Route::get('outlet/detail/{id}', [OutletController::class, 'detailOutlet'])->name('outlet.detail');

        Route::get('warehouse/detail/{id}', [WarehouseController::class, 'detailWarehouse'])->name('warehouse.detail');
        Route::get('warehouse/get-list', [WarehouseController::class, 'getList'])->name('warehouse.get-list');
        Route::resource('warehouse', WarehouseController::class);

        //report order
        Route::get('report/order', [ReportOrderController::class, 'index'])->name('order.report');
        Route::get('order/report/records', [ReportOrderController::class, 'records'])->name('order.report.records');


        Route::get('procurement', [ProcurementController::class, 'index'])->name('procurement.index');
        Route::get('procurement/create', [ProcurementController::class, 'create'])->name('procurement.create');
        Route::post('procurement/store', [ProcurementController::class, 'store'])->name('procurement.store');
        Route::get('procurement/show/{procurement_id}', [ProcurementController::class, 'show'])->name('procurement.show');
        Route::get('procurement/edit/{procurement_id}', [ProcurementController::class, 'edit'])->name('procurement.edit');
        Route::post('procurement/update/{id}', [ProcurementController::class, 'update'])->name('procurement.update');
        Route::delete('procurement/destroy/{id}', [ProcurementController::class, 'destroy'])->name('procurement.destroy');
        Route::get('procurement/update-status/{procurement_id}', [ProcurementController::class, 'updateStatus'])->name('procurement.update-status');
        Route::get('procurement/edit-resi/{procurement_id}', [ProcurementController::class, 'showResi'])->name('procurement.show-resi');
        Route::put('procurement/edit-resi/{procurement_id}', [ProcurementController::class, 'addResi'])->name('procurement.edit-resi');
        Route::get('procurement/print/{id}', [ProcurementController::class, 'printProcurement'])->name('procurement.print');
        Route::get('procurement/print-preview/{id}', [ProcurementController::class, 'generatePrintProcurement'])->name('procurement.print-preview');
    });
    Route::prefix('orders')->name('orders.')->group(function () {
        Route::get('/print/{id}', [OrderController::class, 'printOrder'])->name('print');
    });
});
