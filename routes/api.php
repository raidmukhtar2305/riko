<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CustomerController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\InvoiceController;
use App\Http\Controllers\API\CompanyController;
use App\Http\Controllers\API\GrafikController;
use App\Http\Controllers\API\QuotationController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);


Route::middleware('auth:api')->group(function () {
    Route::get('profile', [AuthController::class, 'profile']);
    Route::put('profile/update', [AuthController::class, 'update']);

    Route::post('logout', [AuthController::class, 'logout']);
    
    Route::get('orderbymonth', [GrafikController::class, 'orderByMonth']);
    Route::get('profitbymonth', [GrafikController::class, 'profitByMonth']);

    Route::get('invoice/{id}', [InvoiceController::class, 'invoice']);
    Route::get('invoice/{id}/show', [InvoiceController::class, 'show']);
    Route::post('invoice', [InvoiceController::class, 'store']);
    Route::post('invoice/{id}/update', [InvoiceController::class, 'update']);
    Route::get('invoice/print-pdf/{id}', [invoiceController::class, 'printInvoice'])->name('invoice.print');
    Route::put('invoice/update-status/{id}', [invoiceController::class, 'updateStatus'])->name('invoice.update-status');
    
    Route::post('product/{id}/update', [ProductController::class, 'update']);
    Route::get('stock-log/by-product/{product}', [ProductController::class, 'stockLog']);
    Route::resource('product', ProductController::class);

    Route::post('customer/{id}/update', [CustomerController::class, 'update']);
    Route::resource('customer', CustomerController::class);
    Route::resource('order', OrderController::class);


    Route::post('quotation/convert-order/{id}', [QuotationController::class, 'convertOrder']);
    Route::get('quotation/print-invoice/{id}', [QuotationController::class, 'downloadInvoice']);

    Route::resource('quotation', QuotationController::class);

    Route::post('categories/{id}/update', [CategoryController::class, 'update']);
    Route::resource('categories', CategoryController::class);

    Route::post('company/{id}/update', [CompanyController::class, 'update']);
    Route::resource('company', CompanyController::class);

});
Route::get('orders/download/{id}', [OrderController::class, 'download'])->name('order.download');
