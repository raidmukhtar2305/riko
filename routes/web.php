<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Member\AuthController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\Member\ProfileController;
use App\Http\Controllers\Member\WishlistController;
use App\Http\Controllers\Member\HistoryOrderController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\Member\MemberAddressController;
use App\Http\Controllers\Member\PaymentController;
use App\Http\Controllers\Member\RatingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/store/{name}', [ProductController::class, 'index'])->name('store.index');
// Route::get('/product/show/{name}', [ProductController::class, 'show'])->name('products.show');


//ordergit 
Route::get('/order/cart', [OrderController::class, 'cart'])->name('order.cart');
Route::get('/order/checkout', [OrderController::class, 'checkout'])->name('order.checkout');
Route::get('/add-to-cart/{id}', [OrderController::class, 'addToCart'])->name('product.addToCart');
Route::get('/cart-remove/{id}', [OrderController::class, 'deleteCart'])->name('order.cart.delete');
Route::get('/invoice/{id}', [OrderController::class, 'invoice'])->name('order.invoice');
Route::post('/checkout', [OrderController::class, 'checkoutStore'])->name('order.checkout.submit');


